import { databaseConfig } from './database-config'
import { swaggerConfig } from './swagger-config'
import { globalConfig } from './global-config'
import { jwtConfig } from './jwt-config'

export default [databaseConfig, swaggerConfig, jwtConfig, globalConfig]
