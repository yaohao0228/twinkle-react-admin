export const swaggerConfig = () => ({
  config: {
    title: 'Voi Admin API文档',
    description: 'Voi Admin API文档',
    version: '1.0'
  }
})

export type SwaggerConfig = ReturnType<typeof swaggerConfig>

export type SwaggerConfigKeysPaths = Record<NestedKeyOf<SwaggerConfig>, any>
