export const globalConfig = () => ({
  request: {
    ttl: 60000,
    limit: 2000
  },
  weChat: {
    appid: 'wx211af2435fd9f288',
    secret: 'f0b1ddaecdecf78a813fb6c288efcc23'
  }
})

export type GlobalConfig = ReturnType<typeof globalConfig>

export type GlobalConfigKeysPaths = Record<NestedKeyOf<GlobalConfig>, any>
