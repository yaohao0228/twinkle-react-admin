export const jwtConfig = () => ({
  jwt: {
    secret: 'MDBQYXAPFWTYCONRPEWOUIYRMCZFGK',
    time: '10h',
    defaultPassword: '123456'
  },
  admin: {
    defaultAdmin: 'admin',
    defaultAdminEmail: '1372407995@qq.com',
    defaultAdminPhone: '15623000328'
  }
})

export type JwtConfig = ReturnType<typeof jwtConfig>

export type JwtConfigKeysPaths = Record<NestedKeyOf<JwtConfig>, any>
