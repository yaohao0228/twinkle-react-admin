export const databaseConfig = () => ({
  database: {
    type: 'mysql',
    host: '110.41.80.161',
    port: 3306,
    database: 'sys_admin',
    username: 'sys_admin',
    password: 'L6nRRcA4nCebkjAy',
    autoLoadEntities: true,
    synchronize: true,
    connectorPackage: 'mysql2'
  }
})

export type DataBaseConfig = ReturnType<typeof databaseConfig>

export type DataBaseConfigKeysPaths = Record<NestedKeyOf<DataBaseConfig>, any>
