import { ArgumentsHost, ExceptionFilter, HttpException, HttpStatus } from '@nestjs/common'
import { Request } from 'express'
import { FastifyReply } from 'fastify'
import { CustomException } from '../exceptions/exception'
import { ResponseDto } from '../dto/response.dto'
import { isObject } from 'class-validator'
import { isFunction } from 'lodash'
import { Logger } from 'src/shared/utils/log4js.util'

/**
 * 全局异常处理
 */
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const context = host.switchToHttp()
    const request = context.getRequest<Request>()
    const response = context.getResponse<FastifyReply>()

    // 处理异常状态码
    const status = exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR
    response.header('Content-Type', 'application/json; charset=utf-8')

    // 处理异常代码
    const code = exception instanceof CustomException ? (exception as CustomException).getErrorCode() : status

    // 处理异常信息
    let message = '服务器异常，请稍后再试'
    message = exception instanceof HttpException ? exception.message : `${exception}`

    const exceptionResponse: any = isFunction(exception.getResponse) && exception.getResponse()
    let validatorMessage = exceptionResponse
    if (isObject(validatorMessage)) {
      validatorMessage = exceptionResponse.message[0]
    }

    // 异常日志格式
    const logFormat = ` <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    【请求路径】: ${request.originalUrl}
    【请求方法】: ${request.method}
    【来源】: ${request.ip}
    【状态码】: ${status}
    【异常信息】: ${exception.toString()} \n  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    `
    // 收集异常日志
    Logger.error(logFormat)

    const result = new ResponseDto(code, null, validatorMessage || message)
    response.status(status).send(result)
  }
}
