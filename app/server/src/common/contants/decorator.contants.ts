// @Keep
export const TRANSFORM_KEEP_KEY_METADATA = 'common:transform_keep'
// @Public
export const IS_PUBLIC_KEY = 'isPublic'
// @Permission
export const PERMISSION_KEY = 'permissionKey'
// @PermissionPrefix
export const PERMISSION_PREFIX = 'permissionPrefix'
// @Define
export const DEFINE_KEY = 'decorator'
