import { ApiProperty } from '@nestjs/swagger'
import { IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator'
import { FindOptionsOrder } from 'typeorm'

/**
 * 查询规则
 */
export type Operator = 'eq' | 'like' | 'not' | 'in' | 'lt' | 'lte' | 'mt' | 'mte' | 'isNull'

/**
 * 查询条件
 */
export class QueryCondition {
  @ApiProperty({ description: '字段', default: 'name' })
  @IsString()
  readonly field: string

  @ApiProperty({ description: '条件', default: 'eq' })
  @IsString()
  readonly operator: Operator

  @ApiProperty({ description: '值', type: String, default: 'mike' })
  @IsString()
  readonly value: unknown

  @ApiProperty({ description: '连接符', default: 'and' })
  @IsString()
  readonly andOr?: 'and' | 'or'
}

/**
 * 分页条件
 */
export class PageParameter {
  @ApiProperty({ description: '每页数量', default: 20 })
  @IsNumber({}, { message: '每页数量不能为空' })
  readonly pageSize: number

  @ApiProperty({ description: '当前页', default: 1 })
  @IsNumber({}, { message: '当前页不能为空' })
  readonly pageIndex: number
}

/**
 * 查询集合
 */
export class QueryParameter {
  @ApiProperty({ description: '查询条件', type: [QueryCondition] })
  @ValidateNested({ each: true })
  queryConditions: QueryCondition[]

  @ApiProperty({ description: '分页查询' })
  @ValidateNested()
  pageParameter: PageParameter

  @ApiProperty({ description: '排序条件' })
  @IsOptional()
  sort: FindOptionsOrder<unknown>
}
