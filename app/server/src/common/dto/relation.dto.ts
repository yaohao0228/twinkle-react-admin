export class RelationDto {
  public create: Record<string, any>[]

  public update: Record<string, any>[] & { id: string }

  public delete: string[]
}
