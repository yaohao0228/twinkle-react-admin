import { ApiProperty } from '@nestjs/swagger'

/**
 * 请求返回类型
 */
export class ResponseDto<T> {
  @ApiProperty({ example: [] })
  readonly data: T

  @ApiProperty({ example: 200 })
  readonly code: number

  @ApiProperty({ example: 'success' })
  readonly message: string

  constructor(code: number, data?: any, message = 'success') {
    this.code = code
    this.data = data
    this.message = message
  }

  static success(data?: any) {
    return new ResponseDto(200, data)
  }
}

/**
 * 请求分页
 */
export class Pagination {
  total: number

  page: number

  size: number
}

/**
 * 请求返回分页类型
 */
export class PaginatedResponseDto<T> {
  list: Array<T>

  pagination: Pagination
}
