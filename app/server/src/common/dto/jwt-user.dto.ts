import { RoleEntity } from 'src/entities/oauth/role.entity'

export class JwtUserDto {
  public readonly username: string

  public readonly userId: string

  public readonly roles: RoleEntity[]
}
