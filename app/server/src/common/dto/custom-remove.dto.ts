import { ApiProperty } from '@nestjs/swagger'
import { IsArray, IsNotEmpty } from 'class-validator'

export class CustomRemoveDto {
  @ApiProperty({ description: 'id集合' })
  @IsArray()
  @IsNotEmpty()
  ids: string[]
}
