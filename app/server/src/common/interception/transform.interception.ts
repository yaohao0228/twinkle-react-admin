import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { FastifyReply } from 'fastify'

import { TRANSFORM_KEEP_KEY_METADATA } from '../contants/decorator.contants'
import { ResponseDto } from '../dto/response.dto'
import { Logger } from 'src/shared/utils/log4js.util'

/**
 * 统一返回类型
 */
@Injectable()
export class TransformInterceptor implements NestInterceptor {
  constructor(private readonly reflector: Reflector) {}
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const req = context.getArgByIndex(1).req
    return next.handle().pipe(
      map((data) => {
        // 响应日志格式
        const logFormat = ` <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    【请求路径】: ${req.originalUrl}
    【请求方法】: ${req.method}
    【来源】: ${req.ip}
    【用户】: ${JSON.stringify(req.user.username)}
    【响应结果】:\n ${JSON.stringify(data)}
    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<`
        // 收集响应日志
        Logger.info(logFormat)
        Logger.access(logFormat)

        const keep = this.reflector.get<boolean>(TRANSFORM_KEEP_KEY_METADATA, context.getHandler())
        if (keep) {
          return data
        } else {
          const response = context.switchToHttp().getResponse<FastifyReply>()
          response.header('Content-Type', 'application/json; charset=utf-8')
          return new ResponseDto(200, data)
        }
      })
    )
  }
}
