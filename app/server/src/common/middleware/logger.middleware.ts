import { Injectable, NestMiddleware } from '@nestjs/common'
import { Request, Response } from 'express'
import { Logger } from 'src/shared/utils/log4js.util'

/**
 * 请求日志中间件
 */ @Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: () => void) {
    const code = res.statusCode // 响应状态码
    next()
    // 请求日志格式
    const logFormat = ` >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    【请求路径】: ${req.originalUrl}
    【请求方法】: ${req.method}
    【来源】: ${req.ip}
    【状态码】: ${code}
    【Params】: ${JSON.stringify(req.params)}
    【Query】: ${JSON.stringify(req.query)}
    【Body】: ${JSON.stringify(req.body)} \n  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  `
    // 根据状态码，进行日志类型区分
    if (code >= 500) {
      Logger.error(logFormat)
    } else if (code >= 400) {
      Logger.warn(logFormat)
    } else {
      Logger.access(logFormat)
      Logger.log(logFormat)
    }
  }
}
