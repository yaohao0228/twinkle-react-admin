import { isArray } from 'class-validator'
import { toArray } from 'lodash'

type IDepend = Promise<any> | Promise<any>[] | any[]
type IPrototypeClass<T = any> = new (...args: any[]) => T

interface IMetadata {
  imports: IDepend
  controllers: IDepend
  providers: IDepend
  exports: IDepend
}

export const isPromise = (value: any): boolean =>
  typeof value.then === 'function' && value.toString() === '[object Promise]'

export const Module = (metadata: Partial<IMetadata>) => {
  return (target: IPrototypeClass) => {
    for (const key in metadata) {
      metadataHandler.handler(metadata[key], target, key)
    }
  }
}

const metadataHandler = {
  setMetadata: (key: string, data: any, target: IPrototypeClass) => {
    const oldMetadata = Reflect.getMetadata(key, target)
    const metadata = []
    if (isArray(oldMetadata)) {
      metadata.push(...[...oldMetadata, ...toArray(data)])
    } else {
      metadata.push(...toArray(data))
    }
    Reflect.defineMetadata(key, metadata, target)
  },
  handler(data: any, target: IPrototypeClass, key: string) {
    const method: (k: string, v: any, t: IPrototypeClass) => void = this.setMetadata
    const single = (value: any) =>
      value && isPromise(value)
        ? (value as Promise<any>).then((d) => method(key, d, target))
        : method(key, value, target)
    isArray(data) ? (data as any[]).forEach((v) => single(v)) : single(data)
  }
}
