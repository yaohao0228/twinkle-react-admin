import { SetMetadata } from '@nestjs/common'
import { PERMISSION_KEY, PERMISSION_PREFIX } from '../contants/decorator.contants'

/**
 * 权限点唯一前缀
 */
export const PermissionPrefix = (permissionPrefix: string) => SetMetadata(PERMISSION_PREFIX, permissionPrefix)

/**
 * 权限点key
 */
export const Permission = (permissionKey: string) => SetMetadata(PERMISSION_KEY, permissionKey)
