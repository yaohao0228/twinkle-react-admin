import { SetMetadata } from '@nestjs/common'
import { IS_PUBLIC_KEY } from '../contants/decorator.contants'

/**
 * 公共路由装饰器
 */
export const Public = () => SetMetadata(IS_PUBLIC_KEY, true)
