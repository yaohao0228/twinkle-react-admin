import { ExecutionContext, createParamDecorator } from '@nestjs/common'
import { Request } from 'express'

/**
 * 获取用户jwt信息
 */
export const EntityCode = createParamDecorator((data: unknown, context: ExecutionContext) => {
  const request: Request = context.switchToHttp().getRequest()
  return request.url.split('/')[3]
})
