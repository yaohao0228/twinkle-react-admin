import { ExecutionContext, createParamDecorator } from '@nestjs/common'
import { JwtUserDto } from '../dto/jwt-user.dto'

/**
 * 获取用户jwt信息
 */
export const User = createParamDecorator((data: unknown, context: ExecutionContext) => {
  const request = context.switchToHttp().getRequest()
  return request.user as JwtUserDto
})
