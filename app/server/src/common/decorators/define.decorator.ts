import { DEFINE_KEY } from '../contants/decorator.contants'

/**
 * 定义导入模块
 */
export const Define = (metadata: string[]) => {
  return (target: any) => Reflect.defineMetadata(DEFINE_KEY, metadata, target)
}
