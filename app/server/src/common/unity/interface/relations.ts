import { Repository } from 'typeorm'

export interface RelationConfig {
  key?: string
  relations: Record<string, Repository<any>>
}
