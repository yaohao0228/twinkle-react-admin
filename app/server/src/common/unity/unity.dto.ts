import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsArray, IsNotEmpty } from 'class-validator'
import { QueryParameter } from '../dto/query-parameter.dto'

@Injectable()
export class UnityRemoveDto {
  @ApiProperty({ description: 'id集合' })
  @IsArray()
  @IsNotEmpty({ message: 'ids not null' })
  public ids: string[]
}

@Injectable()
export class UnityUpdateDto {
  @ApiProperty({ description: 'id' })
  @IsArray()
  @IsNotEmpty({ message: 'id not null' })
  public id: string
}

@Injectable()
export class UnityExportDto {
  @ApiProperty({ description: '字段集合' })
  @IsArray()
  @IsNotEmpty({ message: 'columns not null' })
  public columns: Array<string>

  @ApiProperty({ description: '查询条件' })
  public queryParameter: QueryParameter
}

@Injectable()
export class UnityImportDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: any
}
