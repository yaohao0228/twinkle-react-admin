import { CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn, VersionColumn, BaseEntity } from 'typeorm'

/**
 * 基础实体类
 */
export class UnityEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ comment: '主键' })
  public id: string

  @VersionColumn({ comment: '数据版本' })
  public version: number

  @CreateDateColumn({ comment: '创建时间' })
  public createTime: Date

  @UpdateDateColumn({ comment: '更新时间' })
  public updateTime: Date
}
