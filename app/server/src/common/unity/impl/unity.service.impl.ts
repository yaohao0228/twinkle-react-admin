import { Response } from 'express'
import { DeleteResult, FindManyOptions, ObjectLiteral, UpdateResult } from 'typeorm'

import { CustomRemoveDto } from 'src/common/dto/custom-remove.dto'
import { QueryParameter } from 'src/common/dto/query-parameter.dto'
import { PaginatedResponseDto } from 'src/common/dto/response.dto'
import { UnityExportDto, UnityUpdateDto } from '../unity.dto'
import { DataEntityEntity } from 'src/entities/system/data-entity.entity'

/**
 * 接口服务抽象类
 */
export abstract class UnityServiceImpl<
  Entity extends ObjectLiteral,
  CreateDto extends Object = any,
  UpdateDto extends UnityUpdateDto & Object = any
> {
  /**
   * 条件查询
   */
  public abstract find<T extends Entity>(queryParameter: QueryParameter): Promise<PaginatedResponseDto<T | Entity>>

  /**
   * 查询所有
   */
  public abstract findAll(): Promise<Entity[]>

  /**
   * 根据id查询
   */
  public abstract findById(id: string): Promise<Entity>

  /**
   * 保存
   */
  public abstract save(dto: CreateDto): Promise<any>

  /**
   * 批量保存
   */
  public abstract batchSave(dto: CreateDto[]): Promise<any>

  /**
   * 更新
   */
  public abstract update(dto: UpdateDto): Promise<UpdateResult>

  /**
   * 删除
   */
  public abstract remove(dto: CustomRemoveDto): Promise<DeleteResult>

  /**
   * 导出
   */
  public abstract exportData(dto: UnityExportDto, entityCode: string, res: Response): Promise<any>

  /**
   * 导入
   */
  public abstract importData(file: Express.Multer.File, entityCode: string): Promise<any>

  /**
   * 下载模版
   */
  public abstract downloadTemplate(entityCode: string, res: Response): Promise<void>

  /**
   * 查询前的回调
   */
  public abstract beforeFind<T extends FindManyOptions<Entity>>(options: T): Promise<T>

  /**
   * 查询后的回调
   */
  public abstract afterFind<T extends Entity | Entity[]>(entities: T): Promise<T>

  /**
   * 保存前的回调
   */
  public abstract beforeSave<T extends CreateDto>(dto: T): Promise<T>

  /**
   * 保存后的回调
   */
  public abstract afterSave<T extends Entity>(insertResult: T, dto: CreateDto): Promise<T>

  /**
   * 批量保存前的回调
   */
  public abstract beforeBatchSave<T extends CreateDto[]>(dto: T): Promise<T>

  /**
   * 保存后的回调
   */
  public abstract afterBatchSave<T extends Entity[]>(insertResult: T, dto: CreateDto[]): Promise<T>

  /**
   * 更新前的回调
   */
  public abstract beforeUpdate<T extends UpdateDto>(dto: T): Promise<T>

  /**
   * 更新后的回调
   */
  public abstract afterUpdate<T extends UpdateResult>(updateResult: T): Promise<T>

  /**
   * 删除前的回调
   */
  public abstract beforeDelete<T extends CustomRemoveDto>(dto: T): Promise<T>

  /**
   * 删除后的回调
   */
  public abstract afterDelete<T extends DeleteResult>(deleteResult: T): Promise<T>

  /**
   * 处理查询关系
   */
  public abstract handlerFindRelations(manager: FindManyOptions<Entity>): Promise<void>

  /**
   * 处理保存关系
   */
  public abstract handlerSaveRelations(dto: CreateDto, insertResult: Entity): Promise<void>

  /**
   * 处理查询条件
   */
  public abstract handlerQueryParameter(queryParameter: QueryParameter): FindManyOptions<Entity>

  /**
   * 过滤字段
   */
  public abstract filterDataByColumns(data: Array<Entity>, columns: Array<string>): Record<string, any>

  /**
   * 获取实体列字段
   */
  public abstract getEntityColumns(entityCode: string): Promise<DataEntityEntity>
}
