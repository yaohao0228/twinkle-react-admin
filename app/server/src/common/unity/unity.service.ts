import {
  DataSource,
  DeepPartial,
  DeleteResult,
  Equal,
  FindManyOptions,
  FindOperator,
  FindOptionsOrder,
  FindOptionsWhere,
  In,
  IsNull,
  LessThan,
  LessThanOrEqual,
  Like,
  MoreThan,
  MoreThanOrEqual,
  Not,
  ObjectLiteral,
  QueryRunner,
  Repository,
  UpdateResult
} from 'typeorm'
import { last } from 'lodash'
import { Injectable, StreamableFile } from '@nestjs/common'
import { join } from 'path'
import { createReadStream } from 'fs'
import { isEmpty, isObject } from 'class-validator'
import { Response } from 'express'

import { Operator, QueryParameter } from 'src/common/dto/query-parameter.dto'

import { PaginatedResponseDto, Pagination } from '../dto/response.dto'
import { RelationDto } from '../dto/relation.dto'
import { UnityExportDto, UnityRemoveDto, UnityUpdateDto } from './unity.dto'
import { CustomRemoveDto } from '../dto/custom-remove.dto'
import { UnityServiceImpl } from './impl/unity.service.impl'
import { RelationConfig } from './interface/relations'
import { XlsxActionUtil } from 'src/shared/utils/xlsx-action.util'
import { DataEntityEntity } from 'src/entities/system/data-entity.entity'

/**
 * 基础Service
 */
@Injectable()
export class UnityService<
  Entity extends ObjectLiteral,
  CreateDto extends Object = any,
  UpdateDto extends UnityUpdateDto & Object = any
> implements UnityServiceImpl<Entity, CreateDto, UpdateDto>
{
  private _relations: RelationConfig

  constructor(
    public readonly dataSource: DataSource,
    public readonly repository: Repository<Entity>,
    public relations: RelationConfig | null = null
  ) {
    this._relations = relations
  }

  public async beforeFind<T extends FindManyOptions<Entity>>(options?: T): Promise<T> {
    return options
  }

  public async afterFind<T extends Entity | Entity[]>(entities: T): Promise<T> {
    return entities
  }

  public async beforeSave<T extends CreateDto>(dto: T): Promise<T> {
    return dto
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async afterSave<T extends Entity>(insertResult: T, dto: CreateDto): Promise<T> {
    return insertResult
  }

  public async beforeBatchSave<T extends CreateDto[]>(dto: T): Promise<T> {
    return dto
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async afterBatchSave<T extends Entity[]>(insertResult: T, dto: CreateDto[]): Promise<T> {
    return insertResult
  }

  public async beforeUpdate<T extends UpdateDto>(dto: T): Promise<T> {
    return dto
  }

  public async afterUpdate<T extends UpdateResult>(updateResult: T): Promise<T> {
    return updateResult
  }

  public async beforeDelete<T extends CustomRemoveDto>(dto: T): Promise<T> {
    return dto
  }

  public async afterDelete<T extends DeleteResult>(deleteResult: T): Promise<T> {
    return deleteResult
  }

  public async find(queryParameter: QueryParameter): Promise<PaginatedResponseDto<Entity>> {
    const findManager: FindManyOptions<Entity> = this.handlerQueryParameter(queryParameter)
    const beforeFindManager = await this.beforeFind(findManager)

    // 处理查询关系
    await this.handlerFindRelations(beforeFindManager)

    const [content, count] = await this.repository.findAndCount(beforeFindManager)
    const afterContent = await this.afterFind(content)

    const pagination = new Pagination()
    const { pageIndex, pageSize } = queryParameter.pageParameter
    pagination.page = pageIndex
    pagination.size = pageSize
    pagination.total = count
    const paginatedResponse = new PaginatedResponseDto<Entity>()
    paginatedResponse.list = afterContent
    paginatedResponse.pagination = pagination
    return paginatedResponse
  }

  public async findAll(): Promise<Entity[]> {
    const findManager: FindManyOptions<Entity> = {}
    const beforeFindManager = await this.beforeFind(findManager)

    // 处理查询关系
    // await this.handlerFindRelations(beforeFindManager)

    const cacheKey = this.getCacheKey()
    const entity = await this.repository.find({
      ...beforeFindManager,
      cache: { id: cacheKey, milliseconds: 25000 }
    })
    return this.afterFind(entity)
  }

  public async findById(id: string): Promise<Entity> {
    const findManager: FindManyOptions<Entity> = {}
    const beforeFindManager = await this.beforeFind(findManager)

    // 处理查询关系
    await this.handlerFindRelations(beforeFindManager)

    // @ts-ignore
    const entity = await this.repository.findOne({ ...beforeFindManager, where: { id } })
    return this.afterFind(entity)
  }

  public async save(dto: CreateDto): Promise<any> {
    const queryRunner: QueryRunner = this.dataSource.createQueryRunner()
    await queryRunner.connect()
    await queryRunner.startTransaction()

    try {
      const beforeDto = await this.beforeSave(dto)
      const entity = this.repository.create(beforeDto as unknown as DeepPartial<Entity>)
      const insertResult = await this.repository.save(entity)

      // 处理保存关系
      await this.handlerSaveRelations(beforeDto, insertResult)

      const cacheKey = this.getCacheKey()
      if (cacheKey) {
        await this.dataSource.queryResultCache?.remove([cacheKey])
      }

      const result = await this.afterSave(insertResult, beforeDto)

      // 提交事务
      await queryRunner.commitTransaction()

      return result
    } catch (error) {
      await queryRunner.rollbackTransaction()
      throw error
    } finally {
      await queryRunner.release()
    }
  }

  public async batchSave(dto: CreateDto[]): Promise<any> {
    const queryRunner: QueryRunner = this.dataSource.createQueryRunner()

    await queryRunner.connect()
    await queryRunner.startTransaction()

    try {
      const beforeDto = await this.beforeBatchSave(dto)
      const entities = beforeDto.map((item) => this.repository.create(item as unknown as DeepPartial<Entity>))
      const insertResult = await this.repository.save(entities)

      // 处理保存关系
      for (let i = 0; i < insertResult.length; i++) {
        await this.handlerSaveRelations(beforeDto[i], insertResult[i])
      }

      const cacheKey = this.getCacheKey()
      if (cacheKey) {
        await this.dataSource.queryResultCache?.remove([cacheKey])
      }

      const result = await this.afterBatchSave(insertResult, beforeDto)

      // 提交事务
      await queryRunner.commitTransaction()

      return result
    } catch (error) {
      await queryRunner.rollbackTransaction()
      throw error
    } finally {
      await queryRunner.release()
    }
  }

  public async update(dto: UpdateDto): Promise<UpdateResult> {
    const queryRunner = this.dataSource.createQueryRunner()

    await queryRunner.connect()
    await queryRunner.startTransaction()

    try {
      const beforeDto = await this.beforeUpdate(dto)
      const entity = this.repository.create(beforeDto as unknown as DeepPartial<Entity>)
      const updateResult = await this.repository.update(dto.id, entity)

      const cacheKey = this.getCacheKey()
      if (cacheKey) {
        await this.dataSource.queryResultCache?.remove([cacheKey])
      }

      // 提交事务
      const result = await this.afterUpdate(updateResult)

      await queryRunner.commitTransaction()

      return result
    } catch (error) {
      await queryRunner.rollbackTransaction()
      throw error
    } finally {
      await queryRunner.release()
    }
  }

  public async remove(dto: UnityRemoveDto): Promise<DeleteResult> {
    const queryRunner = this.dataSource.createQueryRunner()

    await queryRunner.connect()
    await queryRunner.startTransaction()

    try {
      const beforeDto = await this.beforeDelete(dto)
      const deleteResult = await this.repository.delete(beforeDto.ids)

      const cacheKey = this.getCacheKey()
      if (cacheKey) {
        await this.dataSource.queryResultCache?.remove([cacheKey])
      }

      const result = await this.afterDelete(deleteResult)

      // 提交事务
      await queryRunner.commitTransaction()

      return result
    } catch (error) {
      await queryRunner.rollbackTransaction()
      throw error
    } finally {
      await queryRunner.release()
    }
  }

  public async exportData(dto: UnityExportDto, entityCode: string, res: Response): Promise<any> {
    const data = await this.find(dto.queryParameter)
    const filterData = this.filterDataByColumns(data.list, dto.columns)
    const entityData = await this.getEntityColumns(entityCode)
    const localizeData = (fields: (typeof entityData)['dataEntityColumn'], data: Record<string, any>[]) => {
      // 创建一个映射表，将字段的code映射到它的中文name
      const fieldMapping: { [code: string]: string } = {}
      for (const field of fields) {
        fieldMapping[field.code] = field.name
      }

      // 将每个数据对象的键替换为对应的中文名
      return data.map((item) => {
        const localizedItem: Record<string, any> = {}
        for (const key in item) {
          if (fieldMapping[key]) {
            localizedItem[fieldMapping[key]] = item[key]
          }
        }
        return localizedItem
      })
    }

    const result = localizeData(entityData.dataEntityColumn, filterData)
    const filePath = join(process.cwd(), 'src/assets/xlsx/export.xlsx')
    XlsxActionUtil.writeFile(filePath, result)
    const streamableFile = new StreamableFile(createReadStream(filePath))
    streamableFile.getStream().pipe(res)
  }

  public async importData(file: Express.Multer.File, entityCode: string): Promise<any> {
    const sheetData: CreateDto[] = XlsxActionUtil.read(file)
    const entityData = await this.getEntityColumns(entityCode)
    const localizeData = <T = CreateDto>(fields: (typeof entityData)['dataEntityColumn'], data: T[]) => {
      // 创建一个映射表，将字段的code映射到它的中文name
      const fieldMapping: { [code: string]: string } = {}
      for (const field of fields) {
        fieldMapping[field.name] = field.code
      }

      // 将每个数据对象的键替换为对应的中文名
      return data.map((item) => {
        const localizedItem: T = {} as T
        for (const key in item) {
          if (fieldMapping[key]) {
            localizedItem[fieldMapping[key]] = item[key]
          }
        }
        return localizedItem
      })
    }
    return await this.batchSave(localizeData(entityData.dataEntityColumn, sheetData))
  }

  public async downloadTemplate(entityCode: string, res: Response): Promise<void> {
    const entityData = await this.getEntityColumns(entityCode)
    const templateData: Record<string, any> = {}
    entityData.dataEntityColumn.forEach((column) => (templateData[column.name] = ''))
    const filePath = join(process.cwd(), 'src/assets/xlsx/template.xlsx')
    XlsxActionUtil.writeFile(filePath, [templateData])

    const streamableFile = new StreamableFile(createReadStream(filePath))
    streamableFile.getStream().pipe(res)
  }

  public handlerQueryParameter(queryParameter: QueryParameter): FindManyOptions<Entity> {
    // 条件策略
    const operatorStrategy: Record<Operator, (value: any) => FindOperator<any>> = {
      like: (value: string) => Like(`%${value}%`),
      eq: (value: string) => Equal(value),
      not: (value: string) => Not(value),
      in: (value: unknown[]) => In(value),
      lt: (value: string) => LessThan(value),
      lte: (value: string) => LessThanOrEqual(value),
      mt: (value: string) => MoreThan(value),
      mte: (value: string) => MoreThanOrEqual(value),
      isNull: () => IsNull()
    }

    const {
      pageParameter: { pageIndex, pageSize },
      queryConditions,
      sort
    } = queryParameter

    const findManager: FindManyOptions<Entity> = {}
    const defaultOrder = { createTime: 'DESC' } as unknown as FindOptionsOrder<Entity>
    findManager.skip = (pageIndex - 1) * pageSize
    findManager.take = pageSize
    findManager.order = Object.keys(sort).length === 0 ? defaultOrder : sort
    // 解析条件查询
    const findOptionsWhere: FindOptionsWhere<any>[] = []
    queryConditions.length > 0 &&
      queryConditions.forEach((queryCondition) => {
        if (findOptionsWhere.length <= 0) findOptionsWhere.push({})

        if (queryCondition.andOr === 'or') return findOptionsWhere.push({})

        if (isEmpty(queryCondition.field) && queryCondition.andOr === 'and') return

        // 处理关系查询
        if (queryCondition.field.includes('.')) {
          const [relation, field] = queryCondition.field.split('.')
          return (last(findOptionsWhere)[relation] = {
            [field]: operatorStrategy[queryCondition.operator](queryCondition.value)
          })
        }

        // 根据条件解析
        last(findOptionsWhere)[queryCondition.field] = operatorStrategy[queryCondition.operator](queryCondition.value)
      })
    findManager.where = findOptionsWhere
    return findManager
  }

  public async handlerFindRelations(manager: FindManyOptions<Entity>): Promise<void> {
    if (isEmpty(this._relations?.relations)) return
    for (const relation in this._relations.relations) {
      if (!isObject(manager.relations)) manager.relations = {}
      manager.relations[relation] = true
    }
  }

  public async handlerSaveRelations(dto: CreateDto, data: Entity): Promise<void> {
    if (isEmpty(this._relations?.relations)) return
    // 处理多个关系
    for (const relation in this._relations.relations) {
      const relationDto: RelationDto = dto[relation]
      // 非空判断
      if (isEmpty(relationDto)) return

      // 多对一关系的处理
      if (!isObject(relationDto)) {
        const targetEntity: Repository<any> = await this._relations.relations[relation].findOne({
          where: { id: relationDto }
        })
        return await targetEntity.save(dto)
      }
      // 一对多关系的处理
      const insetData: any[] = []

      if (relationDto.create?.length > 0) {
        for (const item of relationDto.create) {
          Reflect.deleteProperty(item, 'id')
          for (const key in item) {
            if (isEmpty(item[key])) Reflect.deleteProperty(item, key)
          }
          insetData.push(this._relations.relations[relation].create({ ...item, [this._relations.key]: data }))
        }
      }

      if (relationDto.update?.length > 0) {
        for (const item of relationDto.update) {
          this._relations.relations[relation].update(item.id, item)
        }
      }

      if (relationDto.delete?.length > 0) {
        this._relations.relations[relation].delete(relationDto.delete)
      }

      await this._relations.relations[relation].save(insetData)
    }
  }

  public filterDataByColumns(data: Array<Entity>, columns: Array<string>) {
    return data.map((item) => {
      const filteredItem: Record<string, any> = {}
      for (const column of columns) {
        if (column in item) {
          filteredItem[column] = item[column]
        }
      }
      return filteredItem
    })
  }

  private getCacheKey(): string {
    return `${this.repository.target.toString().split(' ')[1]}_cache`
  }

  public async getEntityColumns(entityCode: string): Promise<DataEntityEntity> {
    const data = await this.dataSource
      .getRepository(DataEntityEntity)
      .createQueryBuilder('dataEntity')
      .where('dataEntity.code = :code', { code: entityCode })
      .leftJoinAndSelect('dataEntity.dataEntityColumn', 'dataEntityColumn')
      .getOne()
    return data
  }
}
