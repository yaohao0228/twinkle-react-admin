import { Body, Delete, Get, Header, Param, Post, Put, Res, UploadedFile, UseInterceptors } from '@nestjs/common'
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiOkResponse, ApiOperation } from '@nestjs/swagger'
import { FileInterceptor } from '@nestjs/platform-express'
import { DeleteResult, UpdateResult } from 'typeorm'
import { Response } from 'express'

import { QueryParameter } from 'src/common/dto/query-parameter.dto'
import { ResponseDto } from 'src/common/dto/response.dto'
import { UnityService } from './unity.service'
import { UnityExportDto, UnityImportDto, UnityRemoveDto } from './unity.dto'
import { Permission } from '../decorators/permission.decorator'
import { PermissionAction } from '../enum/permission-action.enum'
import { EntityCode } from '../decorators/entity-code.decorator'

/**
 * 基础Controller
 */
@ApiBearerAuth()
export class UnityController<CreateDto, UpdateDto, Service extends UnityService<any>> {
  constructor(private readonly service: Service) {}

  @ApiOperation({ summary: '查询列表' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.READ)
  @Post('list')
  public async find(@Body() queryParameter: QueryParameter): Promise<unknown> {
    return this.service.find(queryParameter)
  }

  @ApiOperation({ summary: '查询所有' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.READ)
  @Get('listAll')
  public async findAll(): Promise<unknown[]> {
    return this.service.findAll()
  }

  @ApiOperation({ summary: '查询详情' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.READ)
  @Get(':id')
  public async findById(@Param('id') id: string): Promise<unknown[]> {
    return this.service.findById(id)
  }

  @ApiOperation({ summary: '保存' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.CREATE)
  @Post('save')
  public async save(@Body() dto: CreateDto): Promise<any> {
    return this.service.save(dto)
  }

  @ApiOperation({ summary: '修改' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.UPDATE)
  @Put()
  public async update(@Body() dto: UpdateDto): Promise<UpdateResult> {
    return this.service.update(dto)
  }

  @ApiOperation({ summary: '删除' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.DELETE)
  @Delete()
  public async remove(@Body() dto: UnityRemoveDto): Promise<DeleteResult> {
    return this.service.remove(dto)
  }

  @ApiOperation({ summary: '导出' })
  @Permission(PermissionAction.EXPORT)
  @Post('export')
  @Header('Content-Type', 'application/octet-stream')
  @Header('Content-Disposition', 'attachment; filename="export.xlsx"')
  public async exportData(
    @Body() dto: UnityExportDto,
    @EntityCode() entityCode: string,
    @Res() res: Response
  ): Promise<void> {
    return this.service.exportData(dto, entityCode, res)
  }

  @ApiOperation({ summary: '导入' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({ description: '导入', type: UnityImportDto })
  @Permission(PermissionAction.IMPORT)
  @UseInterceptors(FileInterceptor('file'))
  @Post('import')
  public async importData(@UploadedFile() file: Express.Multer.File, @EntityCode() entityCode: string): Promise<any> {
    return this.service.importData(file, entityCode)
  }

  @ApiOperation({ summary: '下载模版' })
  @Get('template/download')
  @Header('Content-Type', 'application/octet-stream')
  @Header('Content-Disposition', 'attachment; filename="template.xlsx"')
  public async downloadTemplate(@EntityCode() entityCode: string, @Res() res: Response): Promise<void> {
    return this.service.downloadTemplate(entityCode, res)
  }
}
