import { OauthModule } from './oauth/oauth.module'
import { SystemModule } from './system/system.module'

export default [OauthModule, SystemModule]
