import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { DataEntityColumnService } from './data-entity-column.service'
import { DataEntityColumnController } from './data-entity-column.controller'
import { DataEntityColumnEntity } from 'src/entities/system/data-entity-column.entity'
import { DataEntityEntity } from 'src/entities/system/data-entity.entity'
import { Define } from 'src/common/decorators/define.decorator'

@Define(['Module'])
@Module({
  imports: [TypeOrmModule.forFeature([DataEntityColumnEntity, DataEntityEntity])],
  controllers: [DataEntityColumnController],
  providers: [DataEntityColumnService]
})
export class DataEntityColumnModule {}
