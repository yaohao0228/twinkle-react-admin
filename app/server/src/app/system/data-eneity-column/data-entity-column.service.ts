import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { DataSource, Repository } from 'typeorm'

import { DataEntityColumnServiceImpl } from './impl/data-entity-column.service.impl'
import { CreateDataEntityColumnDto } from './dto/create-data-entity-column.dto'
import { UpdateDataEntityColumnDto } from './dto/update-data-entity-column.dto'

import { DataEntityColumnEntity } from 'src/entities/system/data-entity-column.entity'
import { DataEntityEntity } from 'src/entities/system/data-entity.entity'
import { UnityService } from 'src/common/unity/unity.service'

@Injectable()
export class DataEntityColumnService
  extends UnityService<DataEntityColumnEntity, CreateDataEntityColumnDto, UpdateDataEntityColumnDto>
  implements DataEntityColumnServiceImpl
{
  constructor(
    public readonly dataSource: DataSource,
    @InjectRepository(DataEntityColumnEntity)
    public readonly dataEntityColumnRepository: Repository<DataEntityColumnEntity>,
    @InjectRepository(DataEntityEntity)
    public readonly dataEntityRepository: Repository<DataEntityEntity>
  ) {
    super(dataSource, dataEntityColumnRepository, {
      key: 'dataEntityColumn',
      relations: { dataEntity: dataEntityRepository }
    })
  }
}
