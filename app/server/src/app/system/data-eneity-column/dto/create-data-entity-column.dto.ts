import { PartialType } from '@nestjs/mapped-types'
import { ApiProperty } from '@nestjs/swagger'
import { DataEntityColumnEntity } from 'src/entities/system/data-entity-column.entity'
import { DataEntityEntity } from 'src/entities/system/data-entity.entity'

export class CreateDataEntityColumnDto extends PartialType(DataEntityColumnEntity) {
  @ApiProperty({ description: '实体代码' })
  public code: string

  @ApiProperty({ description: '元数据名称' })
  public name: string

  @ApiProperty({ description: '元数据类型' })
  public type: string

  @ApiProperty({ description: '数据类型' })
  public dataType: string

  @ApiProperty({ description: '是否可空', default: false })
  public nullable: boolean

  @ApiProperty({ description: '关键字', default: false })
  public unique: boolean

  @ApiProperty({ description: '默认值' })
  public default: string

  @ApiProperty({ description: '备注' })
  public comment: string

  @ApiProperty({ description: '实体id', type: 'number' })
  public dataEntity?: DataEntityEntity
}
