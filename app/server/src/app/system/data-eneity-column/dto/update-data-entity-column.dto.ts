import { PartialType } from '@nestjs/mapped-types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

import { CreateDataEntityColumnDto } from './create-data-entity-column.dto'

export class UpdateDataEntityColumnDto extends PartialType(CreateDataEntityColumnDto) {
  @ApiProperty({ description: 'id' })
  @IsString()
  @IsNotEmpty({ message: 'id不能为空' })
  id: string
}
