import { Controller, Post, Body } from '@nestjs/common'
import { ApiTags, ApiOperation, ApiOkResponse } from '@nestjs/swagger'

import { DataEntityColumnService } from './data-entity-column.service'

import { CreateDataEntityColumnDto } from './dto/create-data-entity-column.dto'
import { UpdateDataEntityColumnDto } from './dto/update-data-entity-column.dto'

import { UnityController } from 'src/common/unity/unity.controller'
import { Permission, PermissionPrefix } from 'src/common/decorators/permission.decorator'
import { ResponseDto } from 'src/common/dto/response.dto'
import { PermissionAction } from 'src/common/enum/permission-action.enum'

@ApiTags('数据实体字段管理')
@PermissionPrefix('data_entity_column')
@Controller({ path: 'system/dataEntityColumn', version: '1' })
export class DataEntityColumnController extends UnityController<CreateDataEntityColumnDto, UpdateDataEntityColumnDto, DataEntityColumnService> {
  constructor(private readonly dataEntityColumnService: DataEntityColumnService) {
    super(dataEntityColumnService)
  }

  @ApiOperation({ summary: '保存' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.CREATE)
  @Post('save')
  public save(@Body() dto: CreateDataEntityColumnDto): Promise<any> {
    return this.dataEntityColumnService.save(dto)
  }
}
