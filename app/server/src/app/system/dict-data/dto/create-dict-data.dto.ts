import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsBoolean, IsNotEmpty, IsOptional } from 'class-validator'

import { DictTypeEntity } from 'src/entities/system/dict-type.entity'

@Injectable()
export class CreateDictDataDto {
  @ApiProperty({ description: '字典名称' })
  @IsString()
  @IsNotEmpty({ message: 'dictName not null' })
  public dictName: string

  @ApiProperty({ description: '字典编码' })
  @IsString()
  @IsNotEmpty({ message: 'dictCode not null' })
  public dictCode: string

  @ApiProperty({ description: '是否启用', default: true, required: false })
  @IsBoolean()
  @IsOptional()
  public isStart: boolean

  @ApiProperty({ description: '类型名称' })
  @IsString()
  @IsNotEmpty({ message: 'typeName not null' })
  public typeName: string

  @ApiProperty({ description: '类型编码' })
  @IsString()
  @IsNotEmpty({ message: 'typeCode not null' })
  public typeCode: string

  @ApiProperty()
  public dictType: DictTypeEntity
}
