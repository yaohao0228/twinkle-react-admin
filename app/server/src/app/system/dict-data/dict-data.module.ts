import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { DictDataService } from './dict-data.service'
import { DictDataController } from './dict-data.controller'
import { Define } from 'src/common/decorators/define.decorator'

import { DictDataEntity } from 'src/entities/system/dict-data.entity'

import { DictTypeEntity } from 'src/entities/system/dict-type.entity'

@Define(['Module'])
@Module({
  imports: [TypeOrmModule.forFeature([DictDataEntity, DictTypeEntity])],
  controllers: [DictDataController],
  providers: [DictDataService]
})
export class DictDataModule {}
