import { Body, Controller, Post, Put } from '@nestjs/common'
import { UpdateResult } from 'typeorm'
import { ApiTags, ApiOperation, ApiOkResponse } from '@nestjs/swagger'

import { DictDataService } from './dict-data.service'

import { ResponseDto } from 'src/common/dto/response.dto'
import { CreateDictDataDto } from './dto/create-dict-data.dto'
import { UpdateDictDataDto } from './dto/update-dict-data.dto'

import { UnityController } from 'src/common/unity/unity.controller'
import { Permission, PermissionPrefix } from 'src/common/decorators/permission.decorator'
import { PermissionAction } from 'src/common/enum/permission-action.enum'

@ApiTags('数据字典')
@PermissionPrefix('dict_data')
@Controller({ path: 'system/dictData', version: '1' })
export class DictDataController extends UnityController<CreateDictDataDto, UpdateDictDataDto, DictDataService> {
  constructor(private readonly dictDataService: DictDataService) {
    super(dictDataService)
  }

  @ApiOperation({ summary: '保存' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.CREATE)
  @Post('save')
  public save(@Body() dto: CreateDictDataDto): Promise<any> {
    return this.dictDataService.save(dto)
  }

  @ApiOperation({ summary: '修改' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.UPDATE)
  @Put()
  public update(@Body() dto: UpdateDictDataDto): Promise<UpdateResult> {
    return this.dictDataService.update(dto)
  }
}
