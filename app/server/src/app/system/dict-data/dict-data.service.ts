import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { DataSource, Repository } from 'typeorm'

import { DictDataServiceImpl } from './impl/dict-data.service.impl'
import { CreateDictDataDto } from './dto/create-dict-data.dto'
import { UpdateDictDataDto } from './dto/update-dict-data.dto'

import { UnityService } from 'src/common/unity/unity.service'
import { DictDataEntity } from 'src/entities/system/dict-data.entity'

import { DictTypeEntity } from 'src/entities/system/dict-type.entity'

@Injectable()
export class DictDataService
  extends UnityService<DictDataEntity, CreateDictDataDto, UpdateDictDataDto>
  implements DictDataServiceImpl
{
  constructor(
    public readonly dataSource: DataSource,
    @InjectRepository(DictDataEntity)
    public readonly dictDataRepository: Repository<DictDataEntity>,

    @InjectRepository(DictTypeEntity)
    public readonly dictTypeRepository: Repository<DictTypeEntity>
  ) {
    super(dataSource, dictDataRepository, {
      key: 'dictData',
      relations: {
        dictType: dictTypeRepository
      }
    })
  }
}
