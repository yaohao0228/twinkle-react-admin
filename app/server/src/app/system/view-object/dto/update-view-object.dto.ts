import { Injectable } from '@nestjs/common'
import { PartialType } from '@nestjs/mapped-types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

import { CreateViewObjectDto } from './create-view-object.dto'

@Injectable()
export class UpdateViewObjectDto extends PartialType(CreateViewObjectDto) {
  @ApiProperty({ description: 'id' })
  @IsString()
  @IsNotEmpty({ message: 'id not null' })
  public id: string
}
