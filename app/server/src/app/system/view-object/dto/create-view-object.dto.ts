import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsNotEmpty } from 'class-validator'

import { ViewObjectColumnEntity } from 'src/entities/system/view-object-column.entity'

@Injectable()
export class CreateViewObjectDto {
  @ApiProperty({ description: 'VO代码' })
  @IsString()
  @IsNotEmpty({ message: 'code not null' })
  public code: string

  @ApiProperty({ description: 'VO名称' })
  @IsString()
  @IsNotEmpty({ message: 'name not null' })
  public name: string

  @ApiProperty({ description: '接口服务' })
  @IsString()
  @IsNotEmpty({ message: 'service not null' })
  public service: string

  @ApiProperty()
  public viewObjectColumn: ViewObjectColumnEntity
}
