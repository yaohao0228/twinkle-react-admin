import { Controller, Post, Body } from '@nestjs/common'
import { ApiTags, ApiOperation, ApiOkResponse } from '@nestjs/swagger'

import { ViewObjectService } from './view-object.service'

import { CreateViewObjectDto } from './dto/create-view-object.dto'
import { UpdateViewObjectDto } from './dto/update-view-object.dto'

import { UnityController } from 'src/common/unity/unity.controller'
import { Permission, PermissionPrefix } from 'src/common/decorators/permission.decorator'
import { ResponseDto } from 'src/common/dto/response.dto'
import { PermissionAction } from 'src/common/enum/permission-action.enum'

@ApiTags('视图对象管理')
@PermissionPrefix('view_object')
@Controller({ path: 'system/viewObject', version: '1' })
export class ViewObjectController extends UnityController<CreateViewObjectDto, UpdateViewObjectDto, ViewObjectService> {
  constructor(private readonly viewObjectService: ViewObjectService) {
    super(viewObjectService)
  }

  @ApiOperation({ summary: '保存' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.CREATE)
  @Post('save')
  public save(@Body() dto: CreateViewObjectDto): Promise<any> {
    return this.viewObjectService.save(dto)
  }
}
