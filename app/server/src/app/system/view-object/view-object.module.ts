import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ViewObjectService } from './view-object.service'
import { ViewObjectController } from './view-object.controller'
import { ViewObjectEntity } from 'src/entities/system/view-object.entity'
import { ViewObjectColumnEntity } from 'src/entities/system/view-object-column.entity'

import { Define } from 'src/common/decorators/define.decorator'

@Define(['Module'])
@Module({
  imports: [TypeOrmModule.forFeature([ViewObjectEntity, ViewObjectColumnEntity])],
  controllers: [ViewObjectController],
  providers: [ViewObjectService],
  exports: [ViewObjectService]
})
export class ViewObjectModule {}
