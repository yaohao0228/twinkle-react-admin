import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { DataSource, Repository } from 'typeorm'

import { ViewObjectServiceImpl } from './impl/view-object.service.impl'
import { CreateViewObjectDto } from './dto/create-view-object.dto'
import { UpdateViewObjectDto } from './dto/update-view-object.dto'

import { ViewObjectEntity } from 'src/entities/system/view-object.entity'
import { ViewObjectColumnEntity } from 'src/entities/system/view-object-column.entity'
import { UnityService } from 'src/common/unity/unity.service'

@Injectable()
export class ViewObjectService
  extends UnityService<ViewObjectEntity, CreateViewObjectDto, UpdateViewObjectDto>
  implements ViewObjectServiceImpl
{
  constructor(
    public readonly dataSource: DataSource,
    @InjectRepository(ViewObjectEntity)
    public readonly viewObjectRepository: Repository<ViewObjectEntity>,
    @InjectRepository(ViewObjectColumnEntity)
    public readonly viewObjectColumnRepository: Repository<ViewObjectColumnEntity>
  ) {
    super(dataSource, viewObjectRepository, { key: 'viewObject', relations: { viewObjectColumn: viewObjectColumnRepository } })
  }
}
