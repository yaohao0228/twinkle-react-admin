import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { DataSource, Repository } from 'typeorm'

import { MetaDataServiceImpl } from './impl/meta-data.service.impl'
import { CreateMetaDataDto } from './dto/create-meta-data.dto'
import { UpdateMetaDataDto } from './dto/update-meta-data.dto'

import { MetaDataEntity } from 'src/entities/system/meta-data.entity'
import { UnityService } from 'src/common/unity/unity.service'

@Injectable()
export class MetaDataService
  extends UnityService<MetaDataEntity, CreateMetaDataDto, UpdateMetaDataDto>
  implements MetaDataServiceImpl
{
  constructor(
    public readonly dataSource: DataSource,
    @InjectRepository(MetaDataEntity)
    public readonly metaDataRepository: Repository<MetaDataEntity>
  ) {
    super(dataSource, metaDataRepository)
  }
}
