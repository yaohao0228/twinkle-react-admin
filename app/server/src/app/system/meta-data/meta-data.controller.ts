import { Controller } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'

import { MetaDataService } from './meta-data.service'

import { CreateMetaDataDto } from './dto/create-meta-data.dto'
import { UpdateMetaDataDto } from './dto/update-meta-data.dto'

import { UnityController } from 'src/common/unity/unity.controller'
import { PermissionPrefix } from 'src/common/decorators/permission.decorator'

@ApiTags('元数据管理')
@PermissionPrefix('meta_data')
@Controller({ path: 'system/metaData', version: '1' })
export class MetaDataController extends UnityController<CreateMetaDataDto, UpdateMetaDataDto, MetaDataService> {
  constructor(private readonly metaDataService: MetaDataService) {
    super(metaDataService)
  }
}
