import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { MetaDataService } from './meta-data.service'
import { MetaDataController } from './meta-data.controller'
import { MetaDataEntity } from 'src/entities/system/meta-data.entity'
import { Define } from 'src/common/decorators/define.decorator'

@Define(['Module'])
@Module({
  imports: [TypeOrmModule.forFeature([MetaDataEntity])],
  controllers: [MetaDataController],
  providers: [MetaDataService]
})
export class MetaDataModule {}
