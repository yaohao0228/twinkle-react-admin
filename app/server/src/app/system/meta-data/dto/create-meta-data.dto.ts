import { PartialType } from '@nestjs/mapped-types'
import { MetaDataEntity } from 'src/entities/system/meta-data.entity'

export class CreateMetaDataDto extends PartialType(MetaDataEntity) {}
