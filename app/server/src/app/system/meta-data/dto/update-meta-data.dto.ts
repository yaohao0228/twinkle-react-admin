import { PartialType } from '@nestjs/mapped-types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

import { CreateMetaDataDto } from './create-meta-data.dto'

export class UpdateMetaDataDto extends PartialType(CreateMetaDataDto) {
  @ApiProperty({ description: 'id' })
  @IsString()
  @IsNotEmpty({ message: 'id不能为空' })
  id: string
}
