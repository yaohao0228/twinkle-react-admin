import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { DataSource, Repository } from 'typeorm'

import { DataSourceServiceImpl } from './impl/data-source.service.impl'
import { CreateDataSourceDto } from './dto/create-data-source.dto'
import { UpdateDataSourceDto } from './dto/update-data-source.dto'

import { UnityService } from 'src/common/unity/unity.service'
import { DataSourceEntity } from 'src/entities/system/data-source.entity'

@Injectable()
export class DataSourceService
  extends UnityService<DataSourceEntity, CreateDataSourceDto, UpdateDataSourceDto>
  implements DataSourceServiceImpl
{
  constructor(
    public readonly dataSource: DataSource,
    @InjectRepository(DataSourceEntity)
    public readonly dataSourceRepository: Repository<DataSourceEntity>
  ) {
    super(dataSource, dataSourceRepository)
  }
}
