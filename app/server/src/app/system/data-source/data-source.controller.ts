import { Body, Controller, Post, Put } from '@nestjs/common'
import { UpdateResult } from 'typeorm'
import { ApiTags, ApiOperation, ApiOkResponse } from '@nestjs/swagger'

import { DataSourceService } from './data-source.service'

import { ResponseDto } from 'src/common/dto/response.dto'
import { CreateDataSourceDto } from './dto/create-data-source.dto'
import { UpdateDataSourceDto } from './dto/update-data-source.dto'

import { UnityController } from 'src/common/unity/unity.controller'
import { Permission, PermissionPrefix } from 'src/common/decorators/permission.decorator'
import { PermissionAction } from 'src/common/enum/permission-action.enum'

@ApiTags('数据源')
@PermissionPrefix('data_source')
@Controller({ path: 'system/dataSource', version: '1' })
export class DataSourceController extends UnityController<CreateDataSourceDto, UpdateDataSourceDto, DataSourceService> {
  constructor(private readonly dataSourceService: DataSourceService) {
    super(dataSourceService)
  }

  @ApiOperation({ summary: '保存' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.CREATE)
  @Post('save')
  public save(@Body() dto: CreateDataSourceDto): Promise<any> {
    return this.dataSourceService.save(dto)
  }

  @ApiOperation({ summary: '修改' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.UPDATE)
  @Put()
  public update(@Body() dto: UpdateDataSourceDto): Promise<UpdateResult> {
    return this.dataSourceService.update(dto)
  }
}
