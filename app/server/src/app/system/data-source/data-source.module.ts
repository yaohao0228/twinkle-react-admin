import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { DataSourceService } from './data-source.service'
import { DataSourceController } from './data-source.controller'
import { Define } from 'src/common/decorators/define.decorator'

import { DataSourceEntity } from 'src/entities/system/data-source.entity'

@Define(['Module'])
@Module({
  imports: [TypeOrmModule.forFeature([DataSourceEntity])],
  controllers: [DataSourceController],
  providers: [DataSourceService]
})
export class DataSourceModule {}
