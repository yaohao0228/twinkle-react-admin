import { Injectable } from '@nestjs/common'
import { PartialType } from '@nestjs/mapped-types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

import { CreateDataSourceDto } from './create-data-source.dto'

@Injectable()
export class UpdateDataSourceDto extends PartialType(CreateDataSourceDto) {
  @ApiProperty({ description: 'id' })
  @IsString()
  @IsNotEmpty({ message: 'id not null' })
  public id: string
}
