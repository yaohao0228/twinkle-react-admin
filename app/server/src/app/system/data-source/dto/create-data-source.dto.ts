import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsNotEmpty } from 'class-validator'

@Injectable()
export class CreateDataSourceDto {
  @ApiProperty({ description: '数据库类型' })
  @IsString()
  @IsNotEmpty({ message: 'type not null' })
  public type: string

  @ApiProperty({ description: '数据库IP' })
  @IsString()
  @IsNotEmpty({ message: 'host not null' })
  public host: string

  @ApiProperty({ description: '数据库端口号' })
  @IsString()
  @IsNotEmpty({ message: 'port not null' })
  public port: string

  @ApiProperty({ description: '数据库密码' })
  @IsString()
  @IsNotEmpty({ message: 'password not null' })
  public password: string

  @ApiProperty({ description: '数据库用户名' })
  @IsString()
  @IsNotEmpty({ message: 'username not null' })
  public username: string
}
