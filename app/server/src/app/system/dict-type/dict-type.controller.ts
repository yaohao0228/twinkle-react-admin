import { Body, Controller, Post, Put } from '@nestjs/common'
import { UpdateResult } from 'typeorm'
import { ApiTags, ApiOperation, ApiOkResponse } from '@nestjs/swagger'

import { DictTypeService } from './dict-type.service'

import { ResponseDto } from 'src/common/dto/response.dto'
import { CreateDictTypeDto } from './dto/create-dict-type.dto'
import { UpdateDictTypeDto } from './dto/update-dict-type.dto'

import { UnityController } from 'src/common/unity/unity.controller'
import { Permission, PermissionPrefix } from 'src/common/decorators/permission.decorator'
import { PermissionAction } from 'src/common/enum/permission-action.enum'

@ApiTags('字典类型')
@PermissionPrefix('dict_type')
@Controller({ path: 'system/dictType', version: '1' })
export class DictTypeController extends UnityController<CreateDictTypeDto, UpdateDictTypeDto, DictTypeService> {
  constructor(private readonly dictTypeService: DictTypeService) {
    super(dictTypeService)
  }

  @ApiOperation({ summary: '保存' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.CREATE)
  @Post('save')
  public save(@Body() dto: CreateDictTypeDto): Promise<any> {
    return this.dictTypeService.save(dto)
  }

  @ApiOperation({ summary: '修改' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.UPDATE)
  @Put()
  public update(@Body() dto: UpdateDictTypeDto): Promise<UpdateResult> {
    return this.dictTypeService.update(dto)
  }
}
