import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { DictTypeService } from './dict-type.service'
import { DictTypeController } from './dict-type.controller'
import { Define } from 'src/common/decorators/define.decorator'

import { DictTypeEntity } from 'src/entities/system/dict-type.entity'

import { DictDataEntity } from 'src/entities/system/dict-data.entity'

@Define(['Module'])
@Module({
  imports: [TypeOrmModule.forFeature([DictTypeEntity, DictDataEntity])],
  controllers: [DictTypeController],
  providers: [DictTypeService]
})
export class DictTypeModule {}
