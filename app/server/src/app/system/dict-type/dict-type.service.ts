import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { DataSource, Repository } from 'typeorm'

import { DictTypeServiceImpl } from './impl/dict-type.service.impl'
import { CreateDictTypeDto } from './dto/create-dict-type.dto'
import { UpdateDictTypeDto } from './dto/update-dict-type.dto'

import { UnityService } from 'src/common/unity/unity.service'
import { DictTypeEntity } from 'src/entities/system/dict-type.entity'

import { DictDataEntity } from 'src/entities/system/dict-data.entity'

@Injectable()
export class DictTypeService
  extends UnityService<DictTypeEntity, CreateDictTypeDto, UpdateDictTypeDto>
  implements DictTypeServiceImpl
{
  constructor(
    public readonly dataSource: DataSource,
    @InjectRepository(DictTypeEntity)
    public readonly dictTypeRepository: Repository<DictTypeEntity>,

    @InjectRepository(DictDataEntity)
    public readonly dictDataRepository: Repository<DictDataEntity>
  ) {
    super(dataSource, dictTypeRepository, {
      key: 'dictType',
      relations: {
        dictData: dictDataRepository
      }
    })
  }
}
