import { Injectable } from '@nestjs/common'
import { PartialType } from '@nestjs/mapped-types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

import { CreateDictTypeDto } from './create-dict-type.dto'

@Injectable()
export class UpdateDictTypeDto extends PartialType(CreateDictTypeDto) {
  @ApiProperty({ description: 'id' })
  @IsString()
  @IsNotEmpty({ message: 'id not null' })
  public id: string
}
