import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsBoolean, IsNotEmpty, IsOptional } from 'class-validator'

import { DictDataEntity } from 'src/entities/system/dict-data.entity'

@Injectable()
export class CreateDictTypeDto {
  @ApiProperty({ description: '类型编码' })
  @IsString()
  @IsNotEmpty({ message: 'typeCode not null' })
  public typeCode: string

  @ApiProperty({ description: '类型名称' })
  @IsString()
  @IsNotEmpty({ message: 'typeName not null' })
  public typeName: string

  @ApiProperty({ description: '是否启用', default: true, required: false })
  @IsBoolean()
  @IsOptional()
  public isStart: boolean

  @ApiProperty()
  public dictData: DictDataEntity
}
