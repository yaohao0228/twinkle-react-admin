import { Injectable } from '@nestjs/common'
import { PartialType } from '@nestjs/mapped-types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

import { CreateMenuDto } from './create-menu.dto'

@Injectable()
export class UpdateMenuDto extends PartialType(CreateMenuDto) {
  @ApiProperty({ description: 'id' })
  @IsString()
  @IsNotEmpty({ message: 'id not null' })
  public id: string
}
