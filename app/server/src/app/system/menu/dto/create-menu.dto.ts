import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsBoolean, IsNotEmpty, IsOptional } from 'class-validator'

@Injectable()
export class CreateMenuDto {
  @ApiProperty({ description: '菜单编码' })
  @IsString()
  @IsNotEmpty({ message: 'code not null' })
  public code: string

  @ApiProperty({ description: '菜单名称' })
  @IsString()
  @IsNotEmpty({ message: 'name not null' })
  public name: string

  @ApiProperty({ description: '排序', default: 100 })
  @IsNotEmpty({ message: 'sort not null' })
  public sort: number

  @ApiProperty({ description: '菜单图标', required: false })
  @IsString()
  @IsOptional()
  public icon: string

  @ApiProperty({ description: '前端路由', required: false })
  @IsString()
  @IsOptional()
  public path: string

  @ApiProperty({ description: '文件路径', required: false })
  @IsString()
  @IsOptional()
  public element: string

  @ApiProperty({ description: '菜单类型' })
  @IsNotEmpty({ message: 'type not null' })
  public type: string

  @ApiProperty({ description: '是否显示', default: true })
  @IsBoolean()
  @IsNotEmpty({ message: 'isShow not null' })
  public isShow: boolean

  @ApiProperty({ description: '编辑选项' })
  @IsNotEmpty({ message: 'editOption not null' })
  public editOption: any

  @ApiProperty({ description: '父节点' })
  public parentId: number
}
