import { Body, Controller, Post, Put } from '@nestjs/common'
import { UpdateResult } from 'typeorm'
import { ApiTags, ApiOperation, ApiOkResponse } from '@nestjs/swagger'

import { MenuService } from './menu.service'

import { ResponseDto } from 'src/common/dto/response.dto'
import { CreateMenuDto } from './dto/create-menu.dto'
import { UpdateMenuDto } from './dto/update-menu.dto'

import { UnityController } from 'src/common/unity/unity.controller'
import { Permission, PermissionPrefix } from 'src/common/decorators/permission.decorator'
import { PermissionAction } from 'src/common/enum/permission-action.enum'

@ApiTags('菜单管理')
@PermissionPrefix('sys_menu')
@Controller({ path: 'system/menu', version: '1' })
export class MenuController extends UnityController<CreateMenuDto, UpdateMenuDto, MenuService> {
  constructor(private readonly menuService: MenuService) {
    super(menuService)
  }

  @ApiOperation({ summary: '保存' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.CREATE)
  @Post('save')
  public save(@Body() dto: CreateMenuDto): Promise<any> {
    return this.menuService.save(dto)
  }

  @ApiOperation({ summary: '修改' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.UPDATE)
  @Put()
  public update(@Body() dto: UpdateMenuDto): Promise<UpdateResult> {
    return this.menuService.update(dto)
  }
}
