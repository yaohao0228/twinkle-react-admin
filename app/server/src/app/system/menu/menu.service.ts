import { Injectable } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { InjectRepository } from '@nestjs/typeorm'
import { DataSource, Repository } from 'typeorm'

import { PermissionAction } from 'src/common/enum/permission-action.enum'
import { UnityService } from 'src/common/unity/unity.service'
import { MenuEntity, MenuType } from 'src/entities/system/menu.entity'

import { CreateMenuDto } from './dto/create-menu.dto'
import { UpdateMenuDto } from './dto/update-menu.dto'
import { isEmpty, isNotEmpty } from 'class-validator'
import { CustomException } from 'src/common/exceptions/exception'
import { MenuServiceImpl } from './impl/menu.service.impl'

@Injectable()
export class MenuService extends UnityService<MenuEntity, CreateMenuDto, UpdateMenuDto> implements MenuServiceImpl {
  constructor(
    public readonly reflector: Reflector,
    public readonly dataSource: DataSource,
    @InjectRepository(MenuEntity)
    public readonly menuRepository: Repository<MenuEntity>
  ) {
    super(dataSource, menuRepository)
  }

  public async beforeSave<T extends CreateMenuDto>(dto: T): Promise<T> {
    if (dto.type === MenuType.PERMISSION && isEmpty(dto.parentId)) {
      throw new CustomException(10005)
    }
    const menu = await this.menuRepository.findOne({
      where: [{ code: dto.code }]
    })
    if (isNotEmpty(menu)) throw new CustomException(10000)
    return dto
  }

  public async afterSave<T extends MenuEntity>(insertResult: T): Promise<T> {
    if (insertResult.type === MenuType.PERMISSION) return insertResult
    // 获取权限前缀
    const createPermission = this.menuRepository.create({
      name: '新增',
      code: `${insertResult.code}_${PermissionAction.CREATE}`,
      type: MenuType.PERMISSION,
      parentId: insertResult.id
    })
    const updatePermission = this.menuRepository.create({
      name: '修改',
      code: `${insertResult.code}_${PermissionAction.UPDATE}`,
      type: MenuType.PERMISSION,
      parentId: insertResult.id
    })
    const deletePermission = this.menuRepository.create({
      name: '删除',
      code: `${insertResult.code}_${PermissionAction.DELETE}`,
      type: MenuType.PERMISSION,
      parentId: insertResult.id
    })
    const readPermission = this.menuRepository.create({
      name: '查看',
      code: `${insertResult.code}_${PermissionAction.READ}`,
      type: MenuType.PERMISSION,
      parentId: insertResult.id
    })
    const importPermission = this.menuRepository.create({
      name: '导入',
      code: `${insertResult.code}_${PermissionAction.IMPORT}`,
      type: MenuType.PERMISSION,
      parentId: insertResult.id
    })
    const exportPermission = this.menuRepository.create({
      name: '导出',
      code: `${insertResult.code}_${PermissionAction.EXPORT}`,
      type: MenuType.PERMISSION,
      parentId: insertResult.id
    })
    const menus = [
      createPermission,
      updatePermission,
      deletePermission,
      readPermission,
      importPermission,
      exportPermission
    ]
    await this.menuRepository.save(menus)
    return insertResult
  }
}
