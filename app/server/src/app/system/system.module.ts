import { join } from 'path'
import { Require } from 'src/shared/utils/require-all.util'
import { Module } from 'src/common/decorators/module.decorator'

@Module({
  imports: Require.requireAllInNest({ dirname: join(__dirname, './'), currentFile: __filename }, 'Module')
})
export class SystemModule {}
