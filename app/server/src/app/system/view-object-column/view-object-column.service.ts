import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { DataSource, Repository } from 'typeorm'

import { ViewObjectColumnServiceImpl } from './impl/view-object-column.service.impl'
import { CreateViewObjectColumnDto } from './dto/create-view-object-column.dto'
import { UpdateViewObjectColumnDto } from './dto/update-view-object-column.dto'

import { ViewObjectColumnEntity } from 'src/entities/system/view-object-column.entity'
import { UnityService } from 'src/common/unity/unity.service'

@Injectable()
export class ViewObjectColumnService
  extends UnityService<ViewObjectColumnEntity, CreateViewObjectColumnDto, UpdateViewObjectColumnDto>
  implements ViewObjectColumnServiceImpl
{
  constructor(
    public readonly dataSource: DataSource,
    @InjectRepository(ViewObjectColumnEntity)
    public readonly viewObjectColumnRepository: Repository<ViewObjectColumnEntity>
  ) {
    super(dataSource, viewObjectColumnRepository)
  }
}
