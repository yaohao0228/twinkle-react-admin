import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ViewObjectColumnService } from './view-object-column.service'
import { ViewObjectColumnController } from './view-object-column.controller'
import { ViewObjectColumnEntity } from 'src/entities/system/view-object-column.entity'

import { Define } from 'src/common/decorators/define.decorator'

@Define(['Module'])
@Module({
  imports: [TypeOrmModule.forFeature([ViewObjectColumnEntity])],
  controllers: [ViewObjectColumnController],
  providers: [ViewObjectColumnService]
})
export class ViewObjectModule {}
