import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsBoolean, IsNotEmpty, IsOptional } from 'class-validator'

import { ViewObjectEntity } from 'src/entities/system/view-object.entity'

@Injectable()
export class CreateViewObjectColumnDto {
  @ApiProperty({ description: '字段名' })
  @IsString()
  @IsNotEmpty({ message: 'name not null' })
  public name: string

  @ApiProperty({ description: '字段编码' })
  @IsString()
  @IsNotEmpty({ message: 'code not null' })
  public code: string

  @ApiProperty({ description: '排序', default: 10 })
  @IsNotEmpty({ message: 'rank not null' })
  public rank: number

  @ApiProperty({ description: '列宽度', default: 150 })
  @IsNotEmpty({ message: 'width not null' })
  public width: number

  @ApiProperty({ description: '编辑类型' })
  @IsNotEmpty({ message: 'editType not null' })
  public editType: string

  @ApiProperty({ description: '是否可搜索', default: true })
  @IsBoolean()
  @IsNotEmpty({ message: 'isSearch not null' })
  public isSearch: boolean

  @ApiProperty({ description: '是否显示', default: true })
  @IsBoolean()
  @IsNotEmpty({ message: 'isShow not null' })
  public isShow: boolean

  @ApiProperty({ description: '是否必填', default: false })
  @IsBoolean()
  @IsNotEmpty({ message: 'isRequired not null' })
  public isRequired: boolean

  @ApiProperty({ description: '编辑选项' })
  @IsNotEmpty({ message: 'editOption not null' })
  public editOption: string

  @ApiProperty({ description: '备注', required: false })
  @IsString()
  @IsOptional()
  public comment: string

  @ApiProperty()
  public viewObject: ViewObjectEntity
}
