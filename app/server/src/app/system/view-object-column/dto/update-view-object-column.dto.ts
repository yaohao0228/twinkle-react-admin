import { Injectable } from '@nestjs/common'
import { PartialType } from '@nestjs/mapped-types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

import { CreateViewObjectColumnDto } from './create-view-object-column.dto'

@Injectable()
export class UpdateViewObjectColumnDto extends PartialType(CreateViewObjectColumnDto) {
  @ApiProperty({ description: 'id' })
  @IsString()
  @IsNotEmpty({ message: 'id not null' })
  public id: string
}
