import { Controller, Post, Body } from '@nestjs/common'
import { ApiTags, ApiOperation, ApiOkResponse } from '@nestjs/swagger'

import { ViewObjectColumnService } from './view-object-column.service'

import { CreateViewObjectColumnDto } from './dto/create-view-object-column.dto'
import { UpdateViewObjectColumnDto } from './dto/update-view-object-column.dto'

import { UnityController } from 'src/common/unity/unity.controller'
import { Permission, PermissionPrefix } from 'src/common/decorators/permission.decorator'
import { ResponseDto } from 'src/common/dto/response.dto'
import { PermissionAction } from 'src/common/enum/permission-action.enum'

@ApiTags('视图对象字段管理')
@PermissionPrefix('view_object_column')
@Controller({ path: 'system/viewObjectColumn', version: '1' })
export class ViewObjectColumnController extends UnityController<
  CreateViewObjectColumnDto,
  UpdateViewObjectColumnDto,
  ViewObjectColumnService
> {
  constructor(private readonly viewObjectColumnService: ViewObjectColumnService) {
    super(viewObjectColumnService)
  }

  @ApiOperation({ summary: '保存' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.CREATE)
  @Post('save')
  public save(@Body() dto: CreateViewObjectColumnDto): Promise<any> {
    return this.viewObjectColumnService.save(dto)
  }
}
