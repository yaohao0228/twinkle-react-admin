import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { DataSource, In, Repository } from 'typeorm'
import { exec } from 'child_process'
import { promisify } from 'util'
import * as path from 'path'

import { DataEntityServiceImpl } from './impl/data-entity.service.impl'
import { CreateDataEntityDto } from './dto/create-data-entity.dto'
import { UpdateDataEntityDto } from './dto/update-data-entity.dto'

import { DataEntityEntity } from 'src/entities/system/data-entity.entity'
import { UnityService } from 'src/common/unity/unity.service'
import { DataEntityColumnEntity } from 'src/entities/system/data-entity-column.entity'
import { FileAction } from 'src/shared/utils/file-action.util'
import { StringAction } from 'src/shared/utils/string-action.util'
import type { GenerateData } from './interface/generate-data'
import { GenerateCodeDto } from './dto/generate-code.dto'
import { SyncViewObjectDto } from './dto/sync-view-object.dto'
import { ViewObjectService } from '../view-object/view-object.service'
import { CreateViewObjectDto } from '../view-object/dto/create-view-object.dto'
import { EditType, ViewObjectColumnEntity } from 'src/entities/system/view-object-column.entity'

@Injectable()
export class DataEntityService
  extends UnityService<DataEntityEntity, CreateDataEntityDto, UpdateDataEntityDto>
  implements DataEntityServiceImpl
{
  constructor(
    public readonly dataSource: DataSource,
    @InjectRepository(DataEntityEntity)
    public readonly dataEntityRepository: Repository<DataEntityEntity>,
    @InjectRepository(DataEntityColumnEntity)
    public readonly dataEntityColumnRepository: Repository<DataEntityColumnEntity>,
    public readonly viewObjectService: ViewObjectService
  ) {
    super(dataSource, dataEntityRepository, {
      key: 'dataEntity',
      relations: { dataEntityColumn: dataEntityColumnRepository }
    })
  }
  public async syncViewObject(dto: SyncViewObjectDto): Promise<string> {
    const entities = await this.dataEntityRepository.find({
      where: { id: In(dto.ids) },
      relations: { dataEntityColumn: true }
    })

    const defaultEditOption = (type: string) => {
      const strategy = {
        string: {},
        bool: { options: { true: '是', false: '否' } }
      }
      return strategy[type]
    }

    const defaultEditType = (type: string) => {
      const strategy = {
        string: EditType.STRING,
        boolean: EditType.BOOL,
        enum: EditType.ENUM,
        any: EditType.STRING
      }
      return strategy[type]
    }

    for (const index in entities) {
      const entity = entities[index]
      // 跳过已经同步过的
      if (entity.isSyncVo) break
      const createViewObjectDto: CreateViewObjectDto = {
        code: entity.code,
        name: entity.name,
        service: entity.service,
        viewObjectColumn: {
          create: entity.dataEntityColumn.map<Partial<ViewObjectColumnEntity>>((column) => ({
            code: column.code,
            name: column.name,
            type: column.type,
            editType: defaultEditType(column.dataType),
            editOption: defaultEditOption(column.type)
          })),
          update: [],
          delete: []
        } as any
      }
      entity.isSyncVo = true

      await this.viewObjectService.save(createViewObjectDto)
      await entity.save()
    }

    return '同步成功'
  }

  public async generateCode(dto: GenerateCodeDto) {
    const dataEntity = await this.findById(dto.id)

    // 生成的数据
    const generateData: GenerateData = {
      ...dataEntity,
      fileName: StringAction.kebabCase(dataEntity.code),
      camelFileName: StringAction.camelCase(dataEntity.code, false),
      uppercaseFileName: StringAction.camelCase(dataEntity.code),
      // 处理关系字段
      relations: dataEntity.dataEntityColumn
        .filter((column) => column.dataType === 'relation')
        .map((column) => ({
          uppercaseCode: StringAction.camelCase(column.code),
          camelCode: StringAction.camelCase(column.code, false),
          kebabCode: StringAction.kebabCase(column.code),
          relation: column.type
        }))
    }

    const targetMap: Record<string, { template: string; folderPath: string }> = {
      module: {
        template: 'template.module.ejs',
        folderPath: `src/app/${dataEntity.service}/${generateData.fileName}`
      },
      controller: {
        template: 'template.controller.ejs',
        folderPath: `src/app/${dataEntity.service}/${generateData.fileName}`
      },
      service: {
        template: 'template.service.ejs',
        folderPath: `src/app/${dataEntity.service}/${generateData.fileName}`
      },
      serviceImpl: {
        template: 'template.service.impl.ejs',
        folderPath: `src/app/${generateData.service}/${generateData.fileName}/impl`
      },
      create_dto: {
        template: 'create.dto.ejs',
        folderPath: `src/app/${generateData.service}/${generateData.fileName}/dto`
      },
      update_dto: {
        template: 'update.dto.ejs',
        folderPath: `src/app/${generateData.service}/${generateData.fileName}/dto`
      },
      entity: {
        template: 'template.entity.ejs',
        folderPath: `src/entities/${generateData.service}`
      }
    }

    // 需要生成的文件
    const generateTemplates = dto.targetList.map((target) => targetMap[target])
    /**
     * 处理文件路径
     */
    const handlerTargetPath = (target: (typeof targetMap)['module']): string => {
      if (target.template.includes('dto')) {
        const type = target.template.split('.')[0]
        return path.resolve(target.folderPath, `${type}-${generateData.fileName}.dto.ts`)
      } else {
        const fileName = target.template.replace(/template/g, generateData.fileName).replace(/ejs/g, 'ts')
        return path.resolve(target.folderPath, fileName)
      }
    }

    generateTemplates.forEach(async (target) => {
      const data = await FileAction.compile(target.template, generateData)

      if (FileAction.createDirSync(target.folderPath)) {
        const targetPath = handlerTargetPath(target)
        await FileAction.writeToFile(targetPath, data)
        const execAsync = promisify(exec)
        await execAsync(`npx prettier --write "${targetPath}"`)
        await execAsync(`npx eslint --fix ${targetPath}`)
      }
    })

    return `${dataEntity.name}代码生成成功`
  }

  public async afterSave<T extends DataEntityEntity>(insertResult: T): Promise<T> {
    const baseEntityColumns: Partial<DataEntityColumnEntity>[] = [
      {
        code: 'id',
        name: '主键',
        type: 'number',
        dataType: 'number',
        nullable: false,
        unique: true,
        dataEntity: insertResult
      },
      {
        code: 'version',
        name: '数据版本',
        type: 'number',
        dataType: 'number',
        nullable: false,
        dataEntity: insertResult
      },
      {
        code: 'createTime',
        name: '创建时间',
        type: 'string',
        dataType: 'string',
        nullable: false,
        dataEntity: insertResult
      },
      {
        code: 'updateTime',
        name: '更新时间',
        type: 'string',
        dataType: 'string',
        nullable: false,
        dataEntity: insertResult
      }
    ]
    baseEntityColumns.forEach(async (column) => await this.dataEntityColumnRepository.save(column))

    return insertResult
  }
}
