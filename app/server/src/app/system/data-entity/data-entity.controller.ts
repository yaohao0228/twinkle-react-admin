import { Controller, Post, Body } from '@nestjs/common'
import { ApiTags, ApiOperation, ApiOkResponse } from '@nestjs/swagger'

import { DataEntityService } from './data-entity.service'

import { ResponseDto } from 'src/common/dto/response.dto'
import { CreateDataEntityDto } from './dto/create-data-entity.dto'
import { UpdateDataEntityDto } from './dto/update-data-entity.dto'

import { UnityController } from 'src/common/unity/unity.controller'
import { Permission, PermissionPrefix } from 'src/common/decorators/permission.decorator'
import { PermissionAction } from 'src/common/enum/permission-action.enum'
import { GenerateCodeDto } from './dto/generate-code.dto'
import { SyncViewObjectDto } from './dto/sync-view-object.dto'

@ApiTags('数据实体管理')
@PermissionPrefix('data_entity')
@Controller({ path: 'system/dataEntity', version: '1' })
export class DataEntityController extends UnityController<CreateDataEntityDto, UpdateDataEntityDto, DataEntityService> {
  constructor(private readonly dataEntityService: DataEntityService) {
    super(dataEntityService)
  }

  @ApiOperation({ summary: '保存' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.CREATE)
  @Post('save')
  public save(@Body() dto: CreateDataEntityDto): Promise<any> {
    return this.dataEntityService.save(dto)
  }

  @ApiOperation({ summary: '生成代码' })
  @ApiOkResponse({ description: '成功', type: ResponseDto<string> })
  @Permission('generate_code')
  @Post('generateCode')
  public async generateCode(@Body() dto: GenerateCodeDto): Promise<string> {
    return this.dataEntityService.generateCode(dto)
  }

  @ApiOperation({ summary: '同步VO对象' })
  @ApiOkResponse({ description: '成功', type: ResponseDto<string> })
  @Permission('sync_view_object')
  @Post('syncViewObject')
  public async syncViewObject(@Body() dto: SyncViewObjectDto): Promise<string> {
    return this.dataEntityService.syncViewObject(dto)
  }
}
