import { GenerateCodeDto } from '../dto/generate-code.dto'
import { SyncViewObjectDto } from '../dto/sync-view-object.dto'

export abstract class DataEntityServiceImpl {
  /**
   * 生成代码
   */
  public abstract generateCode(dto: GenerateCodeDto): Promise<string>

  /**
   * 同步VO对象
   */
  public abstract syncViewObject(dto: SyncViewObjectDto): Promise<string>
}
