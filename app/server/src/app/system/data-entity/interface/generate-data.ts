import { DataEntityEntity } from 'src/entities/system/data-entity.entity'

type relationCode = 'oneToMany' | 'manyToOne' | 'oneToOne' | 'manyToMany'

export interface Relation {
  /**
   * 大写驼峰代码
   */
  uppercaseCode: string
  /**
   * 驼峰代码
   */
  camelCode: string
  /**
   * 短横线代码
   */
  kebabCode: string
  /**
   * 关系
   */
  relation: relationCode | string
}

export interface GenerateData extends Partial<DataEntityEntity> {
  /**
   * 文件名
   */
  fileName: string

  /**
   * 大写驼峰文件名
   */
  uppercaseFileName: string

  /**
   * 驼峰文件名
   */
  camelFileName: string

  /**
   * 实体关系
   */
  relations: Array<Relation>
}
