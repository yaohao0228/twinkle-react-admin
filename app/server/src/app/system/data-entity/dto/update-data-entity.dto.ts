import { PartialType } from '@nestjs/mapped-types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

import { CreateDataEntityDto } from './create-data-entity.dto'
import { Injectable } from '@nestjs/common'

@Injectable()
export class UpdateDataEntityDto extends PartialType(CreateDataEntityDto) {
  @ApiProperty({ description: 'id' })
  @IsString()
  @IsNotEmpty({ message: 'id不能为空' })
  public id: string
}
