import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { RelationDto } from 'src/common/dto/relation.dto'

@Injectable()
export class CreateDataEntityDto {
  @ApiProperty({ description: '实体代码' })
  public code: string

  @ApiProperty({ description: '实体名称' })
  public name: string

  @ApiProperty({ description: '表名' })
  public tableName: string

  @ApiProperty({ description: '接口服务' })
  public service: string

  @ApiProperty({ description: '权限点前缀' })
  public permissionPrefix: string

  @ApiProperty({ description: '实体字段', type: RelationDto })
  public dataEntityColumn: RelationDto
}
