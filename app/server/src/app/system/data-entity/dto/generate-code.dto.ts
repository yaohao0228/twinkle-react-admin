import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsArray, IsNotEmpty, IsString } from 'class-validator'

@Injectable()
export class GenerateCodeDto {
  @ApiProperty({ description: '实体id' })
  @IsString()
  @IsNotEmpty()
  public id: string

  @ApiProperty({
    description: '文件名集合',
    default: ['module', 'controller', 'service', 'serviceImpl', 'create_dto', 'update_dto', 'entity']
  })
  @IsArray()
  @IsNotEmpty()
  public targetList: string[]
}
