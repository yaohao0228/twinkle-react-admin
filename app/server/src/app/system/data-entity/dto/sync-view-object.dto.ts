import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsArray, IsNotEmpty } from 'class-validator'

@Injectable()
export class SyncViewObjectDto {
  @ApiProperty({ description: '实体id集合' })
  @IsArray()
  @IsNotEmpty({ message: 'ids not null' })
  public ids: string[]
}
