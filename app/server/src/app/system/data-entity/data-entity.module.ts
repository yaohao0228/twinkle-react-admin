import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { DataEntityService } from './data-entity.service'
import { DataEntityController } from './data-entity.controller'
import { DataEntityEntity } from 'src/entities/system/data-entity.entity'
import { DataEntityColumnEntity } from 'src/entities/system/data-entity-column.entity'
import { Define } from 'src/common/decorators/define.decorator'
import { ViewObjectModule } from '../view-object/view-object.module'

@Define(['Module'])
@Module({
  imports: [TypeOrmModule.forFeature([DataEntityEntity, DataEntityColumnEntity]), ViewObjectModule],
  controllers: [DataEntityController],
  providers: [DataEntityService]
})
export class DataEntityModule {}
