import { Module } from '@nestjs/common'
import { RoleService } from './role.service'
import { RoleController } from './role.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { RoleEntity } from 'src/entities/oauth/role.entity'
import { Define } from 'src/common/decorators/define.decorator'

@Define(['Module'])
@Module({
  imports: [TypeOrmModule.forFeature([RoleEntity])],
  controllers: [RoleController],
  providers: [RoleService]
})
export class RoleModule {}
