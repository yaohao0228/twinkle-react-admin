import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsBoolean, IsNotEmpty } from 'class-validator'

@Injectable()
export class CreateRoleDto {
  @ApiProperty({ description: '角色代码' })
  @IsString()
  @IsNotEmpty({ message: 'code not null' })
  public code: string

  @ApiProperty({ description: '角色名称' })
  @IsString()
  @IsNotEmpty({ message: 'name not null' })
  public name: string

  @ApiProperty({ description: '是否系统默认角色', default: false })
  @IsBoolean()
  @IsNotEmpty({ message: 'isDefault not null' })
  public isDefault: boolean

  @ApiProperty({ description: '备注' })
  @IsString()
  @IsNotEmpty({ message: 'remark not null' })
  public remark: string
}
