import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { UnityService } from 'src/common/unity/unity.service'
import { RoleEntity } from 'src/entities/oauth/role.entity'
import { DataSource, In, Repository } from 'typeorm'
import { UpdateRoleDto } from './dto/update-role.dto'
import { CreateRoleDto } from './dto/create-role.dto'
import { isNotEmpty } from 'class-validator'
import { CustomException } from 'src/common/exceptions/exception'
import { RoleServiceImpl } from './impl/role.service.impl'
import { CustomRemoveDto } from 'src/common/dto/custom-remove.dto'

@Injectable()
export class RoleService extends UnityService<RoleEntity, CreateRoleDto, UpdateRoleDto> implements RoleServiceImpl {
  constructor(
    public readonly dataSource: DataSource,
    @InjectRepository(RoleEntity)
    private readonly roleRepository: Repository<RoleEntity>
  ) {
    super(dataSource, roleRepository)
  }

  public async beforeSave<T extends CreateRoleDto>(dto: T): Promise<T> {
    const role = await this.roleRepository.findOne({ where: { code: dto.code } })
    if (isNotEmpty(role)) throw new CustomException(10000)
    // 默认角色处理
    if (dto.isDefault) {
      const defaultRole = await this.roleRepository.findOne({ where: { isDefault: true } })
      defaultRole.isDefault = false
      defaultRole.save()
    }
    return dto
  }

  public async beforeUpdate<T extends UpdateRoleDto>(dto: T): Promise<T> {
    // 默认角色处理
    if (dto.isDefault) {
      const defaultRole = await this.roleRepository.findOne({ where: { isDefault: true } })
      if (isNotEmpty(defaultRole)) {
        defaultRole.isDefault = false
        defaultRole.save()
      }
    }
    return dto
  }

  public async beforeDelete<T extends CustomRemoveDto>(dto: T): Promise<T> {
    // 判断需要删除的角色是否含有用户
    const roles = await this.roleRepository.find({ where: { id: In(dto.ids) }, relations: { users: true } })
    for (const role of roles) {
      if (role.users.length > 0) {
        throw new CustomException(10008)
      }
    }
    return dto
  }
}
