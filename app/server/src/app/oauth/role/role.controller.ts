import { Body, Controller, Post } from '@nestjs/common'
import { RoleService } from './role.service'
import { CreateRoleDto } from './dto/create-role.dto'
import { UpdateRoleDto } from './dto/update-role.dto'
import { UnityController } from 'src/common/unity/unity.controller'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { Permission, PermissionPrefix } from 'src/common/decorators/permission.decorator'
import { OAUTH_PREFIX } from '../oauth.constants'
import { ResponseDto } from 'src/common/dto/response.dto'
import { PermissionAction } from 'src/common/enum/permission-action.enum'

@ApiTags('角色管理')
@PermissionPrefix('sys_role')
@Controller({ path: `${OAUTH_PREFIX}/role`, version: '1' })
export class RoleController extends UnityController<CreateRoleDto, UpdateRoleDto, RoleService> {
  constructor(private readonly roleService: RoleService) {
    super(roleService)
  }

  @ApiOperation({ summary: '保存' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.CREATE)
  @Post('save')
  public save(@Body() dto: CreateRoleDto): Promise<any> {
    return this.roleService.save(dto)
  }
}
