import { join } from 'path'
import { Module } from 'src/common/decorators/module.decorator'
import { Require } from 'src/shared/utils/require-all.util'

@Module({
  imports: Require.requireAllInNest({ dirname: join(__dirname, './'), currentFile: __filename }, 'Module')
})
export class OauthModule {}
