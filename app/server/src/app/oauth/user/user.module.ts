import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule } from '@nestjs/config'

import { UserController } from './user.controller'
import { UserService } from './user.service'
import { UserEntity } from 'src/entities/oauth/user.entity'
import { RoleEntity } from 'src/entities/oauth/role.entity'
import { Define } from 'src/common/decorators/define.decorator'

@Define(['Module'])
@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, RoleEntity]), ConfigModule],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService]
})
export class UserModule {}
