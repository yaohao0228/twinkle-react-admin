
import { UserEntity } from 'src/entities/oauth/user.entity';
import { RestPasswordDto } from '../dto/reset-password.dto'

export abstract class UserServiceImpl {
  /**
   * 根据用户名查询
   */
  public abstract findByUsername(username: string): Promise<UserEntity>

  /**
   * 重置用户密码
   */
  public abstract resetPassword(dto: RestPasswordDto): Promise<void>
}
