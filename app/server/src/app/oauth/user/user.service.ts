import { Injectable, OnApplicationBootstrap } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { ConfigService } from '@nestjs/config'
import { DataSource, Repository } from 'typeorm'

import { JwtConfigKeysPaths } from 'config/jwt-config'
import { UnityService } from 'src/common/unity/unity.service'
import { Crypto } from 'src/shared/utils/crypto.util'
import { CRYPTO_SECRET } from '../oauth.constants'
import { isEmpty, isNotEmpty } from 'class-validator'
import { CreateUserDto } from './dto/create-user.dto'
import { UpdateUserDto } from './dto/update-user.dto'
import { CustomException } from 'src/common/exceptions/exception'
import { CustomRemoveDto } from 'src/common/dto/custom-remove.dto'
import { RestPasswordDto } from './dto/reset-password.dto'
import { UserServiceImpl } from './impl/user.service.impl'
import { UserEntity } from 'src/entities/oauth/user.entity'
import { RoleEntity } from 'src/entities/oauth/role.entity'

@Injectable()
export class UserService
  extends UnityService<UserEntity, CreateUserDto, UpdateUserDto>
  implements UserServiceImpl, OnApplicationBootstrap
{
  constructor(
    public readonly dataSource: DataSource,
    public readonly configService: ConfigService<JwtConfigKeysPaths>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(RoleEntity)
    private readonly roleRepository: Repository<RoleEntity>
  ) {
    super(dataSource, userRepository, { relations: { roles: roleRepository } })
  }

  async onApplicationBootstrap() {
    const defaultAdmin = this.configService.get<string>('admin.defaultAdmin')
    const defaultAdminPhone = this.configService.get<string>('admin.defaultAdminPhone')
    const defaultAdminEmail = this.configService.get<string>('admin.defaultAdminEmail')
    const admin = await this.findByUsername(defaultAdmin)
    if (isEmpty(admin)) {
      this.save({ username: defaultAdmin, phone: defaultAdminPhone, email: defaultAdminEmail })
    }
  }

  public async beforeSave<T extends CreateUserDto>(dto: T): Promise<T> {
    const user = await this.userRepository.findOne({
      where: [{ username: dto.username }, { phone: dto.phone }]
    })
    if (isNotEmpty(user)) throw new CustomException(10000)
    const defaultPassword = this.configService.get<string>('jwt.defaultPassword')
    dto.password = Crypto.md5(`${defaultPassword}${CRYPTO_SECRET}`)
    return dto
  }

  public async afterSave<T extends UserEntity>(insertResult: T): Promise<T> {
    // 获取默认角色
    const defaultRole = await this.roleRepository.findOne({ where: { isDefault: true } })
    if (isNotEmpty(defaultRole)) {
      insertResult.roles = [defaultRole]
      return insertResult.save()
    }
    return insertResult
  }

  public async beforeUpdate<T extends UpdateUserDto>(dto: T): Promise<T> {
    // 判断是否修改管理员
    const defaultAdmin = this.configService.get<string>('admin.defaultAdmin')
    const admin = await this.findByUsername(defaultAdmin)
    if (String(dto.id) === String(admin.id)) throw new CustomException(10016)
    // 判断是否修改密码
    if (isNotEmpty(dto.password)) {
      dto.password = Crypto.md5(`${dto.password}${CRYPTO_SECRET}`)
    }
    return dto
  }

  public async beforeDelete<T extends CustomRemoveDto>(dto: T): Promise<T> {
    // 判断是否删除管理员
    const defaultAdmin = this.configService.get<string>('admin.defaultAdmin')
    const admin = await this.findByUsername(defaultAdmin)
    if (dto.ids.includes(admin.id)) throw new CustomException(10016)
    return dto
  }

  public async findByUsername(username: string): Promise<UserEntity> {
    return this.userRepository.findOne({ where: { username }, relations: { roles: true } })
  }

  public async findByOpenId(openid: string): Promise<UserEntity> {
    return this.userRepository.findOne({ where: { openid }, relations: { roles: true } })
  }

  public async resetPassword(dto: RestPasswordDto) {
    const defaultPassword = this.configService.get<string>('jwt.defaultPassword')
    super.update({
      id: dto.id,
      password: Crypto.md5(`${defaultPassword}${CRYPTO_SECRET}`)
    })
  }
}
