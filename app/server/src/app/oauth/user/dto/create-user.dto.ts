import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsNotEmpty, IsOptional, IsMobilePhone, IsEmail } from 'class-validator'

@Injectable()
export class CreateUserDto {
  @ApiProperty({ description: '用户名' })
  @IsString()
  @IsNotEmpty({ message: 'username not null' })
  public username: string

  @ApiProperty({ description: '密码', required: false })
  @IsString()
  @IsOptional()
  public password?: string

  @ApiProperty({ description: '手机号', required: false })
  @IsMobilePhone('zh-CN')
  @IsOptional()
  public phone?: string

  @ApiProperty({ description: '邮箱', required: false })
  @IsEmail()
  @IsOptional()
  public email?: string

  @ApiProperty({ description: '头像', required: false })
  @IsString()
  @IsOptional()
  public avatar?: string

  @ApiProperty({ description: '姓名', required: false })
  @IsString()
  @IsOptional()
  public name?: string

  @ApiProperty({ description: '微信用户id', required: false })
  @IsString()
  @IsOptional()
  public openid?: string
}
