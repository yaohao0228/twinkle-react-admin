import { Injectable } from '@nestjs/common'
import { PartialType } from '@nestjs/mapped-types'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

import { CreateUserDto } from './create-user.dto'

@Injectable()
export class UpdateUserDto extends PartialType(CreateUserDto) {
  @ApiProperty({ description: 'id' })
  @IsString()
  @IsNotEmpty({ message: 'id not null' })
  public id: string
}
