import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class RestPasswordDto {
  @ApiProperty({ description: 'id' })
  @IsString()
  @IsNotEmpty({ message: 'id不能为空' })
  id: string
}
