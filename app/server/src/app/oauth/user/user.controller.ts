import { Body, Controller, Get, Post, Query } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'

import { UserService } from './user.service'
import { CreateUserDto } from './dto/create-user.dto'
import { UpdateUserDto } from './dto/update-user.dto'
import { RestPasswordDto } from './dto/reset-password.dto'

import { ResponseDto } from 'src/common/dto/response.dto'
import { UnityController } from 'src/common/unity/unity.controller'
import { PermissionAction } from 'src/common/enum/permission-action.enum'
import { Permission, PermissionPrefix } from 'src/common/decorators/permission.decorator'
import { OAUTH_PREFIX } from '../oauth.constants'

@ApiTags('用户管理')
@PermissionPrefix('sys_user')
@Controller({ path: `${OAUTH_PREFIX}/user`, version: '1' })
export class UserController extends UnityController<CreateUserDto, UpdateUserDto, UserService> {
  constructor(public readonly userService: UserService) {
    super(userService)
  }

  @ApiOperation({ summary: '保存' })
  @ApiOkResponse({ description: '成功', type: ResponseDto })
  @Permission(PermissionAction.CREATE)
  @Post('save')
  public save(@Body() dto: CreateUserDto): Promise<any> {
    return this.userService.save(dto)
  }

  @ApiOperation({ summary: '重置用户密码' })
  @Permission('reset_password')
  @Get('resetPassword')
  public resetPassword(@Query() dto: RestPasswordDto) {
    return this.userService.resetPassword(dto)
  }
}
