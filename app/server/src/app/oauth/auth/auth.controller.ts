import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common'
import { ApiBody, ApiOperation, ApiTags } from '@nestjs/swagger'
import { Request as RequestType } from 'express'

import { AuthService } from './auth.service'
import { CustomAuthGuard } from './guard/custom-auth.guard'
import { OAUTH_PREFIX } from '../oauth.constants'

import { Public } from 'src/common/decorators/public.decorator'
import { UserEntity } from 'src/entities/oauth/user.entity'
import { User } from 'src/common/decorators/user.decorator'
import { JwtUserDto } from 'src/common/dto/jwt-user.dto'
import { Permission, PermissionPrefix } from 'src/common/decorators/permission.decorator'

import { RoleAllocationDto } from './dto/role-allocation.dto'
import { MenuAllocationDto } from './dto/menu-allocation.dto'
import { LoginDto } from './dto/login.dto'
import { DepartmentAllocationDto } from './dto/department-allocation.dto'

@ApiTags('权限管理')
@PermissionPrefix('sys_auth')
@Controller({ path: `${OAUTH_PREFIX}/auth`, version: '1' })
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: '登录' })
  @ApiBody({ type: LoginDto })
  @Public()
  @UseGuards(CustomAuthGuard)
  @Post('login')
  async login(@Request() req: RequestType & { user: UserEntity }) {
    return this.authService.login(req.user)
  }

  @ApiOperation({ summary: '菜单分配' })
  @Permission('menu_allocation')
  @Post('menuAllocation')
  public menuAllocation(@Body() dto: MenuAllocationDto) {
    return this.authService.menuAllocation(dto)
  }

  @ApiOperation({ summary: '角色分配' })
  @Permission('role_allocation')
  @Post('roleAllocation')
  public roleAllocation(@Body() dto: RoleAllocationDto) {
    return this.authService.roleAllocation(dto)
  }

  @ApiOperation({ summary: '部门分配' })
  @Permission('department_allocation')
  @Post('departmentAllocation')
  public departmentAllocation(@Body() dto: DepartmentAllocationDto) {
    return this.authService.departmentAllocation(dto)
  }

  @ApiOperation({ summary: '获取用户权限' })
  @Get('findPermission')
  public findUserPermission(@User() user: JwtUserDto) {
    return this.authService.findUserPermission({ id: user.userId })
  }
}
