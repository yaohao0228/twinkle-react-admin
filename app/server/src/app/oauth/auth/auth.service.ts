import { Injectable } from '@nestjs/common'
import { UserService } from '../user/user.service'
import { JwtService } from '@nestjs/jwt'
import { Crypto } from 'src/shared/utils/crypto.util'
import { CRYPTO_SECRET, WXCHAT_LOGIN_URL } from '../oauth.constants'
import { UserEntity } from 'src/entities/oauth/user.entity'
import { RoleAllocationDto } from './dto/role-allocation.dto'
import { InjectRepository } from '@nestjs/typeorm'
import { In, Repository } from 'typeorm'
import { RoleEntity } from 'src/entities/oauth/role.entity'
import { FindUserPermissionDto } from './dto/find-user-permission.dto'
import { MenuEntity } from 'src/entities/system/menu.entity'
import { flatMapDeep, uniqBy } from 'lodash'
import { MenuAllocationDto } from './dto/menu-allocation.dto'
import { DepartmentAllocationDto } from './dto/department-allocation.dto'
import { DepartmentEntity } from 'src/entities/oauth/department.entity'
import { AuthServiceImpl } from './impl/auth.service.impl'
import { LoginVo } from './vo/login.vo'
import { LoginDto, LoginType } from './dto/login.dto'
import { ConfigService } from '@nestjs/config'
import { HttpService } from '@nestjs/axios'
import { GlobalConfigKeysPaths } from 'config/global-config'
import { WxLoginParams, WxLoginResponse } from './interface/wx-login-type'
import { isEmpty } from 'class-validator'
import { JwtConfigKeysPaths } from 'config/jwt-config'

@Injectable()
export class AuthService implements AuthServiceImpl {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService<GlobalConfigKeysPaths>,
    private readonly jwtConfigService: ConfigService<JwtConfigKeysPaths>,
    private readonly httpService: HttpService,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(RoleEntity)
    private readonly roleRepository: Repository<RoleEntity>,
    @InjectRepository(MenuEntity)
    private readonly menuRepository: Repository<MenuEntity>,
    @InjectRepository(DepartmentEntity)
    private readonly departmentRepository: Repository<DepartmentEntity>
  ) {}

  public async validateUser(dto: LoginDto): Promise<unknown> {
    // 密码登陆
    if (dto.login_type === LoginType.PASSWORD) {
      const user = await this.userService.findByUsername(dto.username)
      const cryptoPassword = Crypto.md5(`${dto.password}${CRYPTO_SECRET}`)
      if (user && user.password === cryptoPassword) {
        delete user.password
        return user
      }
      return null
    }
    // 微信登陆
    if (dto.login_type === LoginType.WECHAT) {
      const params: WxLoginParams = {
        appid: this.configService.get<string>('weChat.appid'),
        secret: this.configService.get<string>('weChat.secret'),
        js_code: dto.password,
        grant_type: 'authorization_code'
      }
      const queryString = Object.keys(params)
        .map((key) => key + '=' + params[key])
        .join('&')
      const {
        data: { openid }
      } = await this.httpService.axiosRef.get<WxLoginResponse>(`${WXCHAT_LOGIN_URL}${queryString}`)
      const user = await this.userService.findByOpenId(openid)
      if (isEmpty(user)) {
        const username = this.randomUsername()
        const entity = this.userRepository.create({
          username,
          openid
        })
        const user = await this.userService.save(entity)
        return user
      }
      return user
    }

    return null
  }

  public randomUsername(prefix: string = '', randomLength: number = 8): string {
    const nameArr: string[][] = [
      ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'],
      ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'g', 'k', 'l', 'm', 'n']
    ]
    let name: string = prefix
    for (let i = 0; i < randomLength; i++) {
      // 随机生成index
      const index = Math.floor(Math.random() * 2)
      let zm = nameArr[index][Math.floor(Math.random() * nameArr[index].length)]
      // 如果随机出的是英文字母
      if (index === 1) {
        // 则百分之50的概率变为大写
        if (Math.floor(Math.random() * 2) === 1) {
          zm = zm.toUpperCase()
        }
      }
      // 拼接进名字变量中
      name += zm
    }
    return name
  }

  public async login(user: UserEntity): Promise<LoginVo> {
    const payload = { username: user.username, sub: user.id, roles: user.roles }
    return {
      user,
      access_token: this.jwtService.sign(payload)
    }
  }

  public async roleAllocation(dto: RoleAllocationDto): Promise<UserEntity> {
    const roles = await this.roleRepository.find({ where: { id: In(dto.ids) } })
    const user = await this.userRepository.findOne({ where: { id: dto.userId } })
    user.roles = roles
    return user.save()
  }

  public async menuAllocation(dto: MenuAllocationDto): Promise<RoleEntity> {
    const menus = await this.menuRepository.find({ where: { id: In(dto.ids) } })
    const role = await this.roleRepository.findOne({ where: { id: dto.roleId } })
    role.menus = menus
    return role.save()
  }

  public async departmentAllocation(dto: DepartmentAllocationDto): Promise<DepartmentEntity> {
    const roles = await this.roleRepository.find({ where: { id: In(dto.ids) } })
    const department = await this.departmentRepository.findOne({ where: { id: dto.departmentId } })
    department.roles = roles
    return department.save()
  }

  public async findUserPermission(dto: FindUserPermissionDto) {
    const user = await this.userRepository.findOne({ where: { id: dto.id }, relations: { roles: { menus: true } } })
    // 管理员账号返回所有权限
    const defaultAdmin = this.jwtConfigService.get<string>('admin.defaultAdmin')
    if (user.username === defaultAdmin) return this.menuRepository.find()
    const menus = user.roles.map((role) => role.menus)
    const result: MenuEntity[] = flatMapDeep(menus)
    return uniqBy(result, 'id')
  }
}
