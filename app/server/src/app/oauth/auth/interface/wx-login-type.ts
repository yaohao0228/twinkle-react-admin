export interface WxLoginParams {
  /**
   * 微信小程序应用Id
   */
  appid: string

  /**
   * 微信小程序应用密钥
   */
  secret: string

  /**
   * 用户Code
   */
  js_code: string

  /**
   * 授权类型
   */
  grant_type: 'authorization_code'
}

export interface WxLoginResponse {
  /**
   * 用户唯一ID
   */
  openid: string

  /**
   * 会话密钥
   */
  session_key: string
}
