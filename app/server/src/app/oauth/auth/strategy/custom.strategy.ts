import { PassportStrategy } from '@nestjs/passport'
import { Strategy } from 'passport-custom'
import { Request } from 'express'
import { Injectable } from '@nestjs/common'
import { AuthService } from '../auth.service'
import { LOGIN_STRATEGY_KEY } from '../../oauth.constants'
import { isEmpty } from 'class-validator'
import { CustomException } from 'src/common/exceptions/exception'
import { LoginDto } from '../dto/login.dto'

@Injectable()
export class CustomStrategy extends PassportStrategy(Strategy, LOGIN_STRATEGY_KEY) {
  constructor(private authService: AuthService) {
    super()
  }

  async validate(request: Request): Promise<any> {
    const loginDto: LoginDto = request.body
    const user = await this.authService.validateUser(loginDto)
    if (isEmpty(user)) {
      throw new CustomException(10003)
    }
    return user
  }
}
