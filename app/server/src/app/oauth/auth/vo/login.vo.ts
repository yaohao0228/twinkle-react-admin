import { UserEntity } from 'src/entities/oauth/user.entity'

export class LoginVo {
  user: UserEntity

  access_token: string
}
