import { Global, Module } from '@nestjs/common'
import { PassportModule } from '@nestjs/passport'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { JwtModule } from '@nestjs/jwt'
import { HttpModule } from '@nestjs/axios'

import { AuthService } from './auth.service'
import { AuthController } from './auth.controller'

import { JwtConfigKeysPaths } from 'config/jwt-config'
import { UserModule } from '../user/user.module'
import { JwtStrategy } from './strategy/jwt.strategy'
import { UserEntity } from 'src/entities/oauth/user.entity'
import { RoleEntity } from 'src/entities/oauth/role.entity'
import { MenuEntity } from 'src/entities/system/menu.entity'
import { DepartmentEntity } from 'src/entities/oauth/department.entity'
import { Define } from 'src/common/decorators/define.decorator'
import { CustomStrategy } from './strategy/custom.strategy'

@Define(['Module'])
@Global()
@Module({
  imports: [
    UserModule,
    PassportModule,
    ConfigModule,
    HttpModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService<JwtConfigKeysPaths>) => ({
        secret: configService.get<string>('jwt.secret'),
        signOptions: { expiresIn: configService.get<string>('jwt.time') }
      })
    }),
    TypeOrmModule.forFeature([UserEntity, RoleEntity, MenuEntity, DepartmentEntity])
  ],
  providers: [AuthService, CustomStrategy, JwtStrategy],
  controllers: [AuthController],
  exports: [AuthService]
})
export class AuthModule {}
