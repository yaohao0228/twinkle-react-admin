import { ApiProperty } from '@nestjs/swagger'

export class FindUserPermissionDto {
  @ApiProperty({ description: 'id' })
  id: string
}
