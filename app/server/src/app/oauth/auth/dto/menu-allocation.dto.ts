import { ApiProperty } from '@nestjs/swagger'

export class MenuAllocationDto {
  @ApiProperty({ description: '菜单ids' })
  ids: string[]

  @ApiProperty({ description: '角色id' })
  roleId: string
}
