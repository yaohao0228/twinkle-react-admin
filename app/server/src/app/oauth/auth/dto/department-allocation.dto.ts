import { ApiProperty } from '@nestjs/swagger'

export class DepartmentAllocationDto {
  @ApiProperty({ description: '角色ids' })
  ids: string[]

  @ApiProperty({ description: '部门id' })
  departmentId: string
}
