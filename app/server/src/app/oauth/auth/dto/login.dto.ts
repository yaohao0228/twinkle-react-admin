import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsOptional, IsString } from 'class-validator'

export enum LoginType {
  PASSWORD = 'password',
  WECHAT = 'weChat'
}

export class LoginDto {
  @ApiProperty({ description: '用户名', default: 'admin' })
  @IsString()
  @IsOptional()
  username: string

  @ApiProperty({ description: '密码', default: '123456' })
  @IsString()
  @IsNotEmpty({ message: 'password not null' })
  password: string

  @ApiProperty({ description: '登录类型', default: 'password' })
  @IsString()
  @IsNotEmpty({ message: 'login_type not null' })
  login_type: LoginType
}
