import { ApiProperty } from '@nestjs/swagger'

export class RoleAllocationDto {
  @ApiProperty({ description: '用户id' })
  userId: string

  @ApiProperty({ description: '角色ids' })
  ids: number[]
}
