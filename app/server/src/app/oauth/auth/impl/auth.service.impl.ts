import { LoginVo } from '../vo/login.vo'
import { RoleAllocationDto } from '../dto/role-allocation.dto'
import { MenuAllocationDto } from '../dto/menu-allocation.dto'
import { DepartmentAllocationDto } from '../dto/department-allocation.dto'
import { FindUserPermissionDto } from '../dto/find-user-permission.dto'
import { LoginDto } from '../dto/login.dto'

import { UserEntity } from 'src/entities/oauth/user.entity'
import { RoleEntity } from 'src/entities/oauth/role.entity'
import { DepartmentEntity } from 'src/entities/oauth/department.entity'
import { MenuEntity } from 'src/entities/system/menu.entity'

export abstract class AuthServiceImpl {
  /**
   * 校验用户
   */
  public abstract validateUser(dto: LoginDto): Promise<unknown>

  /**
   * 登录
   */
  public abstract login(user: UserEntity): Promise<LoginVo>

  /**
   * 角色分配
   */
  public abstract roleAllocation(dto: RoleAllocationDto): Promise<UserEntity>

  /**
   * 菜单分配
   */
  public abstract menuAllocation(dto: MenuAllocationDto): Promise<RoleEntity>

  /**
   * 部门分配
   */
  public abstract departmentAllocation(dto: DepartmentAllocationDto): Promise<DepartmentEntity>

  /**
   * 获取用户权限
   */
  public abstract findUserPermission(dto: FindUserPermissionDto): Promise<MenuEntity[]>

  /**
   * 生成随机用户名
   */
  public abstract randomUsername(prefix: string, randomLength: number): string
}
