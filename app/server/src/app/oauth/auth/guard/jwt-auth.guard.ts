import { ExecutionContext, Injectable } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { AuthGuard } from '@nestjs/passport'
import { Observable } from 'rxjs'

import { IS_PUBLIC_KEY } from 'src/common/contants/decorator.contants'
import { CustomException } from 'src/common/exceptions/exception'

/**
 * JWT校验守卫
 */
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private readonly reflector: Reflector) {
    super()
  }

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    // 开放接口不需要权限验证
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass()
    ])
    if (isPublic) return true
    return super.canActivate(context)
  }

  handleRequest<TUser = any>(err: any, user: any): TUser {
    // 如果没有用户信息，则表示token已过期
    if (err || !user) throw err || new CustomException(11002)
    return user
  }
}
