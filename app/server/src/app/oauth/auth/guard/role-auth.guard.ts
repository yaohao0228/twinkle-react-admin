import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Reflector } from '@nestjs/core'
import { JwtConfigKeysPaths } from 'config/jwt-config'
import { Request } from 'express'
import { IS_PUBLIC_KEY, PERMISSION_KEY, PERMISSION_PREFIX } from 'src/common/contants/decorator.contants'
import { JwtUserDto } from 'src/common/dto/jwt-user.dto'
import { isEmpty } from 'class-validator'
import { CustomException } from 'src/common/exceptions/exception'
import { AuthService } from '../auth.service'

/**
 * 角色权限验证管道
 */
@Injectable()
export class RoleAuthGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly authService: AuthService,
    private readonly configService: ConfigService<JwtConfigKeysPaths>
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request: Request = context.switchToHttp().getRequest()
    // 开放接口不需要权限验证
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass()
    ])
    if (isPublic) return true
    // 管理员不需要权限验证
    const defaultAdmin = this.configService.get<string>('admin.defaultAdmin')
    if ((request.user as JwtUserDto).username === defaultAdmin) return true
    // 权限点校验
    const permissionKey = this.reflector.get<string>(PERMISSION_KEY, context.getHandler())
    // 没有权限点则不需要权限验证
    if (isEmpty(permissionKey)) return true
    const permissionPrefix = this.reflector.getAllAndOverride<string>(PERMISSION_PREFIX, [
      context.getHandler(),
      context.getClass()
    ])
    const user = request.user as JwtUserDto
    const permissions = await this.authService.findUserPermission({ id: user.userId })
    const permission = permissions.find((item) => item.code === `${permissionPrefix}_${permissionKey}`)
    if (isEmpty(permission)) throw new CustomException(11003)
    return true
  }
}
