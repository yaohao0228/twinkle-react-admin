import { Injectable } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { LOGIN_STRATEGY_KEY } from '../../oauth.constants'

/**
 * JWT发放守卫
 */
@Injectable()
export class CustomAuthGuard extends AuthGuard(LOGIN_STRATEGY_KEY) {}
