import { Controller } from '@nestjs/common'

import { DepartmentService } from './department.service'
import { CreateDepartmentDto } from './dto/create-department.dto'
import { UpdateDepartmentDto } from './dto/update-department.dto'

import { UnityController } from 'src/common/unity/unity.controller'
import { ApiTags } from '@nestjs/swagger'
import { PermissionPrefix } from 'src/common/decorators/permission.decorator'
import { OAUTH_PREFIX } from '../oauth.constants'

@ApiTags('部门管理')
@PermissionPrefix('sys_department')
@Controller({ path: `${OAUTH_PREFIX}/department`, version: '1' })
export class DepartmentController extends UnityController<CreateDepartmentDto, UpdateDepartmentDto, DepartmentService> {
  constructor(private readonly departmentService: DepartmentService) {
    super(departmentService)
  }
}
