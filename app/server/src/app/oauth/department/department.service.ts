import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { DataSource, Repository } from 'typeorm'

import { UnityService } from 'src/common/unity/unity.service'
import { DepartmentEntity } from 'src/entities/oauth/department.entity'
import { CreateDepartmentDto } from './dto/create-department.dto'
import { UpdateDepartmentDto } from './dto/update-department.dto'
import { DepartmentServiceImpl } from './impl/department.service.impl'

@Injectable()
export class DepartmentService
  extends UnityService<DepartmentEntity, CreateDepartmentDto, UpdateDepartmentDto>
  implements DepartmentServiceImpl
{
  constructor(
    public readonly dataSource: DataSource,
    @InjectRepository(DepartmentEntity)
    public readonly userRepository: Repository<DepartmentEntity>
  ) {
    super(dataSource, userRepository)
  }
}
