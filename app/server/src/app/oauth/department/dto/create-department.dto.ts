import { Injectable } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { IsString, IsBoolean, IsNotEmpty } from 'class-validator'

@Injectable()
export class CreateDepartmentDto {
  @ApiProperty({ description: '部门代码' })
  @IsString()
  @IsNotEmpty({ message: 'code not null' })
  public code: string

  @ApiProperty({ description: '部门名称' })
  @IsString()
  @IsNotEmpty({ message: 'name not null' })
  public name: string

  @ApiProperty({ description: '是否系统默认部门', default: false })
  @IsBoolean()
  @IsNotEmpty({ message: 'isDefault not null' })
  public isDefault: boolean

  @ApiProperty({ description: '父级部门id' })
  @IsString()
  @IsNotEmpty({ message: 'parentId not null' })
  public parentId: string

  @ApiProperty({ description: '父级部门名称' })
  @IsString()
  @IsNotEmpty({ message: 'parentName not null' })
  public parentName: string

  @ApiProperty({ description: '备注' })
  @IsString()
  @IsNotEmpty({ message: 'remark not null' })
  public remark: string
}
