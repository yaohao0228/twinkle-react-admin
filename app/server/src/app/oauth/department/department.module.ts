import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'

import { DepartmentEntity } from 'src/entities/oauth/department.entity'

import { DepartmentService } from './department.service'
import { DepartmentController } from './department.controller'
import { Define } from 'src/common/decorators/define.decorator'

@Define(['Module'])
@Module({
  imports: [TypeOrmModule.forFeature([DepartmentEntity])],
  controllers: [DepartmentController],
  providers: [DepartmentService]
})
export class DepartmentModule {}
