/**
 * 服务前缀
 */
export const OAUTH_PREFIX = 'oauth'

/**
 * 密码加密签名
 */
export const CRYPTO_SECRET = 'WIYLDJXAPFWTYCONRPEWOUZALMCZFGK'

/**
 * 登录策略Key
 */
export const LOGIN_STRATEGY_KEY = 'login_strategy'

/**
 * 微信登录路径
 */
export const WXCHAT_LOGIN_URL = 'https://api.weixin.qq.com/sns/jscode2session?'
