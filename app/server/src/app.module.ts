import { MiddlewareConsumer, Module, NestModule, RequestMethod, ValidationPipe } from '@nestjs/common'
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { ServeStaticModule } from '@nestjs/serve-static'
import { TypeOrmModule } from '@nestjs/typeorm'

import App from './app'
import config from 'config'
import { DataBaseConfigKeysPaths } from 'config/database-config'

import { LoggerMiddleware } from './common/middleware/logger.middleware'
import { TransformInterceptor } from './common/interception/transform.interception'
import { HttpExceptionFilter } from './common/filters/http-execption.filters'
import { DataSource } from 'typeorm'
import { JwtAuthGuard } from './app/oauth/auth/guard/jwt-auth.guard'
import { RoleAuthGuard } from './app/oauth/auth/guard/role-auth.guard'
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler'
import { GlobalConfigKeysPaths } from 'config/global-config'
import { join } from 'path'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [...config],
      envFilePath: [`.env.${process.env.NODE_ENV}`, '.env']
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService<DataBaseConfigKeysPaths>) => ({
        type: configService.get<any>('database.type'),
        host: configService.get<string>('database.host'),
        port: configService.get<number>('database.port'),
        database: configService.get<string>('database.database'),
        username: configService.get<string>('database.username'),
        password: configService.get<string>('database.password'),
        synchronize: configService.get<boolean>('database.synchronize'),
        autoLoadEntities: configService.get<boolean>('database.autoLoadEntities'),
        connectorPackage: configService.get<string>('database.connectorPackage')
      }),
      dataSourceFactory: async (options) => {
        const dataSource = await new DataSource(options).initialize()
        return dataSource
      }
    }),
    ThrottlerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService<GlobalConfigKeysPaths>) => ({
        throttlers: [
          {
            ttl: configService.get<number>('request.ttl'),
            limit: configService.get<number>('request.limit')
          }
        ]
      })
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../src', 'assets')
    }),
    ...App
  ],
  providers: [
    // 全局拦截器
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor
    },
    // 全局异常处理器
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter
    },
    // 全局数据验证管道
    {
      provide: APP_PIPE,
      useClass: ValidationPipe
    },
    // 全局请求限流
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard
    },
    // 全局权限校验
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard
    },
    // 全局角色权限校验
    {
      provide: APP_GUARD,
      useClass: RoleAuthGuard
    }
  ]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL })
  }
}
