import { UnityEntity } from 'src/common/unity/unity.entity'
import { Column, Entity, ManyToOne } from 'typeorm'
import { DataEntityEntity } from './data-entity.entity'

@Entity('sys_data_entity_column')
export class DataEntityColumnEntity extends UnityEntity {
  @Column({ comment: '元数据代码' })
  public code: string

  @Column({ comment: '元数据名称' })
  public name: string

  @Column({ comment: '元数据类型' })
  public type: string

  @Column({ comment: '数据类型' })
  public dataType: string

  @Column({ comment: '是否可空', default: false })
  public nullable: boolean

  @Column({ comment: '关键字', default: false })
  public unique: boolean

  @Column({ comment: '默认值', nullable: true })
  public default: string

  @Column({ comment: '备注', nullable: true })
  public comment: string

  @ManyToOne(() => DataEntityEntity, (entity) => entity.dataEntityColumn)
  public dataEntity: DataEntityEntity
}
