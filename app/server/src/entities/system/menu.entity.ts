import { UnityEntity } from 'src/common/unity/unity.entity'
import { Column, Entity, JoinTable, ManyToMany } from 'typeorm'
import { RoleEntity } from '../oauth/role.entity'

export enum MenuType {
  PERMISSION = 'permission',
  MENU = 'menu'
}

@Entity('sys_menu')
export class MenuEntity extends UnityEntity {
  @Column({ comment: '菜单编码', unique: true })
  public code: string

  @Column({ comment: '菜单名称' })
  public name: string

  @Column({ comment: '父级菜单', nullable: true })
  public parentId: string

  @Column({ comment: '排序', default: 100 })
  public sort: number

  @Column({ comment: '菜单图标', nullable: true })
  public icon: string

  @Column({ comment: '前端路由', nullable: true })
  public path: string

  @Column({ comment: '文件路径', nullable: true })
  public element: string

  @Column({ comment: '菜单类型', type: 'enum', enum: MenuType, default: MenuType.PERMISSION })
  public type: MenuType

  @Column({ comment: '是否显示', default: true })
  public isShow: boolean

  @Column('simple-json', { comment: '编辑选项', nullable: true })
  public editOption: any

  @ManyToMany(() => RoleEntity)
  @JoinTable({ name: 'sys_menu_relevance_sys_role' })
  roles: RoleEntity[]
}
