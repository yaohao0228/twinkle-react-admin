import { UnityEntity } from 'src/common/unity/unity.entity'
import { Column, Entity, OneToMany } from 'typeorm'
import { DataEntityColumnEntity } from './data-entity-column.entity'

@Entity('sys_data_entity')
export class DataEntityEntity extends UnityEntity {
  @Column({ comment: '实体代码' })
  public code: string

  @Column({ comment: '实体名称' })
  public name: string

  @Column({ comment: '表名' })
  public tableName: string

  @Column({ comment: '接口服务' })
  public service: string

  @Column({ comment: '权限点前缀' })
  public permissionPrefix: string

  @Column({ comment: '是否同步数据库', default: false })
  public isSync: boolean

  @Column({ comment: '是否同步VO', default: false })
  public isSyncVo: boolean

  @OneToMany(() => DataEntityColumnEntity, (column) => column.dataEntity)
  dataEntityColumn: DataEntityColumnEntity[]
}
