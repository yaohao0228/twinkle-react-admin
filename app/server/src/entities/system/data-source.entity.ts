import { Column, Entity } from 'typeorm'
import { UnityEntity } from 'src/common/unity/unity.entity'

@Entity('sys_data_source')
export class DataSourceEntity extends UnityEntity {
  @Column({ comment: '数据库类型' })
  public type: string

  @Column({ comment: '数据库IP' })
  public host: string

  @Column({ comment: '数据库端口号' })
  public port: string

  @Column({ comment: '数据库密码' })
  public password: string

  @Column({ comment: '数据库用户名' })
  public username: string
}
