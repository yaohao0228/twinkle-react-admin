import { Column, Entity, OneToMany } from 'typeorm'
import { UnityEntity } from 'src/common/unity/unity.entity'

import { DictTypeEntity } from './dict-type.entity'

@Entity('sys_dict_data')
export class DictDataEntity extends UnityEntity {
  @Column({ comment: '字典名称', unique: true })
  public dictName: string

  @Column({ comment: '字典编码' })
  public dictCode: string

  @Column({ comment: '是否启用', nullable: true, default: true })
  public isStart: boolean

  @Column({ comment: '类型名称' })
  public typeName: string

  @Column({ comment: '类型编码' })
  public typeCode: string

  @OneToMany(() => DictTypeEntity, (column) => column.dictData)
  public dictType: DictTypeEntity
}
