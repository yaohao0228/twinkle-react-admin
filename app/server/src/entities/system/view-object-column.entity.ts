import { UnityEntity } from 'src/common/unity/unity.entity'
import { Column, Entity, ManyToOne } from 'typeorm'
import { ViewObjectEntity } from './view-object.entity'

export enum EditType {
  STRING = 'string',
  NUMBER = 'number',
  BOOL = 'bool',
  ENUM = 'enum',
  CHILDREN = 'children'
}

@Entity('sys_view_object_column')
export class ViewObjectColumnEntity extends UnityEntity {
  @Column({ comment: '字段名' })
  public name: string

  @Column({ comment: '字段编码' })
  public code: string

  @Column({ comment: '排序', default: 10 })
  public rank: number

  @Column({ comment: '列宽度', default: 150 })
  public width: number

  @Column({ comment: '编辑类型', type: 'enum', enum: EditType, default: EditType.STRING })
  public editType: string

  @Column({ comment: '是否可搜索', default: false })
  public isSearch: boolean

  @Column({ comment: '是否显示', default: true })
  public isShow: boolean

  @Column({ comment: '是否必填', default: false })
  public isRequired: boolean

  @Column('simple-json', { comment: '编辑选项', nullable: true })
  public editOption: any

  @Column({ comment: '备注', nullable: true })
  public comment: string

  @ManyToOne(() => ViewObjectEntity, (view) => view.viewObjectColumn, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  public viewObject: ViewObjectEntity
}
