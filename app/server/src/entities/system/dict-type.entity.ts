import { Column, Entity, OneToMany } from 'typeorm'
import { UnityEntity } from 'src/common/unity/unity.entity'

import { DictDataEntity } from './dict-data.entity'

@Entity('sys_dict_type')
export class DictTypeEntity extends UnityEntity {
  @Column({ comment: '类型编码', unique: true })
  public typeCode: string

  @Column({ comment: '类型名称' })
  public typeName: string

  @Column({ comment: '是否启用', nullable: true, default: true })
  public isStart: boolean

  @OneToMany(() => DictDataEntity, (column) => column.dictType)
  public dictData: DictDataEntity
}
