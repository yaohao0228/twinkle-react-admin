import { Column, Entity, OneToMany } from 'typeorm'
import { UnityEntity } from 'src/common/unity/unity.entity'

import { ViewObjectColumnEntity } from './view-object-column.entity'

@Entity('sys_view_object')
export class ViewObjectEntity extends UnityEntity {
  @Column({ comment: 'VO代码', unique: true })
  public code: string

  @Column({ comment: 'VO名称' })
  public name: string

  @Column({ comment: '接口服务' })
  public service: string

  @OneToMany(() => ViewObjectColumnEntity, (column) => column.viewObject)
  public viewObjectColumn: ViewObjectColumnEntity
}
