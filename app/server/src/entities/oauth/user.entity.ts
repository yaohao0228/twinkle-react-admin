import { Column, Entity, JoinTable, ManyToMany } from 'typeorm'
import { UnityEntity } from '../../common/unity/unity.entity'
import { RoleEntity } from './role.entity'

@Entity('sys_user')
export class UserEntity extends UnityEntity {
  @Column({ comment: '用户名', unique: true })
  public username: string

  @Column({ comment: '密码', nullable: true })
  public password: string

  @Column({ comment: '手机号', unique: true, nullable: true })
  public phone: string

  @Column({ comment: '邮箱', nullable: true })
  public email: string

  @Column({ comment: '头像', nullable: true })
  public avatar: string

  @Column({ comment: '姓名', nullable: true })
  public name: string

  @Column({ comment: '微信用户id', nullable: true })
  public openid: string

  @ManyToMany(() => RoleEntity)
  @JoinTable({ name: 'sys_user_relevance_sys_role' })
  public roles: RoleEntity[]
}
