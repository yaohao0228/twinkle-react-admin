import { Column, Entity, OneToMany } from 'typeorm'
import { UnityEntity } from '../../common/unity/unity.entity'
import { RoleEntity } from './role.entity'

@Entity('sys_department')
export class DepartmentEntity extends UnityEntity {
  @Column({ comment: '部门代码', unique: true })
  public code: string

  @Column({ comment: '部门名称' })
  public name: string

  @Column({ comment: '是否系统默认部门', default: false })
  public isDefault: boolean

  @Column({ comment: '父级部门id', nullable: true })
  public parentId: string

  @Column({ comment: '父级部门名称', nullable: true })
  public parentName: string

  @Column({ comment: '备注', nullable: true })
  public remark: string

  @OneToMany(() => RoleEntity, (role) => role.department)
  public roles: RoleEntity[]
}
