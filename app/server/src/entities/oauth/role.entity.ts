import { Column, Entity, JoinTable, ManyToMany, ManyToOne } from 'typeorm'
import { UnityEntity } from '../../common/unity/unity.entity'
import { UserEntity } from './user.entity'
import { MenuEntity } from '../system/menu.entity'
import { DepartmentEntity } from './department.entity'

@Entity('sys_role')
export class RoleEntity extends UnityEntity {
  @Column({ comment: '角色代码', unique: true })
  public code: string

  @Column({ comment: '角色名称' })
  public name: string

  @Column({ comment: '是否系统默认角色', default: false })
  public isDefault: boolean

  @Column({ comment: '备注', nullable: true })
  public remark: string

  @ManyToMany(() => UserEntity)
  @JoinTable({ name: 'sys_user_relevance_sys_role' })
  public users: UserEntity[]

  @ManyToMany(() => MenuEntity)
  @JoinTable({ name: 'sys_menu_relevance_sys_role' })
  public menus: MenuEntity[]

  @ManyToOne(() => DepartmentEntity, (department) => department.roles)
  public department: DepartmentEntity
}
