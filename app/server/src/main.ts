import { NestFactory } from '@nestjs/core'
import { VersioningType } from '@nestjs/common'
import { NestExpressApplication } from '@nestjs/platform-express'

import { AppModule } from './app.module'
import { Swagger } from './shared/swagger/swagger'

const bootstrap = async () => {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, { cors: true })

  // Swagger
  new Swagger(app).setup()

  // 版本控制
  app.enableVersioning({
    type: VersioningType.URI
  })

  await app.listen(process.env.SERVER_LISTENING_PORT)
}
bootstrap()
