import { Data, renderFile } from 'ejs'
import * as path from 'path'
import * as fs from 'fs'

export class FileAction {
  /**
   * ejs模板编译
   */
  static async compile(templateName: string, data: Data): Promise<string> {
    const templatePosition = `../../assets/template/${templateName}`
    const templatePath = path.resolve(__dirname, templatePosition)

    // 读取内容
    return await renderFile(templatePath, data)
  }

  /**
   * 递归创建文件夹
   */
  static createDirSync(filePath: string) {
    if (fs.existsSync(filePath)) return true
    if (this.createDirSync(path.dirname(filePath))) {
      fs.mkdirSync(filePath)
      return true
    }
  }

  /**
   * 写入内容
   */
  static writeToFile(path: string, content: any) {
    return fs.promises.writeFile(path, content)
  }

  /**
   * 删除文件
   */
  static deleteToFile(path: string) {
    return fs.unlinkSync(path)
  }
}
