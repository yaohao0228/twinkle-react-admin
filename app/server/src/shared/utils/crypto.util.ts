import { createHash } from 'crypto'

export class Crypto {
  /**
   * MD5加密
   */
  static md5(input: string) {
    return createHash('md5').update(input).digest('hex')
  }
}
