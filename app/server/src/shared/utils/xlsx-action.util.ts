import { isString } from 'class-validator'
import { readFile, writeFile as xlsxWriteFile, utils, WorkBook, read as xlsxRead } from 'xlsx'

export class XlsxActionUtil {
  /**
   * 读取xlsx数据
   * @param {Express.Multer.File} data 文件对象
   */
  static read<T = unknown>(data: Express.Multer.File | string, page: number = 0): T[] {
    const workbook = isString(data) ? readFile(data) : xlsxRead(data.buffer, { type: 'buffer' })
    const sheetName = workbook.SheetNames[page]
    const sheet = workbook.Sheets[sheetName]
    return utils.sheet_to_json(sheet)
  }

  /**
   * 创建xlsx文件
   * @param {Array} data 数据
   * @param {string} sheetName 表名
   */
  static create(data: Array<Record<string, any>>, sheetName: string = 'Sheet1'): WorkBook {
    const workbook = utils.book_new()
    utils.book_append_sheet(workbook, utils.json_to_sheet(data), sheetName)
    return workbook
  }

  /**
   * 写入并创建xlsx
   * @param {string} filePath 文件路径
   * @param {Array} data 数据
   * @param {string} sheetName 表名
   */
  static writeFile(filePath: string, data: Array<Record<string, any>>, sheetName = 'Sheet1'): void {
    const workbook = this.create(data, sheetName)
    xlsxWriteFile(workbook, filePath)
  }
}
