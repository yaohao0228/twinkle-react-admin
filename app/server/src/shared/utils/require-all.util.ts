import 'reflect-metadata'
import { join } from 'path'
import { readdirSync, statSync } from 'fs'
import { isArray } from 'lodash'

interface IOptions {
  dirname: string
  ignore: RegExp | Function
  filter: RegExp | Function
  currentFile: string
}

export const toArray = (value: any): any[] => (isArray(value) ? value : [value])

// 忽略以.开头的文件、文件夹、node_modules、.d.ts、.d.js文件
const DEFAULT_IGNORE_RULE = /(^\.|^node_modules|(\.d\..*)$)/
// 只取.ts或者.js结尾的文件
const DEFAULT_FILTER = /^([^.].*)\.(ts|js)$/
// 当前文件路径
const DEFAULT_CURRENT_FILE = __filename

export class Require {
  /**
   * 导入单个文件
   */
  static requireSingle(fileName: string): Promise<unknown> {
    return new Promise((resolve, reject) => {
      import(fileName).then((data) => resolve(data)).catch((error) => reject(error))
    })
  }

  /**
   * 导入全部文件
   */
  static async requireAll(options: Partial<IOptions>) {
    if (!options.dirname) throw new Error('dirname cannot be empty!')
    if (!options.currentFile) throw new Error('currentFile cannot be empty!')
    const dirname = options.dirname
    const ignore = options.ignore || DEFAULT_IGNORE_RULE
    const files = readdirSync(dirname)
    const modules: Map<any, any> = new Map()
    const filter = options.filter || DEFAULT_FILTER
    const currentFile = options.currentFile || DEFAULT_CURRENT_FILE

    const ignoreFile: (f: string) => boolean = (fileName: string) => {
      if (typeof ignore === 'function') {
        return ignore(fileName)
      } else {
        return fileName === currentFile || !!fileName.match(ignore as RegExp)
      }
    }

    const filterFile = (fileName: string) => {
      if (typeof filter === 'function') {
        return filter(fileName)
      } else {
        return filter.test(fileName)
      }
    }
    await Promise.all(
      files.map(async (filename) => {
        const fileName = join(dirname, filename)
        // 如果为true, 则为忽略文件
        if (ignoreFile(fileName)) return

        // 如果不为true, 则要读取
        if (statSync(fileName).isDirectory()) {
          const subModules = await Require.requireAll({
            dirname: fileName,
            ignore: ignore,
            currentFile: currentFile
          })
          Require.mergeMap(modules, subModules)
        } else {
          if (filterFile(fileName)) {
            const data = await Require.requireSingle(fileName)
            modules.set(fileName, data)
          }
        }
      })
    )

    return modules
  }

  /**
   * Nest导入全部文件
   */
  static requireAllInNest(options: Partial<IOptions>, type: 'Controller' | 'Injectable' | 'Module' = 'Module') {
    return new Promise((resolve, reject) => {
      if (type === 'Controller') {
        options.filter = /^([^.].*)\.controller\.ts$/
      } else if (type === 'Injectable') {
        options.filter = /^([^.].*)\.service\.ts$/
      } else {
        options.filter = /^([^.].*)\.module\.ts$/
      }
      Require.requireAll(options)
        .then((modules) => {
          try {
            const importsModule: any[] = Require.readKeys(modules, (key, value) => {
              return Require.readKeys(value, (vKey, target) => {
                if (typeof target === 'function') {
                  const metadata = Reflect.getMetadata('decorator', target)
                  if (Array.isArray(metadata) && metadata.length > 0 && metadata.indexOf(type) !== -1) {
                    return target
                  }
                }
              })
            })
            const results: any[] = []
            importsModule.forEach((chunk: any[]) => results.push(...chunk))
            resolve(results)
          } catch (error) {
            reject(error)
          }
        })
        .catch((error) => reject(error))
    })
  }

  /**
   * 合并map
   */
  static mergeMap(targetMap: Map<any, any>, subMap: Map<any, any>): void {
    for (const key of subMap.keys()) targetMap.set(key, subMap.get(key))
  }

  /**
   * 读取属性
   */
  static readKeys(map: Object, callback: (key: any, value: any) => void): any[] {
    const results = []
    if (map instanceof Map) {
      for (const key of map.keys()) {
        const returns = callback(key, map.get(key))
        if (returns !== undefined) {
          results.push(returns)
        }
      }
    } else {
      for (const key of Reflect.ownKeys(map)) {
        // @ts-ignore
        const returns = callback(key, map[key])
        if (returns !== undefined) {
          results.push(returns)
        }
      }
    }
    if (results.length > 0) {
      return results
    }
  }
}
