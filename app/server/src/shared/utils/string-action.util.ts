export class StringAction {
  /**
   * 首字母大写
   */
  static upperFirst(input: string): string {
    const characters = [...input]
    characters[0] = characters[0].toUpperCase()
    return characters.join('')
  }

  /**
   * 字符串转为驼峰命名法
   */
  static camelCase(input: string, isUpperCamelCase: boolean = true): string {
    // 处理短横线和下划线，将其替换为空格
    const stringWithoutHyphensOrUnderscores = input.replace(/[-_]/g, ' ')
    // 将每个单词的首字母大写
    const capitalizedWords = stringWithoutHyphensOrUnderscores.replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
      return index === 0 && !isUpperCamelCase ? word.toLowerCase() : word.toUpperCase()
    })
    // 移除空格并返回结果
    const result = capitalizedWords.replace(/\s+/g, '')
    return result
  }

  /**
   * 字符串转为短横线命名法
   */
  static kebabCase(input: string): string {
    // 将大小驼峰命名转换为短横线命名
    const kebabFromCamel = input.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase()
    // 将下划线命名转换为短横线命名
    const kebabFromUnderscore = kebabFromCamel.replace(/_/g, '-')
    return kebabFromUnderscore
  }
}
