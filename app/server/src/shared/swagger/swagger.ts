import { INestApplication } from '@nestjs/common'
import { DocumentBuilder, SwaggerDocumentOptions, SwaggerModule } from '@nestjs/swagger'
import { knife4jSetup } from 'nestjs-knife4j'
import { swaggerConfig } from 'config/swagger-config'

/**
 * Swagger
 */
export class Swagger {
  public readonly title: string

  public readonly description: string

  public readonly version: string

  private readonly _app: INestApplication<any>

  constructor(private readonly app: INestApplication<any>) {
    const { config } = swaggerConfig()
    this.title = config.title
    this.description = config.description
    this.version = config.version
    this._app = app
  }

  /**
   * 初始化函数
   */
  public async setup() {
    const config = new DocumentBuilder()
      .setTitle(this.title)
      .setDescription(this.description)
      .setVersion(this.version)
      .addBearerAuth()
      .build()
    const options: SwaggerDocumentOptions = {
      operationIdFactory: (controllerKey: string, methodKey: string) => methodKey
    }
    const document = SwaggerModule.createDocument(this._app, config, options)
    SwaggerModule.setup('api', this._app, document)
    knife4jSetup(this._app, {
      urls: [
        {
          name: '2.X版本',
          url: '/api-json',
          swaggerVersion: '2.0',
          location: '/api-json'
        }
      ]
    })
  }
}
