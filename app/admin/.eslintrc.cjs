module.exports = {
  plugins: ['react-refresh'],
  extends: ['../../config/eslint.base.cjs', 'plugin:react-hooks/recommended'],
  rules: {
    'react-refresh/only-export-components': ['warn', { allowConstantExport: true }],
    'react-hooks/rules-of-hooks': 'off',
    'react-hooks/exhaustive-deps': 'off'
  }
}
