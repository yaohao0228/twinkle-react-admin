import { BaseApi, http } from 'twinkle-request'
import { LoginParams } from '@/types'

export enum Api {
  Login = 'login',
  findPermission = 'findPermission'
}

class AuthApi extends BaseApi {
  /**
   * 登录
   */
  async login<T = any>(params: LoginParams) {
    return http.post<T>(`/${this.service}/${this.model}/${Api.Login}`, params, { withToken: false })
  }

  /**
   * 获取用户权限
   */
  async findPermission<T = any>() {
    return http.get<T>(`/${this.service}/${this.model}/${Api.findPermission}`)
  }
}

const authApi = new AuthApi('oauth', 'auth')

export { authApi }
