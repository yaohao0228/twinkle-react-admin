import { BaseApi } from 'twinkle-request'

export enum SystemApiEnum {
  Login = '/login',
  Logout = '/logout'
}

class SystemApi extends BaseApi {}
class MenuApi extends BaseApi {}

const systemApi = new SystemApi('system')
const menuApi = new MenuApi('system', 'menu')

export { systemApi, menuApi }
