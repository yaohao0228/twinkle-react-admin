import React from 'react'
import App from './App.tsx'
import ReactDOM from 'react-dom/client'
import zhCN from 'antd/locale/zh_CN'
import { ConfigProvider } from 'antd'

import 'tailwindcss/tailwind.css'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <ConfigProvider locale={zhCN}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </ConfigProvider>
)
