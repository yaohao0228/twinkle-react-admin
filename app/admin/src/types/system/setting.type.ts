import { LayoutMode, ThemeEnum } from './layout.type'

/**
 * 样式配置
 */
export interface DesignSetting {
  /**
   * 样式配置抽屉是否弹出
   */
  designDrawerOpen?: boolean
  /**
   * 过渡动画时长
   */
  transitionTime: string
  /**
   * 布局
   */
  layout: LayoutMode
  /**
   * 系统主题
   */
  systemTheme: ThemeEnum
  /**
   * 菜单配置
   */
  menuSetting: MenuSetting
  /**
   * 头部导航栏配置
   */
  headerSetting: HeaderSetting
}

/**
 * 菜单配置
 */
export interface MenuSetting {
  /**
   * 是否固定
   */
  fixed: boolean
  /**
   * 收缩状态
   */
  collapsed: boolean
  /**
   * 主题
   */
  theme: ThemeEnum
}

/**
 * 头部导航栏配置
 */
export interface HeaderSetting {
  /**
   * 是否固定
   */
  fixed: boolean
  /**
   * 主题
   */
  theme: ThemeEnum
}
