/**
 * 布局模式
 */
export enum LayoutMode {
  /**
   * 默认布局
   */
  DEFAULT = 'default',
  /**
   * 经典布局
   */
  CLASSICAL = 'classical',
  /**
   * 单栏布局
   */
  SINGLE = 'single',
  /**
   * 单页面布局
   */
  PAGE = 'page'
}

/**
 * 主题
 */
export enum ThemeEnum {
  /**
   * 暗黑主题
   */
  DARK = 'dark',
  /**
   * 明亮主题
   */
  LIGHT = 'light'
}
