import { BaseType } from '../base'

/**
 * 基础配置
 */
export interface BaseOption<T extends Record<string, any> = any> {
  /**
   * 是否显示
   */
  isShow: boolean
  /**
   * 参数
   */
  props: T
}

/**
 * 编辑选项
 */
export interface MenuEditOption {
  /**
   * 视图模型名称
   */
  voName: string
  /**
   * 服务名
   */
  service?: string
  /**
   * 查询栏配置
   */
  queryOptions: BaseOption & {}
  /**
   * 操作栏配置
   */
  activeOptions: BaseOption & {}
  /**
   * 表格配置
   */
  tableOptions: BaseOption & {}
  /**
   * 表单配置
   */
  formOptions: BaseOption & {}
}

/**
 * 菜单
 */
export interface MenuType extends BaseType {
  /**
   * 名称
   */
  name: string
  /**
   * 唯一标识
   */
  code: string
  /**
   * 父级id
   */
  parentId: string
  /**
   * 排序
   */
  sort: number
  /**
   * 菜单图标
   */
  icon: string
  /**
   * 前端路由
   */
  path: string
  /**
   * 文件路径
   */
  element: string
  /**
   * 菜单类型
   */
  type: MenuTypeEnum
  /**
   * 是否显示
   */
  isShow: boolean
  /**
   * 子集
   */
  children?: MenuType[]
  /**
   * 编辑选项
   */
  editOption?: MenuEditOption
}

/**
 * 菜单类型
 */
export enum MenuTypeEnum {
  /**
   * 菜单
   */
  MENU = 'menu',
  /**
   * 权限点
   */
  PERMISSION = 'permission'
}
