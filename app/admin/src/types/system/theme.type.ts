import { ThemeConfig } from 'antd'

/**
 * 基础主题配置
 */
export interface BaseThemeConfig {
  /**
   * 主题配置
   */
  themeConfig: ThemeConfig
  /**
   * 自定义主题配置
   */
  customConfig: Partial<CustomConfig>
}

/**
 * 自定义主题配置
 */
export interface CustomConfig {
  /**
   * 布局边框颜色
   */
  layoutBorderColor: string
  /**
   * 文字颜色
   */
  fontColor: string
  /**
   * 色块背景颜色
   */
  divBgColor: string
  /**
   * 色块背景颜色（hover）
   */
  divBgColorHover: string
}
