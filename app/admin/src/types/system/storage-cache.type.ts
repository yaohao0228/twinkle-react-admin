/**
 * 本地缓存键名
 */
export enum StorageCacheKey {
  /**
   * 缓存
   */
  WEB_STORAGE = 'web_storage',
  /**
   * 环境变量配置
   */
  ENV_CONFIG = 'envConfig',
  /**
   * token
   */
  TOKEN = 'token',
  /**
   * 用户信息
   */
  USER_INFO = 'userInfo',
  /**
   * 菜单
   */
  MENU = 'menu',
  /**
   * 样式
   */
  DESIGN = 'design',
  /**
   * 权限
   */
  AUTH = 'auth'
}

/**
 * 本地缓存类型
 */
export enum StorageCacheType {
  /**
   * 临时缓存
   */
  SESSION,
  /**
   * 永久缓存
   */
  LOCAL
}
