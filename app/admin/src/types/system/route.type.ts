import { NonIndexRouteObject } from 'react-router-dom'
import { MenuType } from '.'

/**
 * 路由路径
 */
export enum RoutePathEnum {
  /**
   * 登录
   */
  LOGIN = '/login',
  /**
   * 工作台
   */
  WORKBENCH = '/workbench'
}

/**
 * 路由信息
 */
export interface BasicRouteMeta {
  /**
   * 是否是单页面布局
   */
  isPageLayout: boolean
}

/**
 * 路由元数据
 */
export type RouteMeta = Partial<BasicRouteMeta> & Partial<MenuType>

/**
 * 应用路由对象
 */
export interface AppRouteObject extends NonIndexRouteObject {
  /**
   * 路由元数据
   */
  meta?: RouteMeta
  /**
   * 子路由集
   */
  children?: AppRouteObject[]
}
