/**
 * 全局事件总线
 */
export enum EventEnum {
  /**
   * 操作栏添加按钮点击事件
   */
  ACTION_BAR_ADD_CLICK = 'action_bar_add_click',
  /**
   * 操作栏按钮点击事件
   */
  ACTION_BAR_BUTTON_CLICK = 'action_bar_button_click',
  /**
   * 表单展示事件
   */
  FORM_VIEW_SHOW = 'form_view_show',
  /**
   * 表格刷新
   */
  TABLE_GRID_RESET = 'table_grid_reset',
  /**
   * 表格选中值变化
   */
  TABLE_SELECT_CHANGE = 'table_select_change'
}
