/**
 * 编辑类型
 */
export enum EditType {
  /**
   * 字符串
   */
  STRING = 'string',
  /**
   * 数字
   */
  NUMBER = 'number',
  /**
   *枚举
   */
  ENUM = 'enum',
  /**
   * 布尔
   */
  BOOLEAN = 'bool',
  /**
   * 下拉
   */
  SELECT = 'select',
  /**
   * 文本域
   */
  TEXTAREA = 'textarea',
  /**
   * 子表
   */
  CHILDREN = 'children'
}
