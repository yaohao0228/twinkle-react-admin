import { BaseType } from '../base'

/**
 * 用户
 */
export interface User extends BaseType {
  /**
   * 用户名
   */
  username: string
  /**
   * 手机号
   */
  phone: string
  /**
   * 邮箱
   */
  email: string
  /**
   * 名称
   */
  name: string
  /**
   * 微信id
   */
  openid: string
  /**
   * 头像
   */
  avatar: string
}

export enum LoginType {
  /**
   * 密码
   */
  PASSWORD = 'password',
  /**
   * 微信
   */
  WECHAT = 'weChat'
}

/**
 * 登录参数
 */
export interface LoginParams {
  /**
   * 用户名
   */
  username: string

  /**
   * 密码
   */
  password: string

  /**
   * 登录类型
   */
  login_type: LoginType
}

/**
 * 登录响应
 */
export interface LoginResponse {
  /**
   * token
   */
  access_token: string
  /**
   * 用户信息
   */
  user: User
}
