/**
 * 基础数据类型
 */
export interface BaseType {
  /**
   * 主键
   */
  id: number
  /**
   * 数据版本
   */
  version: number
  /**
   * 创建时间
   */
  createTime: string
  /**
   * 更新时间
   */
  updateTime: string
}
