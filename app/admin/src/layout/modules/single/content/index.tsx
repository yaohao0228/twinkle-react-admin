import { Layout, theme } from 'antd'
import { Outlet } from 'react-router-dom'

const { Content } = Layout

const SingleContent: React.FC = () => {
  const {
    token: { colorBgContainer }
  } = theme.useToken()

  return (
    <Content style={{ padding: '0 50px' }}>
      <Layout style={{ padding: '24px 0', background: colorBgContainer }}>
        <Content style={{ padding: '0 24px', minHeight: 280 }}>
          <Outlet />
        </Content>
      </Layout>
    </Content>
  )
}

export default SingleContent
