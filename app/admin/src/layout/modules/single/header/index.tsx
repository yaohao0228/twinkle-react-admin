import { Flex, Layout, Menu, theme } from 'antd'

import AppOperateBar from '@/layout/components/app-operate-bar/app-operate-bar'
import { AppLogo, AppLogoLayoutEnum } from '@/layout/components/app-logo'
import { useDesignStore } from '@/store'
import { UploadOutlined, UserOutlined, VideoCameraOutlined } from '@ant-design/icons'
import { ThemeEnum } from '@/types/system/layout.type'

const { Header } = Layout

const SingleHeader: React.FC = () => {
  const {
    fixed,
    transitionTime,
    theme: storeTheme
  } = useDesignStore((state) => ({
    fixed: state.headerSetting.fixed,
    theme: state.headerSetting.theme,
    transitionTime: state.transitionTime
  }))

  const {
    token: { colorBgContainer }
  } = theme.useToken()

  return (
    <Header
      style={{
        position: fixed ? 'sticky' : 'relative',
        top: 0,
        zIndex: 1,
        width: '100%',
        height: 54,
        padding: '0px 24px',
        display: 'flex',
        background: storeTheme === ThemeEnum.DARK ? '' : colorBgContainer,
        transition: `all ${transitionTime}s`,
        borderBottom: '1px solid rgba(5, 5, 5, 0.06)'
      }}
    >
      <Flex style={{ width: '100%' }} justify="space-between">
        <AppLogo layoutMode={AppLogoLayoutEnum.LEVEL} />
        <Flex align="center">
          <Menu
            mode="horizontal"
            theme={storeTheme}
            items={[
              {
                key: '1',
                icon: <UserOutlined />,
                label: '权限管理'
              },
              {
                key: '2',
                icon: <VideoCameraOutlined />,
                label: '角色管理'
              },
              {
                key: '3',
                icon: <UploadOutlined />,
                label: '平台管理'
              }
            ]}
            style={{
              height: 54,
              transition: `all ${transitionTime}s`
            }}
          />
        </Flex>

        <AppOperateBar />
      </Flex>
    </Header>
  )
}

export default SingleHeader
