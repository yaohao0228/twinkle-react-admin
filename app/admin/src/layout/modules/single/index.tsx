import { Layout } from 'antd'

import SingleHeader from './header'
import SingleContent from './content'

const SingleLayout: React.FC = () => {
  return (
    <Layout>
      <SingleHeader />
      <SingleContent />
    </Layout>
  )
}

export default SingleLayout
