import { Layout } from 'antd'
import { Outlet } from 'react-router-dom'
import { motion } from 'framer-motion'

import { AppTabs } from '@/layout/components/app-tabs'
import { useDesignStore } from '@/store'

import './index.css'
import { CloseOutlined } from '@ant-design/icons'
import { useTabStore } from '@/store/tab.store'
import { themeConfigEnums } from '@/theme'

const { Content } = Layout

const DefaultContent: React.FC = () => {
  const { systemTheme, transitionTime } = useDesignStore((state) => ({
    systemTheme: state.systemTheme,
    transitionTime: state.transitionTime
  }))
  const { isScreenFull, setIsScreenFull } = useTabStore((state) => state)

  const {
    customConfig: { layoutBorderColor, fontColor, divBgColor },
    themeConfig
  } = themeConfigEnums[systemTheme]

  const CloseBtn: React.FC = () => {
    const closeScreenFull = () => {
      setIsScreenFull(false)
    }

    return (
      <motion.div
        className="absolute w-14 h-14 rounded-full cursor-pointer z-10 -top-10 left-1/2 right-1/2 -translate-x-1/2 flex justify-center items-center text-xl"
        style={{ backgroundColor: divBgColor, color: fontColor }}
        initial={{ top: '-40px' }}
        whileHover={{ top: '10px' }}
        onClick={closeScreenFull}
      >
        <CloseOutlined />
      </motion.div>
    )
  }

  return (
    <>
      {isScreenFull ? <CloseBtn /> : <></>}
      <AppTabs />
      <Content
        className={`rounded-xl m-6 p-6 my-0 pb-0 overflow-hidden ${isScreenFull ? 'content-full-screen' : ''}`}
        style={{
          minHeight: window.innerHeight - 128,
          backgroundColor: themeConfig.components?.Layout?.bodyBg,
          color: fontColor,
          border: `1px solid ${layoutBorderColor}`,
          transition: `all ${transitionTime}s`
        }}
      >
        <Outlet />
      </Content>
    </>
  )
}

export default DefaultContent
