import { Layout, Flex } from 'antd'
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons'
import { useDesignStore } from '@/store'

import { AppOperateBar } from '@/layout/components/app-operate-bar'
import { themeConfigEnums } from '@/theme'

const { Header } = Layout

const DefaultHeader: React.FC = () => {
  const { fixed, collapsed, transitionTime, theme, setMenuSetting } = useDesignStore((state) => ({
    fixed: state.headerSetting.fixed,
    transitionTime: state.transitionTime,
    collapsed: state.menuSetting.collapsed,
    theme: state.headerSetting.theme,
    setMenuSetting: state.setMenuSetting
  }))

  const {
    customConfig: { layoutBorderColor, fontColor },
    themeConfig
  } = themeConfigEnums[theme]

  return (
    <Header
      className="p-0 h-[54px] flex top-0 w-full"
      style={{
        backgroundColor: themeConfig.components?.Layout?.headerBg,
        position: fixed ? 'sticky' : 'relative',
        zIndex: 2,
        transition: `all ${transitionTime}s`
      }}
    >
      <Flex style={{ width: '100%', padding: '0 24px' }} justify="space-between">
        <div className="flex items-center">
          <div
            className={
              'w-10 h-10 flex justify-center items-center rounded-lg mr-2 cursor-pointer text-lg transition-all'
            }
            style={{
              color: fontColor,
              border: `1px solid ${layoutBorderColor}`
            }}
            onClick={() => setMenuSetting('collapsed', !collapsed)}
          >
            {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
          </div>
        </div>

        <AppOperateBar />
      </Flex>
    </Header>
  )
}

export default DefaultHeader
