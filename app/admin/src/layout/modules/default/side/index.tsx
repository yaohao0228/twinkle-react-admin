import { Layout } from 'antd'

import { useDesignStore } from '@/store'
import { AppLogo, AppLogoLayoutEnum } from '@/layout/components/app-logo'
import { AppMenu } from '@/layout/components/app-menu'
import { themeConfigEnums } from '@/theme'

const { Sider } = Layout

const DefaultSide: React.FC = () => {
  const { theme, fixed, collapsed, transitionTime } = useDesignStore((state) => ({
    theme: state.menuSetting.theme,
    fixed: state.menuSetting.fixed,
    collapsed: state.menuSetting.collapsed,
    transitionTime: state.transitionTime
  }))

  const {
    themeConfig,
    customConfig: { layoutBorderColor }
  } = themeConfigEnums[theme]

  return (
    <Sider
      className="overflow-auto left-0 top-0 bottom-0"
      theme={theme}
      trigger={null}
      collapsible
      collapsed={collapsed}
      style={{
        backgroundColor: themeConfig.components?.Layout?.siderBg,
        position: fixed ? 'fixed' : 'relative',
        transition: `all ${transitionTime}s`,
        border: `1px solid ${layoutBorderColor}`
      }}
    >
      <AppLogo layoutMode={AppLogoLayoutEnum.VERTICAL} />
      <AppMenu />
    </Sider>
  )
}

export default DefaultSide
