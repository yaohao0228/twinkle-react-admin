import { Layout } from 'antd'
import DefaultSide from './side'
import DefaultHeader from './header'
import DefaultContent from './content'

import { useDesignStore } from '@/store'

const DefaultLayout: React.FC = () => {
  const { collapsed, fixed, transitionTime } = useDesignStore((state) => ({
    collapsed: state.menuSetting.collapsed,
    fixed: state.menuSetting.fixed,
    transitionTime: state.transitionTime,
    systemTheme: state.systemTheme
  }))

  return (
    <Layout>
      <DefaultSide />
      <Layout
        style={{
          marginLeft: fixed ? (collapsed ? 80 : 200) : 0,
          transition: `all ${transitionTime}s`
        }}
      >
        <DefaultHeader />
        <DefaultContent />
      </Layout>
    </Layout>
  )
}

export default DefaultLayout
