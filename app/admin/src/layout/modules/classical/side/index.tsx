import { useDesignStore } from '@/store'
import { LaptopOutlined, NotificationOutlined, UserOutlined } from '@ant-design/icons'
import { Layout, Menu, MenuProps } from 'antd'

import React from 'react'

const { Sider } = Layout

const ClassicalSide: React.FC = () => {
  const { collapsed, fixed, transitionTime, theme } = useDesignStore((state) => ({
    collapsed: state.menuSetting.collapsed,
    fixed: state.menuSetting.fixed,
    theme: state.menuSetting.theme,
    transitionTime: state.transitionTime
  }))

  const items2: MenuProps['items'] = [UserOutlined, LaptopOutlined, NotificationOutlined].map((icon, index) => {
    const key = String(index + 1)

    return {
      key: `sub${key}`,
      icon: React.createElement(icon),
      label: `subnav ${key}`,

      children: new Array(4).fill(null).map((_, j) => {
        const subKey = index * 4 + j + 1
        return {
          key: subKey,
          label: `option${subKey}`
        }
      })
    }
  })

  return (
    <Sider
      theme={theme}
      trigger={null}
      collapsed={collapsed}
      collapsible
      style={{
        overflow: 'auto',
        position: fixed ? 'fixed' : 'relative',
        left: 0,
        top: 0,
        bottom: 0,
        paddingTop: fixed ? 54 : 0,
        transition: `all ${transitionTime}s`
      }}
    >
      <Menu
        mode="inline"
        theme={theme}
        defaultSelectedKeys={['1']}
        style={{
          height: '100%',
          borderRight: 0,
          transition: `all ${transitionTime}s`
        }}
        items={items2}
      />
    </Sider>
  )
}

export default ClassicalSide
