import { Layout } from 'antd'

import ClassicalHeader from './header'
import ClassicalSide from './side'
import ClassicalContent from './content'

const ClassicalLayout: React.FC = () => {
  return (
    <Layout>
      <ClassicalHeader />
      <Layout>
        <ClassicalSide />
        <Layout style={{ padding: 24 }}>
          <ClassicalContent />
        </Layout>
      </Layout>
    </Layout>
  )
}

export default ClassicalLayout
