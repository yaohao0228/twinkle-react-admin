import { Layout, theme } from 'antd'
import { Outlet } from 'react-router-dom'
import { useDesignStore } from '@/store'

const { Content } = Layout

const ClassicalContent: React.FC = () => {
  const { collapsed, fixed } = useDesignStore((state) => ({
    collapsed: state.menuSetting.collapsed,
    fixed: state.menuSetting.fixed
  }))

  const {
    token: { colorBgContainer }
  } = theme.useToken()

  return (
    <Content
      style={{
        padding: 24,
        margin: 0,
        marginLeft: fixed ? (collapsed ? 80 : 200) : 0,
        minHeight: window.innerHeight - 112,
        background: colorBgContainer,
        transition: 'all 0.3s'
      }}
    >
      <Outlet />
    </Content>
  )
}

export default ClassicalContent
