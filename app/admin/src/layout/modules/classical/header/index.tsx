import { Flex, Layout, theme } from 'antd'
import { useDesignStore } from '@/store'

import AppOperateBar from '@/layout/components/app-operate-bar/app-operate-bar'
import { AppLogo, AppLogoLayoutEnum } from '@/layout/components/app-logo'
import { ThemeEnum } from '@/types/system/layout.type'

const { Header } = Layout

const ClassicalHeader: React.FC = () => {
  const {
    theme: storeTheme,
    fixed,
    transitionTime
  } = useDesignStore((state) => ({
    theme: state.headerSetting.theme,
    fixed: state.headerSetting.fixed,
    transitionTime: state.transitionTime
  }))

  const {
    token: { colorBgContainer }
  } = theme.useToken()

  return (
    <Header
      style={{
        position: fixed ? 'sticky' : 'relative',
        top: 0,
        zIndex: 1,
        width: '100%',
        height: 54,
        padding: '0px 24px',
        display: 'flex',
        background: storeTheme === ThemeEnum.DARK ? '' : colorBgContainer,
        transition: `all ${transitionTime}s`
      }}
    >
      <Flex style={{ width: '100%' }} justify="space-between">
        <AppLogo layoutMode={AppLogoLayoutEnum.LEVEL} />
        <AppOperateBar />
      </Flex>
    </Header>
  )
}

export default ClassicalHeader
