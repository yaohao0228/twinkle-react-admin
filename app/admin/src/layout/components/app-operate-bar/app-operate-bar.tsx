import { FullscreenExitOutlined, FullscreenOutlined, SearchOutlined, SettingFilled } from '@ant-design/icons'
import { Flex } from 'antd'
import { useState } from 'react'
import { motion } from 'framer-motion'

import { AppUserAvatar } from '.'
import { useDesignStore } from '@/store'
import { themeConfigEnums } from '@/theme'

const AppOperateBar: React.FC = () => {
  const { theme, setDesignDrawerOpen } = useDesignStore((state) => ({
    theme: state.headerSetting.theme,
    setDesignDrawerOpen: state.setDesignDrawerOpen
  }))

  const {
    customConfig: { fontColor, layoutBorderColor }
  } = themeConfigEnums[theme]

  // 全屏
  const [isFullscreen, setIsFullscreen] = useState(false)
  const handleFullscreen = () => {
    if (!isFullscreen) {
      if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen()
      }
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen()
      }
    }
    setIsFullscreen(!isFullscreen)
  }

  const operateList: {
    id: number
    icon: React.ReactNode
    onClick: () => void
  }[] = [
    {
      id: 1,
      icon: <SearchOutlined />,
      onClick: () => {}
    },
    {
      id: 2,
      icon: isFullscreen ? <FullscreenExitOutlined /> : <FullscreenOutlined />,
      onClick: handleFullscreen
    },
    {
      id: 3,
      icon: <SettingFilled />,
      onClick: () => setDesignDrawerOpen(true)
    }
  ]

  return (
    <>
      <Flex justify="space-between" align="center">
        {operateList.map((item) => (
          <div
            className="w-10 h-10 flex justify-center items-center rounded-lg mr-2 cursor-pointer text-base"
            style={{
              color: fontColor,
              border: `1px solid ${layoutBorderColor}`
            }}
            onClick={item.onClick}
            key={item.id}
          >
            <motion.div whileHover={{ rotate: 360 }}>{item.icon}</motion.div>
          </div>
        ))}
        <AppUserAvatar />
      </Flex>
    </>
  )
}

export default AppOperateBar
