import { useNavigate } from 'react-router-dom'
import { storage } from 'twinkle-storage'

import { Avatar, Dropdown, MenuProps, App } from 'antd'
import { ExclamationCircleFilled, PoweroffOutlined, UserOutlined } from '@ant-design/icons'

import { useDesignStore } from '@/store'
import { StorageCacheKey } from '@/types'
import { themeConfigEnums } from '@/theme'

const AppUserAvatar: React.FC = () => {
  const navigate = useNavigate()

  const { modal } = App.useApp()

  const {
    headerSetting: { theme }
  } = useDesignStore()
  const {
    customConfig: { layoutBorderColor, fontColor }
  } = themeConfigEnums[theme]

  const logoutClick = () => {
    modal.confirm({
      title: '提示',
      icon: <ExclamationCircleFilled />,
      content: '你确定要退出登录吗？',
      onOk: () => {
        navigate('/login')
        storage.remove(StorageCacheKey.TOKEN)
        storage.remove(StorageCacheKey.AUTH)
      }
    })
  }

  const items: MenuProps['items'] = [
    {
      key: 1,
      label: '个人信息',
      icon: <UserOutlined />
    },
    {
      key: 2,
      danger: true,
      label: '退出登录',
      icon: <PoweroffOutlined />,
      onClick: logoutClick
    }
  ]

  return (
    <>
      <Dropdown menu={{ items }}>
        <div
          className="flex justify-between items-center h-10 px-1 rounded-lg box-border cursor-pointer transition-all"
          style={{
            border: `1px solid ${layoutBorderColor}`
          }}
        >
          <Avatar className="cursor-pointer" size="default" icon={<UserOutlined />} />
          <div className="ml-1 text-xs flex items-center">
            <span className="text-[#5a5a5a] mr-1">上午好,</span>
            <span style={{ color: fontColor }}>Admin</span>
          </div>
        </div>
      </Dropdown>
    </>
  )
}

export default AppUserAvatar
