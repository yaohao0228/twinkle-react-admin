export interface AppLogoProps extends React.PropsWithChildren {
  layoutMode: AppLogoLayoutEnum
}

export enum AppLogoLayoutEnum {
  /**
   * 垂直布局
   */
  VERTICAL = 'vertical',
  /**
   * 水平布局
   */
  LEVEL = 'level'
}
