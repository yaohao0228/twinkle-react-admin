import { useNavigate } from 'react-router-dom'
import { useDesignStore, useEnvStore, useTabStore } from '@/store'
import { ThemeEnum } from '@/types'
import { themeConfigEnums } from '@/theme'
import { REDIRECT_PATH } from '@/router/constant'
import { AppLogoLayoutEnum, AppLogoProps } from './type'

const AppLogo: React.FC<AppLogoProps> = ({ layoutMode }) => {
  const { VITE_PROJECT_NAME } = useEnvStore()
  const { updateActiveTabKey } = useTabStore()
  const {
    menuSetting: { collapsed, theme: menuStoreTheme },
    headerSetting: { theme: headerStoreTheme }
  } = useDesignStore()

  const layoutModeEnums: Record<
    AppLogoLayoutEnum,
    () => { className: string; theme: ThemeEnum; allowCollapsed: boolean }
  > = {
    [AppLogoLayoutEnum.VERTICAL]: () => ({
      className: 'flex-col',
      theme: menuStoreTheme,
      allowCollapsed: true
    }),
    [AppLogoLayoutEnum.LEVEL]: () => ({
      className: 'm-0',
      theme: headerStoreTheme,
      allowCollapsed: false
    })
  }

  const {
    customConfig: { fontColor }
  } = themeConfigEnums[layoutModeEnums[layoutMode]().theme]

  // 项目名称 只有垂直布局才可以被折叠（隐藏）
  const AppProjectName: React.FC = () => {
    return layoutModeEnums[layoutMode]().allowCollapsed && collapsed ? (
      <></>
    ) : (
      <span
        className="ml-2 whitespace-nowrap text-2xl"
        style={{
          color: fontColor
        }}
      >
        {VITE_PROJECT_NAME}
      </span>
    )
  }

  const navigate = useNavigate()

  const logoClick = () => {
    navigate(REDIRECT_PATH)
    updateActiveTabKey(REDIRECT_PATH)
  }

  return (
    <h1
      className={`${
        layoutModeEnums[layoutMode]().className
      } flex justify-center items-center cursor-pointer overflow-hidden`}
      onClick={logoClick}
    >
      <img
        className="w-10 h-10"
        src={layoutModeEnums[layoutMode]().theme === ThemeEnum.DARK ? '/logo.svg' : '/logo_light.svg'}
        alt={VITE_PROJECT_NAME}
      />
      {<AppProjectName />}
    </h1>
  )
}

export default AppLogo
