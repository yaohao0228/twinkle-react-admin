import { HomeOutlined, UserOutlined } from '@ant-design/icons'
import { Breadcrumb } from 'antd'

const AppBreadcrumb: React.FC = () => {
  return (
    <Breadcrumb
      className=" ml-2"
      items={[
        {
          href: '',
          title: <HomeOutlined />
        },
        {
          href: '',
          title: (
            <>
              <UserOutlined />
              <span>Application List</span>
            </>
          )
        },
        {
          title: 'Application'
        }
      ]}
    />
  )
}

export default AppBreadcrumb
