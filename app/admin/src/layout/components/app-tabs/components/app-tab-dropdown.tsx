import { Dropdown, MenuProps } from 'antd'
import { CloseOutlined, FullscreenOutlined, ReloadOutlined, TagOutlined, TagsOutlined } from '@ant-design/icons'

import type { AppTabDropdownElement, AppTabDropdownProps } from '../index'
import { useState } from 'react'
import { REDIRECT_PATH } from '@/router/constant'
import { useTabStore } from '@/store/tab.store'
import { useNavigate } from 'react-router-dom'

const AppTabDropdown: React.FC<AppTabDropdownProps> = ({ element }) => {
  const navigate = useNavigate()
  const { deleteTab, deleteOther, deleteAll, setIsScreenFull, updateActiveTabKey } = useTabStore((state) => state)

  const [activeKey, setActiveKey] = useState<string>('')

  const dropdownOpenChange = (children: AppTabDropdownElement, open: boolean) => {
    open ? setActiveKey(children.key as string) : setActiveKey('')
  }

  const items: MenuProps['items'] = [
    {
      key: '1',
      label: '重新加载',
      icon: <ReloadOutlined />,
      onClick: () => {
        console.log(activeKey)
      }
    },
    {
      key: '2',
      label: '关闭标签',
      icon: <CloseOutlined />,
      disabled: activeKey === REDIRECT_PATH,
      onClick: () => {
        const { lastTab } = deleteTab(activeKey)
        lastTab && navigate(lastTab)
      }
    },
    {
      key: '3',
      label: '当前标签全屏',
      icon: <FullscreenOutlined />,
      onClick: () => {
        setIsScreenFull(true)
        navigate(activeKey)
        updateActiveTabKey(activeKey)
      }
    },
    {
      key: '4',
      label: '关闭其它标签',
      icon: <TagOutlined />,
      onClick: () => deleteOther(activeKey)
    },
    {
      key: '5',
      label: '关闭全部标签',
      icon: <TagsOutlined />,
      onClick: () => {
        deleteAll()
        navigate(REDIRECT_PATH)
      }
    }
  ]

  return (
    <Dropdown menu={{ items }} trigger={['contextMenu']} onOpenChange={(open) => dropdownOpenChange(element, open)}>
      {element}
    </Dropdown>
  )
}

export default AppTabDropdown
