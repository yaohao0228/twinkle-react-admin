export type AppTabDropdownElement = React.ReactElement<any, string | React.JSXElementConstructor<any>>

export interface AppTabDropdownProps {
  element: AppTabDropdownElement
}
