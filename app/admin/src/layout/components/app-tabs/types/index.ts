export * from './AppTabDropdownType'

export interface TabItemType {
  closeIcon?: boolean | React.ReactNode
  destroyInactiveTabPane?: boolean
  disabled?: boolean
  forceRender?: boolean
  key: string
  label: React.ReactNode
  children?: React.ReactNode
}
