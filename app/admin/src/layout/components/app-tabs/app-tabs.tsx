import { Tabs } from 'antd'
import React, { useEffect, useState } from 'react'
import { arrayMove, horizontalListSortingStrategy, SortableContext, useSortable } from '@dnd-kit/sortable'
import { CSS } from '@dnd-kit/utilities'
import { DndContext, DragEndEvent, PointerSensor, useSensor } from '@dnd-kit/core'

import { useAuthStore, useTabStore } from '@/store'

import './index.css'
import { TabItemType } from './types'
import { useNavigate } from 'react-router-dom'
import { AppTabDropdown } from '.'

import { REDIRECT_PATH } from '@/router/constant'

type TargetKey = React.MouseEvent | React.KeyboardEvent | string

interface DraggableTabPaneProps extends React.HTMLAttributes<HTMLDivElement> {
  'data-node-key': string
}

const DraggableTabNode = ({ ...props }: DraggableTabPaneProps) => {
  const { attributes, listeners, setNodeRef, transform, transition } = useSortable({
    id: props['data-node-key']
  })

  const style: React.CSSProperties = {
    ...props.style,
    transform: CSS.Transform.toString(transform && { ...transform, scaleX: 1 }),
    transition,
    cursor: 'move'
  }

  const element = React.cloneElement(props.children as React.ReactElement, {
    ref: setNodeRef,
    style,
    ...attributes,
    ...listeners
  })
  return <AppTabDropdown element={element}></AppTabDropdown>
}

const AppTabs: React.FC = () => {
  const { tabList, activeTabKey, updateActiveTabKey, deleteTab } = useTabStore((state) => state)
  const { menuList, setSelectMenuKey } = useAuthStore((state) => state)

  const [items, setItems] = useState<TabItemType[]>(tabList)
  const [activeKey, setActiveKey] = useState<string>(activeTabKey)

  const navigate = useNavigate()

  useEffect(() => {
    setItems(tabList)
    setActiveKey(activeTabKey)
  }, [tabList, activeTabKey])

  const sensor = useSensor(PointerSensor, {
    activationConstraint: { distance: 10 }
  })

  const onDragEnd = ({ active, over }: DragEndEvent) => {
    if (active.id !== over?.id) {
      setItems((prev) => {
        const activeIndex = prev.findIndex((i) => i.key === active.id)
        const overIndex = prev.findIndex((i) => i.key === over?.id)
        return arrayMove(prev, activeIndex, overIndex)
      })
    }
  }

  const tabClick = (key: string, event: TargetKey) => {
    console.log(event)
    if (activeKey === key) return
    // 获取当前点击tab所对应菜单的key
    if (key === REDIRECT_PATH) {
      setSelectMenuKey('')
    } else {
      const menu = menuList.find((menu) => menu.path === key)
      menu && setSelectMenuKey(menu.id)
    }
    updateActiveTabKey(key)
    navigate(key)
  }

  const tabEdit = (event: TargetKey, action: 'add' | 'remove') => {
    if (action === 'remove') {
      const { lastTab } = deleteTab(event as string)
      console.log(lastTab)
      lastTab && navigate(lastTab)
    }
  }

  return (
    <Tabs
      type="editable-card"
      tabPosition="top"
      items={items}
      activeKey={activeKey}
      hideAdd={true}
      onEdit={tabEdit}
      onTabClick={tabClick}
      renderTabBar={(tabBarProps, DefaultTabBar) => (
        <DndContext sensors={[sensor]} onDragEnd={onDragEnd}>
          <SortableContext items={items.map((i) => i.key)} strategy={horizontalListSortingStrategy}>
            <DefaultTabBar {...tabBarProps}>
              {(node) => (
                <DraggableTabNode {...node.props} key={node.key}>
                  {node}
                </DraggableTabNode>
              )}
            </DefaultTabBar>
          </SortableContext>
        </DndContext>
      )}
    />
  )
}

export default AppTabs
