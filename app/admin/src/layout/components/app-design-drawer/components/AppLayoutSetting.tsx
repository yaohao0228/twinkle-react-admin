import { Col, Row } from 'antd'
import { useDesignStore } from '@/store'
import { LayoutMode } from '@/types'

interface AppLayoutModeVesselProps extends React.PropsWithChildren {
  title: string
  mode: LayoutMode
}

const AppLayoutModeVessel: React.FC<AppLayoutModeVesselProps> = (props) => {
  const [layout, setLayout] = useDesignStore((state) => [state.layout, state.setLayout])

  const handlerLayout = (mode: LayoutMode) => {
    setLayout(mode)
  }

  return (
    <div
      className={`h-[100px] rounded cursor-pointer ${
        layout === props.mode ? 'border-blue-500' : 'border-[#e4e7ed]'
      } overflow-hidden hover:border-blue-500 border border-solid transition-all relative`}
      onClick={() => handlerLayout(props.mode)}
    >
      {props.children}
      <div className="absolute left-2/4 top-2/4 -translate-x-2/4 -translate-y-2/4 w-11 h-11  rounded-full border border-solid border-blue-400 flex justify-center items-center text-blue-400 text-xs">
        {props.title}
      </div>
    </div>
  )
}

const AppLayoutSetting: React.FC = () => {
  return (
    <>
      <Row className="mt-3" gutter={24}>
        <Col span={12}>
          <AppLayoutModeVessel title="默认" mode={LayoutMode.DEFAULT}>
            <div className="flex w-full h-full">
              <div className=" w-1/5 h-full bg-[#dcdfe6]"></div>
              <div className="flex-1">
                <div className="h-[12%] w-full bg-[#f2f6fc]"></div>
                <div className="h-[88%] p-[6%] box-border">
                  <div className="w-full h-full bg-[#f2f6fc]"></div>
                </div>
              </div>
            </div>
          </AppLayoutModeVessel>
        </Col>
        <Col span={12}>
          <AppLayoutModeVessel title="经典" mode={LayoutMode.CLASSICAL}>
            <div className="w-full h-full">
              <div className="w-full h-[12%] bg-[#f2f6fc]"></div>
              <div className="flex h-[88%]">
                <div className="w-1/5 h-full bg-[#dcdfe6]"></div>
                <div className="flex-1 h-full p-[4%] box-border">
                  <div className="w-full h-full bg-[#f2f6fc]"></div>
                </div>
              </div>
            </div>
          </AppLayoutModeVessel>
        </Col>
      </Row>
      <Row className="mt-3" gutter={24}>
        <Col span={12}>
          <AppLayoutModeVessel title="单栏" mode={LayoutMode.SINGLE}>
            <div className="w-full h-full">
              <div className="w-full h-[12%] bg-[#dcdfe6]"></div>
              <div className="flex justify-center h-[88%]">
                <div className="w-[80%] h-full bg-[#f2f6fc]"></div>
              </div>
            </div>
          </AppLayoutModeVessel>
        </Col>
      </Row>
    </>
  )
}

export default AppLayoutSetting
