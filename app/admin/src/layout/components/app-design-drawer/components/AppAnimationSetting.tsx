import { Form, InputNumber } from 'antd'

import { useDesignStore } from '@/store'
import { designSetting } from '@/settings'

const AppAnimationSetting: React.FC = () => {
  const [transitionTime, setTransitionTime] = useDesignStore((state) => [state.transitionTime, state.setTransitionTime])

  return (
    <>
      <Form.Item
        name="transitionTime"
        className="mb-2"
        label={<div className="text-[#606266]">过渡时长</div>}
        wrapperCol={{ offset: 8 }}
        colon={false}
        initialValue={transitionTime}
      >
        <InputNumber
          max="1"
          min="0.1"
          step="0.1"
          stringMode
          value={transitionTime}
          onChange={(value) => setTransitionTime(value || designSetting.transitionTime)}
        />
      </Form.Item>
    </>
  )
}

export default AppAnimationSetting
