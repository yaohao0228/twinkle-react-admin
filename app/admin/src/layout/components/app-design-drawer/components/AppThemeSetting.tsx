import { Col, Row, Tooltip } from 'antd'
import { useDesignStore } from '@/store'
import { ThemeEnum } from '@/types'

interface AppThemeModeVesselProps extends React.PropsWithChildren {
  title: string
  themeMode: ThemeEnum
}

const AppThemeModeVessel: React.FC<AppThemeModeVesselProps> = ({ children, title, themeMode }) => {
  const [systemTheme, setSystemTheme] = useDesignStore((state) => [state.systemTheme, state.setSystemTheme])
  return (
    <Tooltip title={title}>
      <div
        className={`h-10 w-full rounded cursor-pointer overflow-hidden border border-solid ${
          systemTheme === themeMode ? 'border-blue-500' : 'border-[#e4e7ed]'
        } hover:border-blue-500 transition-all relative`}
        onClick={() => setSystemTheme(themeMode)}
      >
        {children}
      </div>
    </Tooltip>
  )
}

const AppThemeSetting: React.FC = () => {
  return (
    <>
      <Row gutter={24}>
        <Col span={6}>
          <AppThemeModeVessel title="明亮主题" themeMode={ThemeEnum.LIGHT}>
            <div className="flex w-full h-full">
              <div className=" w-1/5 h-full bg-[#dcdfe6]"></div>
              <div className="flex-1">
                <div className="h-[12%] w-full bg-[#f2f6fc]"></div>
                <div className="h-[88%] p-[6%] box-border">
                  <div className="w-full h-full bg-[#f2f6fc]"></div>
                </div>
              </div>
            </div>
          </AppThemeModeVessel>
        </Col>
        <Col span={6}>
          <AppThemeModeVessel title="暗黑主题" themeMode={ThemeEnum.DARK}>
            <div className="flex w-full h-full bg-[#15161A]">
              <div className=" w-1/5 h-full bg-[#1E1F25]"></div>
              <div className="flex-1">
                <div className="h-[12%] w-full bg-[#151212]"></div>
                <div className="h-[88%] p-[6%] box-border">
                  <div className="w-full h-full bg-[#1E1F25]"></div>
                </div>
              </div>
            </div>
          </AppThemeModeVessel>
        </Col>
      </Row>
    </>
  )
}

export default AppThemeSetting
