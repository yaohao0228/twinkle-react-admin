import { Form, Switch } from 'antd'
import { useDesignStore } from '@/store'
import { ThemeEnum } from '@/types'

const AppSideSetting: React.FC = () => {
  const [collapsed, setCollapsed] = useDesignStore((state) => [state.menuSetting.collapsed, state.setMenuSetting])
  const [theme, setTheme] = useDesignStore((state) => [state.menuSetting.theme, state.setMenuSetting])
  const [fixed, setFixed] = useDesignStore((state) => [state.menuSetting.fixed, state.setMenuSetting])

  return (
    <>
      <Form.Item
        name="collapsed"
        className="mb-2"
        label={<div className="text-[#606266]">菜单栏折叠</div>}
        wrapperCol={{ offset: 12 }}
        colon={false}
      >
        <Switch
          checkedChildren="开"
          unCheckedChildren="关"
          checked={collapsed}
          onChange={(checked: boolean) => setCollapsed('collapsed', checked)}
        />
      </Form.Item>
      <Form.Item
        className="mb-2"
        name="theme"
        label={<div className="text-[#606266]">菜单栏主题</div>}
        wrapperCol={{ offset: 12 }}
        colon={false}
      >
        <Switch
          checkedChildren="明"
          unCheckedChildren="暗"
          checked={theme === ThemeEnum.LIGHT}
          onChange={(checked: boolean) => setTheme('theme', checked ? ThemeEnum.LIGHT : ThemeEnum.DARK)}
        />
      </Form.Item>
      <Form.Item
        className="mb-2"
        name="fixed"
        label={<div className="text-[#606266]">菜单栏固定</div>}
        wrapperCol={{ offset: 12 }}
        colon={false}
      >
        <Switch
          checkedChildren="开"
          unCheckedChildren="关"
          checked={fixed}
          onChange={(checked: boolean) => setFixed('fixed', checked)}
        />
      </Form.Item>
    </>
  )
}

export default AppSideSetting
