import { Form, Switch } from 'antd'
import { useDesignStore } from '@/store'
import { ThemeEnum } from '@/types'

const AppHeaderSetting: React.FC = () => {
  const [theme, setTheme] = useDesignStore((state) => [state.headerSetting.theme, state.setHeaderSetting])
  const [fixed, setFixed] = useDesignStore((state) => [state.headerSetting.fixed, state.setHeaderSetting])

  return (
    <>
      <Form.Item
        name="theme"
        className="mb-2"
        label={<div className="text-[#606266]">顶栏主题</div>}
        wrapperCol={{ offset: 12 }}
        colon={false}
      >
        <Switch
          checkedChildren="明"
          unCheckedChildren="暗"
          checked={theme === ThemeEnum.LIGHT}
          onChange={(checked: boolean) => setTheme('theme', checked ? ThemeEnum.LIGHT : ThemeEnum.DARK)}
        />
      </Form.Item>
      <Form.Item
        name="collapsed"
        className="mb-2"
        label={<div className="text-[#606266]">顶栏固定</div>}
        wrapperCol={{ offset: 12 }}
        colon={false}
      >
        <Switch
          checkedChildren="开"
          unCheckedChildren="关"
          checked={fixed}
          onChange={(checked: boolean) => setFixed('fixed', checked)}
        />
      </Form.Item>
    </>
  )
}

export default AppHeaderSetting
