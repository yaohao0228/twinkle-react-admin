import React from 'react'
import { Drawer, Divider, Form, Button, message, App } from 'antd'
import { debounce } from 'lodash'
import { ExclamationCircleFilled, ReloadOutlined } from '@ant-design/icons'

import { useDesignStore } from '@/store'
import { themeConfigEnums } from '@/theme'

import AppSideSetting from './components/AppSideSetting'
import AppLayoutSetting from './components/AppLayoutSetting'
import AppHeaderSetting from './components/AppHeaderSetting'
import AppAnimationSetting from './components/AppAnimationSetting'
import AppThemeSetting from './components/AppThemeSetting'

interface DesignDrawerTitleProps extends React.PropsWithChildren {
  title: string
}

const DesignDrawerTitle: React.FC<DesignDrawerTitleProps> = (props) => {
  const systemTheme = useDesignStore((state) => state.systemTheme)
  const {
    customConfig: { fontColor }
  } = themeConfigEnums[systemTheme]

  return (
    <Divider dashed>
      <div className="font-bold text-sm" style={{ color: fontColor }}>
        {props.title}
      </div>
    </Divider>
  )
}

const AppDesignDrawer: React.FC = () => {
  const { modal } = App.useApp()

  const [designDrawerOpen, setDesignDrawerOpen] = useDesignStore((state) => [
    state.designDrawerOpen,
    state.setDesignDrawerOpen
  ])

  const { resetDesign } = useDesignStore((state) => ({
    resetDesign: state.resetDesign
  }))

  const resetDesignClick = debounce(() => {
    modal.confirm({
      title: '提示',
      icon: <ExclamationCircleFilled />,
      content: '你确定需要重置所有样式吗？',
      onOk: () => {
        resetDesign()
        message.success('样式重置成功！')
      }
    })
  }, 500)

  return (
    <Drawer
      title={<div className=" text-sm font-normal text-[#72767b]">布局配置</div>}
      width={320}
      placement="right"
      open={designDrawerOpen}
      onClose={() => setDesignDrawerOpen(false)}
      styles={{
        body: {
          paddingTop: 0
        }
      }}
      footer={
        <Button className="w-full" type="primary" icon={<ReloadOutlined />} onClick={resetDesignClick}>
          重置所有样式
        </Button>
      }
    >
      <Form labelAlign="left" labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>
        <DesignDrawerTitle title="布局方式" />
        <AppLayoutSetting />
        <DesignDrawerTitle title="主题" />
        <AppThemeSetting />
        <DesignDrawerTitle title="菜单栏" />
        <AppSideSetting />
        <DesignDrawerTitle title="顶栏" />
        <AppHeaderSetting />
        <DesignDrawerTitle title="动画" />
        <AppAnimationSetting />
      </Form>
    </Drawer>
  )
}

export default AppDesignDrawer
