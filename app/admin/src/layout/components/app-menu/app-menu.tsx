import { Menu, MenuProps } from 'antd'
import { useEffect, useState } from 'react'
import { isNotEmpty } from 'twinkle-utils'
import { storage } from 'twinkle-storage'

import { useAuthStore, useDesignStore } from '@/store'
import { MenuType, StorageCacheKey, MenuTypeEnum } from '@/types'

import { useMenu } from '.'

const AppMenu: React.FC = () => {
  const { clickMenuItem, handlerMenu } = useMenu()
  const { selectMenuKey } = useAuthStore((state) => state)

  const {
    transitionTime,
    menuSetting: { theme }
  } = useDesignStore((state) => state)

  const [menus, setMenus] = useState<MenuProps['items']>([])
  const [cacheMenu, setCacheMenu] = useState<MenuType[]>([])

  useEffect(() => {
    const permissionAndMenu = storage.getItem<MenuType[]>(StorageCacheKey.MENU)
    if (isNotEmpty(permissionAndMenu)) {
      const menus = permissionAndMenu.filter((menu) => menu.type === MenuTypeEnum.MENU)
      const menuList = handlerMenu(menus)
      setMenus(menuList)
      setCacheMenu(menus)
    }
  }, [])

  return (
    <Menu
      theme={theme}
      mode="inline"
      items={menus}
      selectedKeys={[selectMenuKey]}
      style={{
        transition: `all ${transitionTime}s`
      }}
      onClick={({ key }) => clickMenuItem(cacheMenu, +key)}
    />
  )
}

export default AppMenu
