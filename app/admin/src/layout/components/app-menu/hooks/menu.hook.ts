import React from 'react'
import { MenuProps } from 'antd'
import { useNavigate } from 'react-router-dom'
import { isEmpty, isUndefined, listBuildTree } from 'twinkle-utils'

import { useAuthStore, useTabStore } from '@/store'
import { MenuType, MenuTypeEnum } from '@/types'

import * as Icons from '@ant-design/icons'

const iconList = Icons

export const useMenu = () => {
  const navigate = useNavigate()
  const { setSelectMenuKey } = useAuthStore((state) => state)
  const { addTab, updateActiveTabKey } = useTabStore((state) => state)

  const handlerMenu = (menus: MenuType[]): MenuProps['items'] => {
    menus = menus.filter((menu) => menu.isShow && menu.type === MenuTypeEnum.MENU).sort((a, b) => a.sort - b.sort)
    const menuList = menus.map((menu) => ({
      key: menu.id,
      icon: addIconToMenu(menu.icon as keyof typeof iconList),
      label: menu.name,
      parentid: menu.parentId
    }))
    return listBuildTree(menuList, 'key', 'parentid')
  }

  const addIconToMenu = (menuIcon: keyof typeof iconList): React.ReactElement | '' => {
    if (isEmpty(menuIcon)) return ''
    return React.createElement((iconList as any)[menuIcon])
  }

  const findObjectById = (menus: MenuType[], idToFind: number): MenuType | null => {
    const menu = menus.find((menu) => menu.id === idToFind)
    if (isUndefined(menu)) {
      return null
    } else {
      return menu
    }
  }

  const clickMenuItem = (storeMenuList: MenuType[], key: number) => {
    const clickMenu = findObjectById(storeMenuList, key)
    clickMenu && addTab(clickMenu)
    clickMenu && updateActiveTabKey(clickMenu.path)
    clickMenu && setSelectMenuKey(`${clickMenu.id}`)
    clickMenu && clickMenu.path && navigate(clickMenu.path)
  }

  return { handlerMenu, addIconToMenu, findObjectById, clickMenuItem }
}
