import DefaultLayout from './modules/default'
import ClassicalLayout from './modules/classical'
import SingleLayout from './modules/single'
import PageLayout from './modules/page'
import AppDesignDrawer from './components/app-design-drawer/app-design-drawer'

import { useDesignStore } from '@/store'
import { useRoute } from '@/router/hooks/route.hook'

import { App, ConfigProvider } from 'antd'
import { LayoutMode } from '@/types/system/layout.type'
import { themeConfigEnums } from '@/theme'

const layoutEnum: Record<string, JSX.Element> = {
  [LayoutMode.DEFAULT]: <DefaultLayout />,
  [LayoutMode.CLASSICAL]: <ClassicalLayout />,
  [LayoutMode.SINGLE]: <SingleLayout />,
  [LayoutMode.PAGE]: <PageLayout />
}

const DynamicLayout: React.FC = () => {
  const { layout, systemTheme } = useDesignStore((state) => ({
    layout: state.layout,
    systemTheme: state.systemTheme
  }))
  const { searchRoute } = useRoute()
  const route = searchRoute()

  return (
    <ConfigProvider theme={themeConfigEnums[systemTheme].themeConfig}>
      <App>
        {route?.meta?.isPageLayout ? layoutEnum[LayoutMode.PAGE] : layoutEnum[layout as LayoutMode]}
        <AppDesignDrawer />
      </App>
    </ConfigProvider>
  )
}

export default DynamicLayout
