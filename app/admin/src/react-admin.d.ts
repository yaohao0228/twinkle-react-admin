/// <reference types="vite/client" />

declare module 'vite-plugin-eslint'

/**
 * 环境变量
 */
declare interface ImportMetaEnv extends Readonly<Record<string, string>> {
  /**
   * 项目名称
   */
  readonly VITE_PROJECT_TITLE: string
  /**
   * 请求地址
   */
  readonly VITE_HTTP_URL: string
}

declare interface ImportMeta {
  readonly env: ImportMetaEnv
}
