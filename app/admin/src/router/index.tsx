import { BrowserRouter } from 'react-router-dom'
import { Router } from './routes'

const Root: React.FC = () => {
  return (
    <BrowserRouter>
      <Router />
    </BrowserRouter>
  )
}

export default Root
