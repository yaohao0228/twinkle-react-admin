import { useLocation } from 'react-router-dom'
import { cloneDeep } from 'lodash'

import { RootRoute } from '@/router/routes'
import LazyLoad from '@/router/utils/lazy-load'

import { MenuTypeEnum, AppRouteObject } from '@/types'
import { MenuType } from '@/types/system/menu.type'

/**
 * 路由相关hook
 */
const useRoute = () => {
  /**
   * 查询路由信息
   * @param {string} path 路径
   * @param {AppRouteObject[]} routes 路由数据
   */
  const searchRoute = (
    path: string = useLocation().pathname || '',
    routes: AppRouteObject[] = RootRoute[0].children || []
  ): AppRouteObject => {
    let result: AppRouteObject = {}
    for (const route of routes) {
      if (route.path === path) return route
      if (route.children) {
        const res = searchRoute(path, route.children)
        if (Object.keys(res).length) result = res
      }
    }
    return result
  }

  /**
   * 处理路由菜单
   * @param {MenuType[]} menus 菜单类型
   */
  const handlerRoutes = (menus: MenuType[]): AppRouteObject[] => {
    menus = menus.filter((menu) => menu.isShow && menu.type === MenuTypeEnum.MENU)
    const routes: AppRouteObject[] = menus.map<AppRouteObject>((menu) => ({
      path: menu.path,
      element: LazyLoad(() => addElementToRoute(menu.element)),
      meta: {
        ...cloneDeep(menu),
        key: menu.name,
        title: menu.name
      }
    }))
    return routes
  }

  const addElementToRoute = (elementPath: string) => {
    // 默认表格页面
    if (elementPath === 'table-view') {
      return import('../../views/template/twinkle-table-view/twinkle-table-view')
    }
    return import(/* @vite-ignore */ `../../views/${elementPath}`)
  }

  return { searchRoute, handlerRoutes, addElementToRoute }
}

export { useRoute }
