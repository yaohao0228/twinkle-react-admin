import { ComponentType, Suspense, lazy } from 'react'
import RouterGuard from '../guard/RouterGuard'
import { Spin } from 'antd'

/**
 * 路由懒加载组件
 */
const LazyLoad = (importFn: () => Promise<{ default: ComponentType<any> }>) => {
  const Element = lazy(importFn)
  const lazyElement = (
    <Suspense fallback={<Spin size="large" />}>
      <Element />
    </Suspense>
  )

  return <RouterGuard element={lazyElement}></RouterGuard>
}

export default LazyLoad
