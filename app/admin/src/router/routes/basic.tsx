import { Navigate } from 'react-router-dom'
import { RoutePathEnum, AppRouteObject } from '@/types'

import { SysLogin } from '@/views/system/login'

import LazyLoad from '../utils/lazy-load'
import { REDIRECT_PATH } from '../constant'

/**
 * 静态路由
 */
const basicRoutes: AppRouteObject[] = [
  {
    path: RoutePathEnum.LOGIN,
    element: <SysLogin />,
    meta: {
      code: 'login',
      name: '登录',
      isPageLayout: true
    }
  },
  {
    path: RoutePathEnum.WORKBENCH,
    element: LazyLoad(() => import('@/views/dashboard/workbench/Workbench')),
    meta: {
      code: 'workbench',
      name: '工作台'
    }
  },
  {
    path: '/',
    element: <Navigate to={REDIRECT_PATH} />,
    meta: {
      code: 'redirect'
    }
  },
  {
    path: '/*',
    element: LazyLoad(() => import('@/views/system/error/sys-error')),
    meta: {
      code: 'sysError',
      name: '404 Not Found'
    }
  }
]

export { basicRoutes }
