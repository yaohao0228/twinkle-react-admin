import { useEffect } from 'react'
import { useRoutes } from 'react-router-dom'
import { isNotEmpty } from 'twinkle-utils'
import { storage } from 'twinkle-storage'

import { MenuTypeEnum, AppRouteObject, StorageCacheKey, MenuType } from '@/types'

import { basicRoutes } from './basic'
import { useRoute } from '@/router/hooks/route.hook'
import Layout from '@/layout'

const RootRoute: AppRouteObject[] = [
  {
    path: '/',
    element: <Layout />,
    children: []
  }
]

/**
 * 初始化Basic路由
 * @param {AppRouteObject[]} basicRoutes 基础静态路由
 */
const setupBasicRoutes = (basicRoutes: AppRouteObject[]) => {
  RootRoute[0].children = basicRoutes
}
setupBasicRoutes(basicRoutes)

/**
 * 动态路由
 */
const Router = () => {
  const menuList = storage.getItem<MenuType[]>(StorageCacheKey.MENU)
  const { handlerRoutes } = useRoute()

  useEffect(() => {
    if (isNotEmpty(menuList)) {
      const menus = menuList.filter((menu) => menu.type === MenuTypeEnum.MENU)
      const routes = handlerRoutes(menus) || []
      routes.forEach((route) => {
        if (
          RootRoute[0].children?.some((item) => {
            return item.path !== route.path
          })
        ) {
          RootRoute[0].children?.push(route)
        }
      })
    }
  }, [menuList])

  const element = useRoutes(RootRoute)
  return <>{element}</>
}

export { Router, RootRoute }
