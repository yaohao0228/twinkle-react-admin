import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { isEmpty } from 'twinkle-utils'
import { storage } from 'twinkle-storage'

import { StorageCacheKey } from '@/types/system/storage-cache.type'
import { RoutePathEnum } from '@/types/system/route.type'

export interface AuthRouterProps {
  element: React.ReactNode
}

/**
 * 路由守卫
 */
const RouterGuard: React.FC<AuthRouterProps> = ({ element }) => {
  const navigate = useNavigate()

  useEffect(() => {
    // 判断是否有token
    const checkToken = () => {
      const token = storage.getItem<string>(StorageCacheKey.TOKEN)
      if (isEmpty(token)) navigate(RoutePathEnum.LOGIN)
    }

    checkToken()
  }, [])

  return element
}

export default RouterGuard
