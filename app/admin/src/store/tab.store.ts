import { create } from 'zustand'

import { TabItemType } from '@/layout/components/app-tabs'
import { REDIRECT_PATH } from '@/router/constant'
import { MenuType } from '@/types/system/menu.type'

export interface TabStoreState {
  tabList: TabItemType[]
  activeTabKey: string
  isScreenFull: boolean
}

export interface TabStoreAction {
  addTab: (menu: MenuType) => void
  deleteTab: (key: string) => { lastTab?: string }
  deleteAll: () => void
  deleteOther: (key: string) => void
  updateActiveTabKey: (activeTabKey: string) => void
  setIsScreenFull: (status: boolean) => void
}

export type TabStore = TabStoreState & TabStoreAction

const setupState = (): TabStoreState => {
  return {
    isScreenFull: false,
    tabList: [{ label: '工作台', key: REDIRECT_PATH, closeIcon: false }],
    activeTabKey: REDIRECT_PATH
  }
}

const useTabStore = create<TabStore>((set, get) => ({
  ...setupState(),
  addTab: (menu) => {
    const tabList = get().tabList
    // 判断新增标签是否已存在
    if (tabList.some((tab) => tab.key === menu.path)) return
    const tab: TabItemType = {
      label: menu.name,
      key: menu.path
    }
    tabList.push(tab)
    set({ tabList: [...tabList] })
  },
  deleteTab: (key) => {
    let lastTab: string | undefined = undefined
    const { tabList, activeTabKey } = get()
    const newTabList = tabList.filter((tab, index) => {
      // 找到当前需要删除的那一项
      if (tab.key === key && activeTabKey === key) {
        // 如果当前位置处于第一个就展示重定向路由 否则展示前一项
        if (index !== 0) {
          set({ activeTabKey: tabList[index - 1].key })
          lastTab = tabList[index - 1].key
        } else {
          set({ activeTabKey: REDIRECT_PATH })
          lastTab = REDIRECT_PATH
        }
      }
      return tab.key !== key
    })
    set({ tabList: newTabList })
    return { lastTab }
  },
  deleteAll: () => {
    if (get().activeTabKey === REDIRECT_PATH) return
    set({
      tabList: [{ label: '工作台', key: REDIRECT_PATH, closeIcon: false }],
      activeTabKey: REDIRECT_PATH
    })
  },
  deleteOther: (key) => {
    const tabList = get().tabList
    const activeTab = tabList.find((tab) => tab.key === key)
    activeTab &&
      set({
        tabList: [{ label: '工作台', key: REDIRECT_PATH, closeIcon: false }, activeTab]
      })
  },
  updateActiveTabKey: (activeTabKey) => {
    set({ activeTabKey })
  },
  setIsScreenFull: (status) => {
    set({ isScreenFull: status })
  }
}))

export { useTabStore }
