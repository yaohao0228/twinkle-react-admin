import { create } from 'zustand'
import { createJSONStorage, persist } from 'zustand/middleware'
import { customPersist } from './pipe/persist'

import { StorageCacheKey } from '@/types'

interface EnvState extends Partial<ImportMetaEnv> {}

interface EnvAction {
  setBaseUrl: (value: string) => void
  setEnv: (envConfig: Partial<ImportMetaEnv>) => void
}

type EnvStore = EnvState & EnvAction

const setupState = (): EnvState => {
  return {}
}

const useEnvStore = create<EnvStore, any>(
  persist(
    (set) => ({
      ...setupState(),
      setBaseUrl: (value) => set({ VOI_ENV_BASE_URL: value }),
      setEnv: (envConfig: Partial<ImportMetaEnv>) => {
        set(envConfig)
      }
    }),
    {
      name: StorageCacheKey.ENV_CONFIG,
      storage: createJSONStorage(() => customPersist)
    }
  )
)

export { useEnvStore }
