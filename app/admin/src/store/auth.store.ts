import { create } from 'zustand'
import { createJSONStorage, persist } from 'zustand/middleware'
import { customPersist } from './pipe/persist'

import { StorageCacheKey } from '@/types/system/storage-cache.type'
import { MenuType } from '@/types/system/menu.type'

export interface AuthStoreState {
  token: string
  menuList: MenuType[]
  selectMenuKey: string
}

export interface AuthStoreAction {
  setToken: (token: string) => void
  setMenuList: (value: MenuType[]) => void
  setSelectMenuKey: (menuKey: string) => void
}

export type AuthStore = AuthStoreState & AuthStoreAction

const setupState = (): AuthStoreState => {
  return {
    token: '',
    menuList: [],
    selectMenuKey: ''
  }
}

const useAuthStore = create<AuthStore, any>(
  persist(
    (set) => ({
      ...setupState(),
      setToken: (value) => {
        set({ token: value })
      },
      setMenuList: (value) => {
        set({ menuList: value })
      },
      setSelectMenuKey: (menuKey) => {
        set({ selectMenuKey: menuKey })
      }
    }),
    {
      name: StorageCacheKey.AUTH,
      storage: createJSONStorage(() => customPersist)
    }
  )
)

export { useAuthStore }
