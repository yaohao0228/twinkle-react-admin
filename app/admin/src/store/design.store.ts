import { create } from 'zustand'
import { createJSONStorage, persist } from 'zustand/middleware'

import { designSetting } from '@/settings'
import { customPersist } from './pipe/persist'
import { DesignSetting, HeaderSetting, MenuSetting } from '@/types/system/setting.type'
import { LayoutMode, ThemeEnum } from '@/types/system/layout.type'
import { StorageCacheKey } from '@/types/system/storage-cache.type'

export interface DesignStore extends DesignSetting {
  setLayout: (value: LayoutMode) => void
  setSystemTheme: (value: ThemeEnum) => void
  setTransitionTime: (value: string) => void
  setDesignDrawerOpen: (value: boolean) => void
  setMenuSetting: <T extends keyof MenuSetting>(key: T, value: MenuSetting[T]) => void
  setHeaderSetting: <T extends keyof HeaderSetting>(key: T, value: HeaderSetting[T]) => void
  resetDesign: () => void
}

const setupState = (): DesignSetting => {
  return {
    designDrawerOpen: false,
    layout: designSetting.layout,
    systemTheme: designSetting.systemTheme,
    transitionTime: designSetting.transitionTime,
    menuSetting: {
      collapsed: designSetting.menuSetting?.collapsed,
      theme: designSetting.menuSetting.theme,
      fixed: designSetting.menuSetting.fixed
    },
    headerSetting: {
      theme: designSetting.headerSetting.theme,
      fixed: designSetting.headerSetting.fixed
    }
  }
}

const useDesignStore = create<DesignStore, any>(
  persist(
    (set, get) => ({
      ...setupState(),
      setLayout: (value) => {
        set({ layout: value })
      },
      setSystemTheme: (value) => {
        const menuSetting = get().menuSetting
        const headerSetting = get().headerSetting
        menuSetting.theme = value
        headerSetting.theme = value
        set({ systemTheme: value, menuSetting, headerSetting })
      },
      setTransitionTime: (value) => {
        set({ transitionTime: value })
      },
      setDesignDrawerOpen: (value) => {
        set({ designDrawerOpen: value })
      },
      setMenuSetting: (key, value) => {
        const menuSetting = get().menuSetting
        menuSetting[key] = value
        set({ menuSetting })
      },
      setHeaderSetting: (key, value) => {
        const headerSetting = get().headerSetting
        headerSetting[key] = value
        set({ headerSetting })
      },
      resetDesign: () => {
        const defaultDesign: DesignSetting = JSON.parse(JSON.stringify(designSetting))
        set(defaultDesign)
      }
    }),
    {
      name: StorageCacheKey.DESIGN,
      storage: createJSONStorage(() => customPersist)
    }
  )
)

export { useDesignStore }
