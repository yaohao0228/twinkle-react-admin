import { StorageCacheKey } from '@/types/system/storage-cache.type'
import { storage } from 'twinkle-storage'
import { StateStorage } from 'zustand/middleware'

export const customPersist: StateStorage = {
  getItem: async (key: string): Promise<string | null> => {
    const KEY = key as StorageCacheKey
    return storage.getItem(KEY)
  },
  setItem: async (key: string, value: string): Promise<void> => {
    const KEY = key as StorageCacheKey
    storage.setItem(KEY, value)
  },
  removeItem: async (key: string): Promise<void> => {
    const KEY = key as StorageCacheKey
    storage.remove(KEY)
  }
}
