import { LayoutMode, ThemeEnum } from '@/types/system/layout.type'
import { DesignSetting } from '@/types/system/setting.type'

export const designSetting: DesignSetting = {
  /**
   * 布局模式
   */
  layout: LayoutMode.DEFAULT,
  /**
   * 系统主题
   */
  systemTheme: ThemeEnum.LIGHT,
  /**
   * 过渡时长
   */
  transitionTime: '0.3',
  /**
   * 侧边栏配置
   */
  menuSetting: {
    /**
     * 是否固定
     */
    fixed: true,
    /**
     * 是否折叠
     */
    collapsed: false,
    /**
     * 主题
     */
    theme: ThemeEnum.LIGHT
  },
  /**
   * 顶栏配置
   */
  headerSetting: {
    /**
     * 是否固定
     */
    fixed: false,
    /**
     * 主题
     */
    theme: ThemeEnum.LIGHT
  }
}
