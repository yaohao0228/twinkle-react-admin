import { DataSource } from 'twinkle-datasource'

export const datasource = new DataSource({ modelUrl: import.meta.env.VITE_HTTP_URL })
