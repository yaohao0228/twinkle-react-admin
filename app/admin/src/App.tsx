import React from 'react'
import Root from '@/router'
import { useSetup } from '@/hooks'

import '@/app.css'

const App: React.FC = () => {
  const { setupEnv } = useSetup()

  // 初始化环境配置
  setupEnv()

  // 初始化菜单
  // setupMenu()

  return <Root />
}

export default App
