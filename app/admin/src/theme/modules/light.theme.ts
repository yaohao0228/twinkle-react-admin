import { theme } from 'antd'
import { BaseThemeConfig } from '@/types'

export const lightConfig: BaseThemeConfig = {
  themeConfig: {
    algorithm: theme.defaultAlgorithm,
    components: {
      Layout: {
        bodyBg: '#FFFFFF',
        headerBg: '#FFFFFF',
        siderBg: '#FFFFFF'
      },
      Menu: {
        itemBg: '#FFFFFF',
        itemSelectedBg: '#e6f4ff'
      }
    }
  },
  customConfig: {
    layoutBorderColor: '#eeeeee',
    fontColor: 'rgba(0,0,0,0.8)',
    divBgColor: '#F5F5F5',
    divBgColorHover: '#dcdcdc'
  }
}
