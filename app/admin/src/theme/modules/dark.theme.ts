import { theme } from 'antd'
import { BaseThemeConfig } from '@/types'

export const darkConfig: BaseThemeConfig = {
  themeConfig: {
    algorithm: theme.darkAlgorithm,
    components: {
      Layout: {
        bodyBg: '#15161A',
        headerBg: '#15161A',
        siderBg: '#1E1F25'
      },
      Menu: {
        darkItemBg: '#1E1F25',
        darkItemSelectedBg: '#24252B'
      }
    }
  },
  customConfig: {
    layoutBorderColor: '#282C2F',
    fontColor: 'rgba(255,255,255,0.8)',
    divBgColor: '#1E1F25',
    divBgColorHover: '#24252B'
  }
}
