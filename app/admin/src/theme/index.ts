import { BaseThemeConfig, ThemeEnum } from '@/types'

import { darkConfig } from './modules/dark.theme'
import { lightConfig } from './modules/light.theme'

const themeConfigEnums: Record<ThemeEnum, BaseThemeConfig> = {
  [ThemeEnum.DARK]: darkConfig,
  [ThemeEnum.LIGHT]: lightConfig
}

export { themeConfigEnums }
