import React, { memo } from 'react'
import { TreeListProps } from './interface/props'

const TwinkleTreeList: React.FC<TreeListProps> = memo(() => {
  return <div>TwinkleTreeList</div>
})

export default TwinkleTreeList
