import React, { memo } from 'react'
import { FileUploadProps } from './interface/props'

const TwinkleFileUpload: React.FC<FileUploadProps> = memo(() => {
  return <div>TwinkleFileUpload</div>
})

export default TwinkleFileUpload
