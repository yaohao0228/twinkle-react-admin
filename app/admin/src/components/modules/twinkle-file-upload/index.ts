import TwinkleFileUpload from './twinkle-file-upload'

export type * from './interface/props'
export { TwinkleFileUpload }
