export interface AntdIconProps {
  /**
   * 图标名称
   */
  icon: string
}
