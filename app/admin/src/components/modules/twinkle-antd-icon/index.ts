import TwinkleAntdIcon from './twinkle-antd-icon'

export type * from './interface/props'
export { TwinkleAntdIcon }
