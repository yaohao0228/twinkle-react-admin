import { AntdIconProps } from './interface/props'
import * as Icons from '@ant-design/icons'
import React from 'react'

const iconList = Icons

const TwinkleAntdIcon: React.FC<AntdIconProps> = (props) => {
  return React.createElement((iconList as any)[props.icon])
}

export default TwinkleAntdIcon
