import React, { memo } from 'react'
import { QueryBarProps } from './interface/props'

const TwinkleQueryBar: React.FC<QueryBarProps> = memo(() => {
  return <div>TwinkleQueryBar</div>
})

export default TwinkleQueryBar
