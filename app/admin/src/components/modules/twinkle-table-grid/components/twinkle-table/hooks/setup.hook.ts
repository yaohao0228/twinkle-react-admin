import { useEffect, useState } from 'react'
import { cloneDeep, sortBy } from 'lodash'
import { isEmpty } from 'twinkle-utils'
import { DataSourceType, ViewObjectColumn } from 'twinkle-datasource'

import { EditType } from '@/types'
import { datasource } from '@/utils/datasource'

import { TwinkleTableSetupHookProps } from '../interface/props'
import { TableType } from '../interface/type'
import { PageResult } from 'twinkle-request'

export const useSetup = (props: TwinkleTableSetupHookProps) => {
  const { voName, columns, data, tableType } = props

  // 表格列配置
  const [handlerColumns, setHandlerColumns] = useState<ViewObjectColumn[]>([])
  // 表格数据
  const [handlerDataList, setHandlerDataList] = useState<Record<string, any>[]>([])
  // 表格加载状态
  const [loading, setLoading] = useState<boolean>(false)

  /**
   * 初始化数据源
   */
  const setupDataSource = async () => {
    if (voName) {
      await datasource.register(new DataSourceType({ key: voName }))
      const dataConfig = await datasource.getDataSource(voName)
      const { handlerAfterColumns } = handlerFormColumns(dataConfig.viewObject?.viewObjectColumn || [])
      setHandlerColumns(handlerAfterColumns)
      return
    }
    if (columns) {
      const { handlerAfterColumns } = handlerFormColumns(columns)
      setHandlerColumns(handlerAfterColumns)
      return
    }
    throw new Error('请传入voName或者columns')
  }

  /**
   * 初始化数据列表
   */
  const setupDataList = async () => {
    setLoading(true)
    if (data && data.length > 0) {
      setHandlerDataList(data)
      return setLoading(false)
    }
    if ((tableType === TableType.EDIT || tableType === TableType.VIEW) && voName) {
      const data = (await datasource.get(voName)) as PageResult
      setHandlerDataList(data.list)
      return setLoading(false)
    }
  }

  /**
   * 处理表单字段数据
   * @param {ViewObjectColumn[]} columns 列字段配置集合
   */
  const handlerFormColumns = (columns: ViewObjectColumn[]) => {
    const copyColumns = cloneDeep(columns)
    let handlerAfterColumns: ViewObjectColumn[] = []
    const childrenColumns: ViewObjectColumn[] = []
    copyColumns.forEach((item) => {
      if (item.editType === EditType.CHILDREN && item.isShow) {
        childrenColumns.push(item)
      } else if (item.isShow) {
        handlerAfterColumns.push(item)
      }
    })

    // 对字段进行排序
    handlerAfterColumns = sortBy(handlerAfterColumns, (item) => item.rank)

    return { handlerAfterColumns, childrenColumns }
  }

  useEffect(() => {
    if (isEmpty(voName)) return
    setupDataSource()
  }, [voName])

  useEffect(() => {
    if (handlerColumns.length <= 0 || isEmpty(tableType)) return
    setupDataList()
  }, [tableType, handlerColumns])

  return { handlerColumns, handlerDataList, loading }
}
