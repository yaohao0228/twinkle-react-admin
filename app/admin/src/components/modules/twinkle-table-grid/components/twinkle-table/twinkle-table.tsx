import { forwardRef, useState } from 'react'
import { Table } from 'antd'

import { TwinkleTableContainer, TwinkleTableProps } from './interface/props'
import { TableType } from './interface/type'
import { useSetup } from './hooks/setup.hook'
import { renderStrategy } from './config/render-strategy'

export const TwinkleTable = forwardRef<TwinkleTableContainer, TwinkleTableProps>((props, ref) => {
  const { voName = '', columns = [], data = [], queryConditions = [], ...tableProps } = props

  const [tableType, setTableType] = useState<TableType>(TableType.ADD)

  const { handlerColumns, handlerDataList, loading } = useSetup({
    voName,
    columns,
    data,
    tableType,
    queryConditions
  })

  return (
    <Table
      bordered
      rowKey="id"
      scroll={{ x: 1300, y: window.innerHeight - 330 }}
      loading={loading}
      rowSelection={{
        type: 'checkbox',
        onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
          console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows)
        },
        getCheckboxProps: () => ({
          disabled: undefined,
          name: undefined
        })
      }}
      pagination={paginationConfig}
      dataSource={handlerDataList}
      style={{ position: 'relative', zIndex: 1 }}
      {...tableProps}
    >
      {handlerColumns.map((column) => renderStrategy({ column, tableType, data, setData, setUpdateDataIds }))}
      {config.isShowAction && (config.gridType === 'view' ? renderAction : renderEditorAction)}
    </Table>
  )
})
