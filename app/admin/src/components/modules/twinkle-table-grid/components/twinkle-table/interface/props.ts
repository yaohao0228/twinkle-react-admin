import { TableProps } from 'antd'
import { ViewObjectColumn } from 'twinkle-datasource'
import { QueryCondition } from 'twinkle-request'

import { TableType } from './type'

export interface TwinkleTableProps extends Partial<TableProps<any>> {
  /**
   * 视图对象名称
   */
  voName?: string
  /**
   * 表格数据
   */
  data?: Record<string, any>[]
  /**
   * 表单字段列配置
   */
  columns?: ViewObjectColumn[]
  /**
   * 查询条件
   */
  queryConditions?: QueryCondition[]
}

export interface TwinkleTableContainer {}

type SetupHookBase = Pick<TwinkleTableProps, 'voName' | 'columns' | 'data' | 'queryConditions'>

export interface TwinkleTableSetupHookProps extends SetupHookBase {
  tableType: TableType
}
