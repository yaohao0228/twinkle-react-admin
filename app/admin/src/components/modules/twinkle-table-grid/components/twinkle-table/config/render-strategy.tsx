import React from 'react'
import { InputNumber, Input, Switch, Table, Tag } from 'antd'
import { isObject } from 'twinkle-utils'
import { ViewObjectColumn } from 'twinkle-datasource'

import { EditType } from '@/types'
import { TableType } from '../interface/type'

const { Column } = Table

interface RenderStrategyProps {
  column: ViewObjectColumn
  tableType: TableType
  data: any[]
  setData: React.Dispatch<React.SetStateAction<any[]>>
  setUpdateDataIds: React.Dispatch<React.SetStateAction<string[]>>
}

/**
 * 渲染策略
 * @param {RenderStrategyProps} props 列字段配置
 */
export const renderStrategy = (props: RenderStrategyProps): React.ReactNode => {
  const { column, tableType, data, setData, setUpdateDataIds } = props
  const style = column.editOption?.style || {}
  const options = column.editOption?.options || {}

  const onInputChange = (record: Record<string, any>, value: any) => {
    if (tableType === TableType.EDIT) {
      setUpdateDataIds((target) => {
        if (target.includes(record.id)) {
          return target
        } else {
          return [...target, record.id]
        }
      })
    }
    const newData = data.map((item) => {
      if (item.id === record.id) {
        item[column.code] = value
      }
      return item
    })
    setData(newData)
  }

  const strategy: Partial<Record<EditType, (value: any) => React.ReactNode>> = {
    [EditType.STRING]: (value) => <div style={style}>{(isObject(value) && JSON.stringify(value)) || value}</div>,
    [EditType.NUMBER]: (value) => <div style={style}>{value}</div>,
    [EditType.BOOLEAN]: (value) => (
      <Tag color={String(value) === 'true' ? 'processing' : 'error'}>{options[value] || value}</Tag>
    ),
    [EditType.ENUM]: (value) => <div style={style}>{options[value] || value}</div>,
    [EditType.SELECT]: (value) => <div style={style}>{value}</div>,
    [EditType.TEXTAREA]: (value) => <div style={style}>{value}</div>
  }

  const editableStrategy: Partial<Record<EditType, (value: any, record: Record<string, any>) => React.ReactNode>> = {
    [EditType.STRING]: (value, record) => {
      return (
        <div style={style}>
          <Input
            value={value}
            placeholder={`请输入${column.name}`}
            onChange={(event) => onInputChange(record, event.target.value)}
          />
        </div>
      )
    },
    [EditType.NUMBER]: (value, record) => (
      <div style={style}>
        <InputNumber
          className="w-full"
          placeholder={`请输入${column.name}`}
          value={value}
          onChange={(value) => onInputChange(record, value)}
        />
      </div>
    ),
    [EditType.BOOLEAN]: (value, record) => (
      <div style={style}>
        <Switch
          checked={value}
          checkedChildren={column.editOption.options?.['true']}
          unCheckedChildren={column.editOption.options?.['false']}
          onChange={(value) => onInputChange(record, value)}
        />
      </div>
    )
  }

  return (
    <Column
      ellipsis={true}
      sorter={true}
      key={column.code}
      width={column.width}
      title={column.name}
      dataIndex={column.code}
      render={
        tableType === TableType.VIEW
          ? strategy[column.editType as EditType]
          : editableStrategy[column.editType as EditType]
      }
    />
  )
}
