export enum TableType {
  ADD = 'add',
  EDIT = 'edit',
  VIEW = 'view'
}
