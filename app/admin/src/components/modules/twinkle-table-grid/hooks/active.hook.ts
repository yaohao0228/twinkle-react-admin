import { EventEnum } from '@/types'
import { datasource } from '@/utils/datasource'
import { eventBus, isNotEmpty } from 'twinkle-utils'
import { App } from 'antd'
import { useState } from 'react'
import { TablePaginationConfig, FilterValue, SorterResult } from 'antd/es/table/interface'

export interface TableGridActionProps {
  voName: string
  gridType: 'view' | 'edit' | 'add'
  setData: React.Dispatch<React.SetStateAction<any[]>>
}

export const useAction = (props: TableGridActionProps) => {
  const { voName, gridType, setData } = props
  const { message } = App.useApp()
  const [deleteDataIds, setDeleteDataIds] = useState<string[]>([])

  /** 点击查看 */
  const onClickByView = (row: any) => {
    eventBus.publish(EventEnum.FORM_VIEW_SHOW, true, 'view', row)
  }

  /** 点击编辑 */
  const onClickByEdit = (row: any) => {
    eventBus.publish(EventEnum.FORM_VIEW_SHOW, true, 'edit', row)
  }

  /** 点击删除 */
  const onDeleteRow = async (id: string) => {
    if (gridType === 'view') {
      await datasource.remove(voName, [id])
      message.success('删除成功')
      eventBus.publish(EventEnum.TABLE_GRID_RESET)
    }
    if (gridType === 'edit') {
      setDeleteDataIds((target) => {
        if (target.includes(id)) {
          return target
        } else {
          return [...target, id]
        }
      })
      setData((target) => target.filter((item) => String(item.id) !== String(id)))
    }
    if (gridType === 'add') {
      setData((target) => target.filter((item) => String(item.id) !== String(id)))
    }
  }

  /** 表格选中变化 */
  const onSelectChange = (selectedRowKeys: React.Key[], selectedRows: any[]) => {
    eventBus.publish(EventEnum.TABLE_SELECT_CHANGE, selectedRowKeys, selectedRows)
  }

  /** 表格变化事件 */
  const onTableChange = (
    _pagination: TablePaginationConfig,
    _filters: Record<string, FilterValue | null>,
    sorter: SorterResult<any> | SorterResult<any>[]
  ) => {
    // 处理服务端排序
    const sortData: Record<string, any> = {}
    if (Array.isArray(sorter)) {
      sorter.forEach((item) => (sortData[item.field as string] = item.order === 'ascend' ? 'ASC' : 'DESC'))
    } else {
      if (isNotEmpty(sorter.field)) {
        sortData[sorter.field as string] = sorter.order === 'ascend' ? 'ASC' : 'DESC'
      }
    }
    datasource.setSort(props.voName, sortData)
    eventBus.publish(EventEnum.TABLE_GRID_RESET)
  }

  return { deleteDataIds, onClickByView, onClickByEdit, onDeleteRow, onTableChange, onSelectChange }
}
