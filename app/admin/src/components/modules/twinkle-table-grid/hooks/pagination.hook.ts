import { useEffect, useState } from 'react'
import { TablePaginationConfig } from 'antd'
import { PageResult } from 'twinkle-request'
import { datasource } from '@/utils/datasource'

interface PaginationHookProps {
  voName: string
  pagination?: PageResult['pagination']
  setupData: () => Promise<void>
}

interface PaginationSum {
  current: number
  pageSize: number
  total: number
}

/**
 * 分页逻辑
 * @param {TableGridProps} props 组件参数
 */
export const usePagination = ({ voName, pagination, setupData }: PaginationHookProps) => {
  // 分页组件配置
  const [paginationConfig, setPaginationConfig] = useState<TablePaginationConfig>({})
  // 分页合计
  const [paginationSum, setPaginationSum] = useState<PaginationSum>({
    current: 1,
    pageSize: 20,
    total: pagination?.total || 0
  })

  useEffect(() => {
    setPaginationConfig({
      current: paginationSum.current,
      pageSize: paginationSum.pageSize,
      total: pagination?.total,
      showSizeChanger: true,
      showQuickJumper: true,
      pageSizeOptions: [20, 50, 100, 500],
      onChange,
      onShowSizeChange,
      showTotal: (total) => `共 ${total} 条`
    })
  }, [pagination])

  /**
   * 分页切换函数
   * @param {Number} current 当前页
   * @param {Number} pageSize 每页展示数
   */
  const onChange = (current: number, pageSize: number) => {
    setPaginationConfig((value) => ({ ...value, current }))
    datasource.setPageParameter(voName, { pageIndex: current, pageSize })
    setPaginationSum((value) => ({ ...value, current }))
    setupData()
  }

  const onShowSizeChange = (_: number, pageSize: number) => {
    setPaginationConfig((value) => ({ ...value, current: 1, pageSize }))
    datasource.setPageParameter(voName, { pageIndex: 1, pageSize })
    setPaginationSum((value) => ({ ...value, current: 1, pageSize }))
    setupData()
  }

  return { paginationConfig }
}
