import { useEffect, useState } from 'react'
import { cloneDeep, merge, sortBy } from 'lodash'
import { PageResult, QueryParameter } from 'twinkle-request'
import { ViewObjectColumn, DataSourceType } from 'twinkle-datasource'

import { TableGridProps } from '..'
import { datasource } from '@/utils/datasource'
import { eventBus } from 'twinkle-utils'
import { EditType, EventEnum } from '@/types'
import { TableProps } from 'antd'

export type GridType = 'view' | 'edit' | 'add'

/**
 * 初始化
 * @param {TableGridProps} props 组件参数
 */
export const useSetup = (props: TableGridProps) => {
  // 表格列配置
  const [columns, setColumns] = useState<ViewObjectColumn[]>([])
  // 表格数据
  const [data, setData] = useState<PageResult['list']>([])
  // 修改过的表格数据id
  const [updateDataIds, setUpdateDataIds] = useState<string[]>([])
  // 分页参数
  const [pagination, setPagination] = useState<PageResult['pagination']>()
  // 表格加载状态
  const [loading, setLoading] = useState<boolean>(true)
  // 表格参数配置
  const [config, setConfig] = useState<TableGridProps>(props)
  // 表格组件配置
  const [tableProps, setTableProps] = useState<TableProps>({})

  const setupDefaultProps = () => {
    setConfig({ isShowAction: true, gridType: 'view', ...props })
  }

  useEffect(() => {
    setupDefaultProps()
  }, [props])

  useEffect(() => {
    eventBus.subscribe(EventEnum.TABLE_GRID_RESET, setupData)
  }, [])

  /** 初始化数据源 */
  const setupDataSource = async () => {
    setLoading(true)
    if (props.columns && props.columns.length > 0) {
      const columns = handlerTableColumns(props.columns)
      setColumns(columns)
      return await setupData()
    }

    if (props.voName) {
      const queryParameter = new QueryParameter()
      queryParameter.queryConditions = props.queryConditions || []
      await datasource.register(new DataSourceType({ key: props.voName, queryParameter }))
      const config = await datasource.getDataSource(props.voName)
      const columns = handlerTableColumns(config.viewObject?.viewObjectColumn || [])
      setColumns(columns)
      return await setupData()
    }
  }

  /** 初始化数据 */
  const setupData = async () => {
    setLoading(true)
    if (props.data) {
      setData(props.data)
    } else if (props.gridType !== 'add' && props.voName) {
      const data = (await datasource.get(props.voName || '')) as PageResult
      setPagination(data.pagination)
      setData(data.list)
    }
    setLoading(false)
  }

  /** 初始化表格Props */
  const setupTableProps = () => {
    const defaultProps: TableProps = {}
    setTableProps(merge({}, defaultProps, props.antdTableProps))
  }

  /** 重置数据 */
  const reset = () => {
    setData([])
    setPagination(undefined)
  }

  useEffect(() => {
    setupDataSource()
    setupTableProps()
  }, [props.voName, props.columns, props.queryConditions])

  /**
   * 处理table字段数据
   * @param {ViewObjectColumn[]} columns 列字段配置集合
   */
  const handlerTableColumns = (columns: ViewObjectColumn[]): ViewObjectColumn[] => {
    const copyColumns = cloneDeep(columns)
    const handlerAfterColumns: ViewObjectColumn[] = []
    const childrenColumns: ViewObjectColumn[] = []
    copyColumns.forEach((item) => {
      if (item.editType === EditType.CHILDREN && item.isShow) {
        childrenColumns.push(item)
      } else if (item.isShow) {
        handlerAfterColumns.push(item)
      }
    })

    // 对字段进行排序
    const result = sortBy(handlerAfterColumns, (item) => item.rank)

    return result
  }

  return {
    handlerTableColumns,
    setupData,
    setData,
    reset,
    setupDataSource,
    setUpdateDataIds,
    updateDataIds,
    config,
    loading,
    columns,
    data,
    pagination,
    tableProps
  }
}
