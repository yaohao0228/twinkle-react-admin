import React, { memo, forwardRef, useImperativeHandle } from 'react'
import { Popconfirm, Button, Space, Table, Tooltip } from 'antd'

import { TableGridProps } from './interface/props'
import { TableGridContainer } from './interface/container'
import { useSetup } from './hooks/setup.hook'

import { renderStrategy } from './config/render-strategy'
import { DeleteOutlined, EditOutlined, FileSearchOutlined } from '@ant-design/icons'
import { usePagination } from './hooks/pagination.hook'
import { useAction } from './hooks/active.hook'

const { Column } = Table

const TwinkleTableGrid = memo(
  forwardRef<TableGridContainer, TableGridProps>((props, ref) => {
    const {
      columns,
      data,
      pagination,
      loading,
      config,
      tableProps,
      updateDataIds,
      reset,
      setData,
      setupData,
      setupDataSource,
      setUpdateDataIds
    } = useSetup(props)
    const { deleteDataIds, onClickByEdit, onClickByView, onDeleteRow, onTableChange, onSelectChange } = useAction({
      voName: config.voName || '',
      gridType: config.gridType || 'view',
      setData
    })
    const { paginationConfig } = usePagination({ voName: config.voName || '', pagination, setupData })

    useImperativeHandle(ref, () => ({
      voName: config.voName || '',
      data,
      columns,
      updateDataIds,
      deleteDataIds,
      reset,
      setData,
      setupDataSource
    }))

    const renderAction = (
      <Column
        width={150}
        title="操作"
        key="active"
        fixed="right"
        render={(value, record: Record<string, any> & { id: string }) => (
          <Space size="middle">
            <Tooltip title="编辑">
              <Button type="primary" size="small" icon={<EditOutlined />} onClick={() => onClickByEdit(value)} />
            </Tooltip>
            <Tooltip title="查看">
              <Button type="primary" size="small" icon={<FileSearchOutlined />} onClick={() => onClickByView(value)} />
            </Tooltip>
            <Popconfirm
              title="提示"
              description="你确定需要删除该数据吗？"
              onConfirm={() => onDeleteRow(record.id)}
              okText="确定"
              cancelText="取消"
            >
              <Button type="primary" size="small" danger icon={<DeleteOutlined />} />
            </Popconfirm>
          </Space>
        )}
      />
    )

    const renderEditorAction = (
      <Column
        width={150}
        title="操作"
        key="active"
        fixed="right"
        render={(_, record: Record<string, any> & { id: string }) => (
          <Space size="middle">
            <Popconfirm
              title="提示"
              description="你确定需要删除该数据吗？"
              onConfirm={() => onDeleteRow(record.id)}
              okText="确定"
              cancelText="取消"
            >
              <Button type="primary" size="small" danger icon={<DeleteOutlined />} />
            </Popconfirm>
          </Space>
        )}
      />
    )

    return (
      <Table
        bordered
        rowKey="id"
        scroll={{ x: 1300, y: window.innerHeight - 330 }}
        loading={loading}
        rowSelection={{
          type: 'checkbox',
          fixed: 'left',
          onChange: onSelectChange,
          getCheckboxProps: () => ({
            disabled: undefined,
            name: undefined
          })
        }}
        pagination={paginationConfig}
        dataSource={data || []}
        style={{ position: 'relative', zIndex: 1 }}
        onChange={onTableChange}
        {...tableProps}
      >
        {columns.map((column) => renderStrategy({ column, containerProps: config, data, setData, setUpdateDataIds }))}
        {config.isShowAction && (config.gridType === 'view' ? renderAction : renderEditorAction)}
      </Table>
    )
  })
)

export default TwinkleTableGrid
