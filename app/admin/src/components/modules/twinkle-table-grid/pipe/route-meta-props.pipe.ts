import { removeUndefinedProperties } from 'twinkle-utils'
import { RouteMeta } from '@/types'
import { TableGridProps } from '..'

/**
 * 路由元数据转化组件Props
 * @param {Object} routeMeta 路由元数据
 */
export const tableGridRouteMetaPropsPipe = (routeMeta: RouteMeta): Partial<TableGridProps> => {
  const props: Partial<TableGridProps> = {
    voName: routeMeta.code,
    queryConditions: routeMeta.editOption?.tableOptions?.props?.queryConditions,
    isShowPagination: routeMeta.editOption?.tableOptions?.props?.isShowPagination,
    isShowAction: routeMeta.editOption?.tableOptions?.props?.isShowAction
  }
  // 移除所有的undefined 避免覆盖默认值
  return removeUndefinedProperties(props)
}
