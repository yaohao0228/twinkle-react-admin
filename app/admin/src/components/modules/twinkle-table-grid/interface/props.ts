import { TableProps } from 'antd'
import { ViewObjectColumn } from 'twinkle-datasource'
import { QueryCondition } from 'twinkle-request'

/**
 * 表格参数
 */
export interface TableGridProps {
  /**
   * 视图对象名称
   */
  voName?: string
  /**
   * 列字段
   */
  columns?: ViewObjectColumn[]
  /**
   * 表格数据
   */
  data?: any[]
  /**
   * 表格类型
   */
  gridType?: 'view' | 'edit' | 'add'
  /**
   * 查询条件
   */
  queryConditions?: QueryCondition[]
  /**
   * 是否展示分页
   */
  isShowPagination?: boolean
  /**
   * 是否显示操作栏
   */
  isShowAction?: boolean
  /**
   * Antd表格配置
   */
  antdTableProps?: TableProps
}
