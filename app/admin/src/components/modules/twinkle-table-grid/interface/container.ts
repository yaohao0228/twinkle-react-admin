import { ViewObjectColumn } from 'twinkle-datasource'

export interface TableGridContainer {
  voName: string
  columns: ViewObjectColumn[]
  data: any[]
  updateDataIds: string[]
  deleteDataIds: string[]
  reset: () => void
  setData: React.Dispatch<React.SetStateAction<any[]>>
  setupDataSource: () => Promise<void>
}
