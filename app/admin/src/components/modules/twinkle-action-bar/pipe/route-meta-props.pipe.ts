import { RouteMeta } from '@/types'
import { ActionBarProps } from '../interface/props'
import { removeUndefinedProperties } from 'twinkle-utils'

/**
 * 路由元数据转化组件Props
 * @param {Object} routeMeta 路由元数据
 */
export const actionBarRouteMetaPropsPipe = (routeMeta: RouteMeta): Partial<ActionBarProps> => {
  const editOption = routeMeta.editOption
  const props: Partial<ActionBarProps> = {
    voName: editOption?.voName || routeMeta.code,
    buttons: editOption?.activeOptions?.props?.buttons
  }
  return removeUndefinedProperties(props)
}
