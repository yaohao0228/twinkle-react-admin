import { useEffect, useState } from 'react'
import { ActionBarProps } from '../interface/props'

/**
 * 初始化
 * @param {ActionBarProps} props 组件参数
 */
export const useSetup = (props: ActionBarProps) => {
  // 表格参数配置
  const [config, setConfig] = useState<ActionBarProps>(props)

  const setupDefaultProps = () => {
    setConfig({ ...props })
  }

  useEffect(() => {
    setupDefaultProps()
  }, [props])

  return { config }
}
