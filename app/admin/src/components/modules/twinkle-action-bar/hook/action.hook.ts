import { EventEnum } from '@/types'
import { ActionBarProps } from '..'
import { eventBus } from 'twinkle-utils'

export const useAction = (props: ActionBarProps) => {
  /** 添加按钮点击事件 */
  const onAddClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    const fn = props.onAddClick
    fn ? fn(event) : eventBus.publish(EventEnum.FORM_VIEW_SHOW, true, 'add')
  }

  /** 刷新按钮点击事件 */
  const onReset = () => {
    eventBus.publish(EventEnum.TABLE_GRID_RESET)
  }

  return { onAddClick, onReset }
}
