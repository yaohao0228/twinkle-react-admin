import { Button, Tooltip } from 'antd'
import React, { memo } from 'react'
import {
  DeleteOutlined,
  DownloadOutlined,
  ExportOutlined,
  FilterOutlined,
  ImportOutlined,
  PlusOutlined,
  RetweetOutlined
} from '@ant-design/icons'

import { ActionBarProps, ButtonCode } from './interface/props'
import { useAction } from './hook/action.hook'
import { useSetup } from './hook/setup.hook'

import { TwinkleActionButtons } from './components/twinkle-buttons'

const TwinkleActionBar: React.FC<ActionBarProps> = memo((props) => {
  const { config } = useSetup(props)
  const { onAddClick, onReset } = useAction(config)

  const isShowButton = (buttonType: ButtonCode) => !config.exclude?.includes(buttonType)

  return (
    <div className=" flex justify-between mb-2">
      <div className="flex items-center">
        {isShowButton('add') && (
          <Button className="mr-2" type="primary" icon={<PlusOutlined />} onClick={onAddClick}>
            新增
          </Button>
        )}
        {isShowButton('delete') && (
          <Button className="mr-2" type="primary" danger icon={<DeleteOutlined />}>
            批量删除
          </Button>
        )}
        <TwinkleActionButtons buttons={config.buttons || []} />
        {config.children}
      </div>
      <div className="flex items-center">
        {isShowButton('download') && (
          <Tooltip title="下载模版">
            <Button className="mr-2" icon={<DownloadOutlined />}></Button>
          </Tooltip>
        )}
        {isShowButton('import') && (
          <Tooltip title="导入">
            <Button className="mr-2" icon={<ImportOutlined />}></Button>
          </Tooltip>
        )}
        {isShowButton('export') && (
          <Tooltip title="导出">
            <Button className="mr-2" icon={<ExportOutlined />}></Button>
          </Tooltip>
        )}
        {isShowButton('filter') && (
          <Tooltip title="过滤器">
            <Button className="mr-2" icon={<FilterOutlined />}></Button>
          </Tooltip>
        )}
        {isShowButton('reset') && (
          <Tooltip title="刷新">
            <Button icon={<RetweetOutlined />} onClick={onReset}></Button>
          </Tooltip>
        )}
      </div>
    </div>
  )
})

export default TwinkleActionBar
