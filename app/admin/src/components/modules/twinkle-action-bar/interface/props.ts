import { ReactElement } from 'react'
import { ActionButtonType } from '../components/twinkle-buttons'

export type ButtonCode = 'add' | 'delete' | 'download' | 'export' | 'import' | 'filter' | 'reset'

/**
 * 操作栏参数
 */
export interface ActionBarProps {
  /**
   * 视图对象名称
   */
  voName?: string
  /**
   * 插槽
   */
  children?: ReactElement<any, any>
  /**
   * 生成式按钮集合
   */
  buttons?: Array<ActionButtonType>
  /**
   * 排除的按钮
   */
  exclude?: Array<ButtonCode>
  /**
   * 添加点击事件
   */
  onAddClick?: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
}
