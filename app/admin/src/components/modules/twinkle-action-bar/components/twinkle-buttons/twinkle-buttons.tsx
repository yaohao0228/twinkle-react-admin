import { Button } from 'antd'
import { memo } from 'react'

import { useAction } from './hooks/action.hook.ts'
import { ActionButtonsProps } from './interface/props'
import { TwinkleAntdIcon } from '@/components/index.ts'

const TwinkleButtons: React.FC<ActionButtonsProps> = memo((props) => {
  const { handlerDisabled, onClick } = useAction()

  return (
    <>
      {props.buttons?.map((option, index) => {
        const { content, icon, antdProps } = option

        return (
          <Button
            key={index}
            type={antdProps?.type || 'primary'}
            variant={antdProps?.variant || 'filled'}
            className={antdProps?.className || 'mr-2'}
            disabled={handlerDisabled(option)}
            icon={icon ? <TwinkleAntdIcon icon={icon} /> : undefined}
            {...antdProps}
            onClick={() => onClick(option)}
          >
            <span>{content}</span>
          </Button>
        )
      })}
    </>
  )
})

export default TwinkleButtons
