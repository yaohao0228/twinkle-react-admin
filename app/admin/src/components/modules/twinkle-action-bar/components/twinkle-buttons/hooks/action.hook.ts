import { App } from 'antd'
import { ActionButtonType } from '../interface/props'
import { http, RequestConfig } from 'twinkle-request'
import { useEffect, useState } from 'react'
import { eventBus, replaceTemplateStrings } from 'twinkle-utils'
import { EventEnum } from '@/types'

export const useAction = () => {
  const { modal, message } = App.useApp()
  // 表格选中数据
  const [tableSelectList, setTableSelectList] = useState([])
  // 表格选中数据key
  const [tableSelectKeys, setTableSelectKeys] = useState([])

  useEffect(() => {
    eventBus.subscribe(EventEnum.TABLE_SELECT_CHANGE, ([selectKeys, selectColumns]) => {
      setTableSelectList(selectColumns)
      setTableSelectKeys(selectKeys)
    })
  }, [])

  /**
   * 处理禁用状态
   * @param {ActionButtonType} option 按钮配置
   */
  const handlerDisabled = (option: ActionButtonType): boolean => {
    try {
      const selectLength = tableSelectList.length
      const minSelect = option.disabledConfig?.minSelect || 0
      const maxSelect = option.disabledConfig?.maxSelect || 0
      const rules = option.disabledConfig?.rules || []
      // 处理选中范围
      if (selectLength < minSelect || (maxSelect !== 0 && selectLength > maxSelect)) return true
      // 处理规则
      rules.length > 0 &&
        rules.forEach((rule) => {
          tableSelectList.forEach((select) => {
            const value = select[rule.field]
            const rule_value = rule.value
            if (rule.operator === 'eq' && rule_value === value) throw new Error()
            if (rule.operator === 'not' && rule_value !== value) throw new Error()
            if (rule.operator === 'mte' && rule_value >= value) throw new Error()
            if (rule.operator === 'lte' && rule_value <= value) throw new Error()
          })
        })
      return false
    } catch (error) {
      return true
    }
  }

  /**
   * 处理请求
   * @param {ActionButtonType['request']} params 操作按钮参数
   */
  const handlerRequest = async (params: ActionButtonType['request']) => {
    try {
      if (!params) return

      const requestConfig: RequestConfig = {
        url: import.meta.env.VITE_HTTP_URL,
        method: params.method,
        data: { ids: tableSelectKeys }
      }

      // 解析路径参数
      if (params.url) {
        requestConfig.url = requestConfig.url + replaceTemplateStrings(params.url, tableSelectList[0])
      }

      // 判断是否携带请求体
      if (params.body) {
        if (Array.isArray(params.body) && params.body.length > 0) {
          const data = tableSelectList.map((row) => {
            const result: Record<string, any> = {}
            params.body.forEach((key: string) => (result[key] = row[key]))
            return result
          })
          requestConfig.data = { ids: tableSelectKeys, data }
        } else {
          requestConfig.data = { ids: tableSelectKeys, ...params.body }
        }
      }

      const result = await http.request(requestConfig, params.options)
      if (result.code === 200) {
        message.success(params.options.successMessageText || '请求成功')
        eventBus.publish(EventEnum.TABLE_GRID_RESET)
      } else {
        message.error(params.options.errorMessageText || '请求失败')
      }
    } catch (error: any) {
      message.error(error.message || '请求失败')
    }
  }

  /**
   * 按钮点击事件
   * @param {ActionButtonType} option 按钮配置
   */
  const onClick = async (option: ActionButtonType) => {
    eventBus.publish(EventEnum.ACTION_BAR_BUTTON_CLICK, option.code)
    const { request } = option
    if (request?.isShowModal) {
      await modal.confirm({
        title: request.modalTitle || '提示',
        content: request.modalMessage || '你确定要这样做吗？',
        onOk: () => handlerRequest(request)
      })
    }
  }

  return { handlerDisabled, onClick }
}
