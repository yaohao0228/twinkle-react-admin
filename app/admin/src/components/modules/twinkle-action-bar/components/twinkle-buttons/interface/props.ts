import { Operator, RequestOptions } from 'twinkle-request'
import { ButtonProps } from 'antd/es/button'

interface ActionButtonsRequest {
  /**
   * 请求路径
   */
  url: string
  /**
   * 请求方法
   */
  method: 'GET' | 'POST' | 'PUT' | 'DELETE'
  /**
   * 请求体
   */
  body: Array<string> | Record<string, any>
  /**
   * 是否展示对话框
   */
  isShowModal: boolean
  /**
   * 对话框标题
   */
  modalTitle: string
  /**
   * 对话框消息
   */
  modalMessage: string
  /**
   * 请求配置
   */
  options: RequestOptions
}

interface ActionButtonsDisabledConfig {
  /**
   * 最多选中
   */
  maxSelect: number
  /**
   * 最少选中
   */
  minSelect: number
  /**
   * 规则
   */
  rules: { field: string; operator: Operator; value: any; andOr: 'and' | 'or' }[]
}

/**
 * 操作按钮
 */
export interface ActionButtonType {
  /**
   * 按钮权限点
   */
  code: string
  /**
   * 按钮内容
   */
  content?: string
  /**
   * 按钮图标
   */
  icon?: string
  /**
   * 禁用规则
   */
  disabledConfig?: ActionButtonsDisabledConfig
  /**
   * 按钮请求
   */
  request?: ActionButtonsRequest
  /**
   * antd参数
   */
  antdProps?: ButtonProps
  /**
   * 点击事件
   */
  onClick?: () => void
}

export interface ActionButtonsProps {
  /**
   * 按钮集合
   */
  buttons?: Array<ActionButtonType>
}
