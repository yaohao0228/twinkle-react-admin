import React, { memo, useRef } from 'react'

import { TwinkleFormViewProps } from './interface/props'
import { useAction } from './hooks/action.hook'

import { TwinkleFormModal } from './components/twinkle-form-modal'
import { TwinkleForm, type TwinkleFormContainer } from './components/twinkle-form'

const TwinkleFormView: React.FC<TwinkleFormViewProps> = memo((props) => {
  const { voName, routeMeta } = props

  const formRef = useRef<TwinkleFormContainer>(null)

  const { formShow, onCancel, onConfirm } = useAction({
    voName,
    formRef
  })

  return (
    <TwinkleFormModal width={1200} title={`${routeMeta.name}`} open={formShow} onOk={onConfirm} onCancel={onCancel}>
      <TwinkleForm ref={formRef} voName={voName} />

      {/* {childrenColumns.length > 0 && (
          <TwinkleChildrenForm
            ref={childrenRef}
            parentVoName={voName}
            current={current}
            config={childrenColumns}
            formType={formType}
          />
        )} */}
    </TwinkleFormModal>
  )
})

export default TwinkleFormView
