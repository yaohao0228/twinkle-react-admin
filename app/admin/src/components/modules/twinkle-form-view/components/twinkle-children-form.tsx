import { Button, Tabs } from 'antd'
import { useChildrenFormAction } from '../hooks/children-form-action.hook'

import { PlusOutlined } from '@ant-design/icons'
import { ViewObjectColumn } from 'twinkle-datasource'
import { EditType } from '@/types'
import { forwardRef, useImperativeHandle, useRef } from 'react'
import { TableGridContainer } from '../../twinkle-table-grid/interface/container'
import { Current } from '../hooks/setup.hook'
import { QueryCondition } from 'twinkle-request'
import { TwinkleTableGrid } from '../../twinkle-table-grid'
import { isString } from 'lodash'
import { FormType } from '../interface/type'
import { valueStrategy } from './twinkle-form/config/value-strategy'

export interface ChildrenFormProps {
  current: Current | null
  config: ViewObjectColumn[]
  formType: FormType
  parentVoName: string
}

export interface ChildrenFormContainer {
  getChildrenValues: () => Record<string, any>
  resetChildrenValues: () => void
  gridRef: React.RefObject<TableGridContainer>
}

export class ChildrenValuesDto {
  public create: Record<string, any>[] = []
  public update: Record<string, any>[] = []
  public delete: string[] = []

  constructor(dto?: ChildrenValuesDto) {
    this.create = dto?.create || []
    this.update = dto?.update || []
    this.delete = dto?.delete || []
  }
}

const TwinkleChildrenForm = forwardRef<ChildrenFormContainer, ChildrenFormProps>((props, ref) => {
  const gridRef = useRef<TableGridContainer>(null)

  const { activeKey, onTabsChange } = useChildrenFormAction()

  useImperativeHandle(ref, () => ({ gridRef, getChildrenValues, resetChildrenValues }))

  let createNum: number = 1

  const onAddData = () => {
    const columns = gridRef.current?.columns
    const form: Record<string, any> = { id: `create_${createNum++}` }
    columns!.forEach((colum) => {
      form[colum.code] = valueStrategy[colum.editType as EditType]
    })
    gridRef.current?.setData((data) => [...data, form])
  }

  const getChildrenValues = () => {
    const childrenValuesDto = new ChildrenValuesDto()
    if (props.formType === 'add' && gridRef.current) {
      childrenValuesDto.create = gridRef.current.data
    }
    if (props.formType === 'edit' && gridRef.current) {
      const data = gridRef.current?.data
      const updateDataIds = gridRef.current.updateDataIds
      const deleteDataIds = gridRef.current.deleteDataIds
      childrenValuesDto.update = data.filter((item) => updateDataIds.includes(item.id))
      childrenValuesDto.create = data.filter((item) => isString(item.id) && (item.id as string).includes('create'))
      childrenValuesDto.delete = deleteDataIds
    }
    return gridRef.current ? { [gridRef.current?.voName]: childrenValuesDto } : {}
  }

  const resetChildrenValues = () => {
    gridRef.current?.reset()
  }

  const operations = (
    <div>
      {props.formType !== 'view' && <Button type="primary" icon={<PlusOutlined />} onClick={onAddData}></Button>}
    </div>
  )

  const renderTabItems = () => {
    return props.config.map((item) => {
      const voName = item.editOption.options?.voName
      const formType = props.formType
      const queryConditions = new QueryCondition(props.parentVoName, 'eq', props.current?.id)

      return {
        key: item.code,
        label: item.name,
        children: (
          <TwinkleTableGrid
            ref={gridRef}
            isShowAction={formType !== 'view'}
            voName={voName}
            gridType={formType}
            queryConditions={[queryConditions]}
          />
        )
      }
    })
  }

  return (
    <Tabs accessKey={activeKey} items={renderTabItems()} tabBarExtraContent={operations} onChange={onTabsChange}></Tabs>
  )
})

export default TwinkleChildrenForm
