import TwinkleFormModal from './twinkle-form-modal'
import { TwinkleFormModalProps } from './interface/props'

export { TwinkleFormModal, type TwinkleFormModalProps }
