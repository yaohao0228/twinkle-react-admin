import { ModalProps } from 'antd'

type BaseFormModal = React.PropsWithChildren & ModalProps

export interface TwinkleFormModalProps extends BaseFormModal {}
