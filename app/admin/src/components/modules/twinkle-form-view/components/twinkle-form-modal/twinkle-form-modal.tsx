import { Modal } from 'antd'
import { TwinkleFormModalProps } from './interface/props'

const TwinkleFormModal: React.FC<TwinkleFormModalProps> = (props) => {
  const { children, ...modalProps } = props

  return (
    <Modal centered {...modalProps}>
      <section className="max-h-[80vh] overflow-y-scroll hidden-scrollbar">{children}</section>
    </Modal>
  )
}

export default TwinkleFormModal
