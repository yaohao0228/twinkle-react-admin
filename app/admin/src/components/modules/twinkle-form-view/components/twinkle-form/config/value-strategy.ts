import { EditType } from '@/types'

/**
 * 表单默认值
 */
export const valueStrategy: Partial<Record<EditType, any>> = {
  [EditType.STRING]: '',
  [EditType.NUMBER]: null,
  [EditType.BOOLEAN]: true,
  [EditType.ENUM]: '',
  [EditType.SELECT]: '',
  [EditType.TEXTAREA]: '',
  [EditType.CHILDREN]: []
}
