import { TwinkleFormActionHookProps } from '../interface/props'
import { FormType } from '../interface/type'

export const useAction = (props: TwinkleFormActionHookProps) => {
  const { formType, formInstance, targetId } = props

  const getFormValues = async () => {
    const formValues = await formInstance.validateFields()
    if (formType === FormType.EDIT) {
      return { ...formValues, id: targetId }
    }

    return { ...formValues }
  }

  return { getFormValues }
}
