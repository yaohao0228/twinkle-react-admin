import { useState, forwardRef, useImperativeHandle } from 'react'

import { Form, Row } from 'antd'

import { useSetup } from './hooks/setup.hook'
import { renderStrategy } from './config/render-strategy'
import { TwinkleFormContainer, TwinkleFormProps } from './interface/props'
import { FormType } from './interface/type'
import { useAction } from './hooks/action.hook'

const TwinkleForm = forwardRef<TwinkleFormContainer, TwinkleFormProps>((props, ref) => {
  const { voName = '', initialValues = {}, columns = [], ...formProps } = props

  const [formInstance] = Form.useForm()
  const [formType, setFormType] = useState<FormType>(FormType.ADD)
  const [targetId, setTargetId] = useState<string | null>(null)

  const { handlerColumns, handlerInitialValues, reset } = useSetup({
    voName,
    columns,
    targetId,
    formType,
    initialValues,
    formInstance,
    setTargetId
  })

  const { getFormValues } = useAction({
    formType,
    targetId,
    formInstance
  })

  useImperativeHandle(ref, () => ({ formInstance, formType, reset, setFormType, setTargetId, getFormValues }))

  return (
    <Form
      form={formInstance}
      initialValues={handlerInitialValues}
      labelCol={{ span: 6 }}
      wrapperCol={{ span: 18 }}
      {...formProps}
    >
      <Row>{handlerColumns.map((column) => renderStrategy(column, formType, formInstance.getFieldsValue()))}</Row>
    </Form>
  )
})

export default TwinkleForm
