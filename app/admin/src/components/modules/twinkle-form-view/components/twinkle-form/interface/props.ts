import { FormInstance, FormProps } from 'antd'
import { ViewObjectColumn } from 'twinkle-datasource'
import { FormType } from './type'

export interface TwinkleFormProps extends Partial<FormProps> {
  /**
   * 视图对象名称
   */
  voName?: string
  /**
   * 初始化表单数据
   */
  initialValues?: Record<string, any>
  /**
   * 表单字段列配置
   */
  columns?: ViewObjectColumn[]
}

export interface TwinkleFormContainer {
  formType: FormType
  formInstance: FormInstance<any>
  reset: () => void
  setFormType: React.Dispatch<React.SetStateAction<FormType>>
  setTargetId: React.Dispatch<React.SetStateAction<string | null>>
  getFormValues: () => Record<string, any> & { id?: string }
}

type SetupHookBase = Pick<TwinkleFormProps, 'voName' | 'columns' | 'initialValues'>

export interface TwinkleFormSetupHookProps extends SetupHookBase {
  targetId: string | null
  formType: FormType
  formInstance: FormInstance<any>
  setTargetId: React.Dispatch<React.SetStateAction<string | null>>
}

type ActionHookBase = {}

export interface TwinkleFormActionHookProps extends ActionHookBase {
  targetId: string | null
  formType: FormType
  formInstance: FormInstance<any>
}
