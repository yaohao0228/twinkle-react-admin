export enum FormType {
  ADD = 'add',
  EDIT = 'edit',
  VIEW = 'view'
}
