import { useEffect, useState } from 'react'
import { cloneDeep, sortBy } from 'lodash'
import { DataSourceType, ViewObjectColumn } from 'twinkle-datasource'
import { isEmpty, isNotObject } from 'twinkle-utils'

import { datasource } from '@/utils/datasource'
import { EditType } from '@/types'

import { TwinkleFormSetupHookProps } from '../interface/props'
import { FormType } from '../interface/type'
import { valueStrategy } from '../config/value-strategy'

export const useSetup = (props: TwinkleFormSetupHookProps) => {
  const { voName, columns, targetId, formType, initialValues, formInstance, setTargetId } = props

  // 表单列配置
  const [handlerColumns, setHandlerColumns] = useState<ViewObjectColumn[]>([])
  // 表单初始值
  const [handlerInitialValues, setHandlerInitialValues] = useState<Record<string, any>>({})

  /**
   * 初始化数据源
   */
  const setupDataSource = async () => {
    if (voName) {
      await datasource.register(new DataSourceType({ key: voName }))
      const dataConfig = await datasource.getDataSource(voName)
      const { handlerAfterColumns } = handlerFormColumns(dataConfig.viewObject?.viewObjectColumn || [])
      setHandlerColumns(handlerAfterColumns)
      return
    }
    if (columns) {
      const { handlerAfterColumns } = handlerFormColumns(columns)
      setHandlerColumns(handlerAfterColumns)
      return
    }
    throw new Error('请传入voName或者columns')
  }

  /**
   * 初始化表单默认值
   */
  const setupInitialValues = async () => {
    if (initialValues && !isNotObject(initialValues)) {
      setHandlerInitialValues(initialValues)
      return
    }
    if (formType === FormType.ADD && columns) {
      const form: Record<string, any> = {}
      columns.forEach((colum) => {
        form[colum.code] = valueStrategy[colum.editType as EditType]
      })
      setHandlerInitialValues(form)
      return
    }
    if ((formType === FormType.EDIT || formType === FormType.VIEW) && targetId && voName && columns) {
      const data = await datasource.getInfo(voName, targetId)
      const form: Record<string, any> = {}
      columns.forEach((colum) => {
        form[colum.code] = data[colum.code]
      })
      setHandlerInitialValues(form)
      formInstance.setFieldsValue(data)
      return
    }
  }

  /**
   * 重置数据
   */
  const reset = () => {
    setHandlerInitialValues({})
    setTargetId(null)
    formInstance.resetFields()
  }

  /**
   * 处理表单字段数据
   * @param {ViewObjectColumn[]} columns 列字段配置集合
   */
  const handlerFormColumns = (columns: ViewObjectColumn[]) => {
    const copyColumns = cloneDeep(columns)
    let handlerAfterColumns: ViewObjectColumn[] = []
    const childrenColumns: ViewObjectColumn[] = []
    copyColumns.forEach((item) => {
      if (item.editType === EditType.CHILDREN && item.isShow) {
        childrenColumns.push(item)
      } else if (item.isShow) {
        handlerAfterColumns.push(item)
      }
    })

    // 对字段进行排序
    handlerAfterColumns = sortBy(handlerAfterColumns, (item) => item.rank)

    return { handlerAfterColumns }
  }

  useEffect(() => {
    if (isEmpty(voName)) return
    setupDataSource()
  }, [voName])

  useEffect(() => {
    if (isEmpty(targetId) || isEmpty(formType)) return
    setupInitialValues()
  }, [formType, targetId])

  return { handlerColumns, handlerInitialValues, reset }
}
