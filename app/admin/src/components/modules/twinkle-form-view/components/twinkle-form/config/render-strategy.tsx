import { EditType } from '@/types'
import { Form, FormItemProps, Input, InputNumber, Switch, Col } from 'antd'
import React from 'react'
import { ViewObjectColumn } from 'twinkle-datasource'
import { FormType } from '../interface/type'

interface Render {
  render: () => React.ReactNode
  props: FormItemProps
}

/**
 * 表单渲染策略
 * @param {ViewObjectColumn} column 列字段配置
 */
export const renderStrategy = (
  column: ViewObjectColumn,
  formType: FormType,
  initialValues: Record<string, any>
): React.ReactNode => {
  const strategy: Partial<Record<EditType, Render>> = {
    [EditType.STRING]: { render: () => <Input placeholder={`请输入${column.name}`} />, props: {} },
    [EditType.NUMBER]: {
      render: () => <InputNumber className="w-full" placeholder={`请输入${column.name}`} />,
      props: {}
    },
    [EditType.BOOLEAN]: {
      render: () => (
        <Switch
          checkedChildren={column.editOption.options?.['true']}
          unCheckedChildren={column.editOption.options?.['false']}
        />
      ),
      props: { valuePropName: 'checked' }
    },
    [EditType.ENUM]: { render: () => <Input placeholder={`请选择${column.name}`} />, props: {} },
    [EditType.SELECT]: { render: () => <Input placeholder={`请选择${column.name}`} />, props: {} },
    [EditType.TEXTAREA]: { render: () => <Input placeholder={`请输入${column.name}`} />, props: {} }
  }

  return (
    <Col span={12}>
      <Form.Item
        label={column.name}
        name={column.code}
        key={column.code}
        required={column.isRequired}
        {...strategy[column.editType as EditType]?.props}
      >
        {formType === FormType.VIEW ? (
          <div>{initialValues[column.code]}</div>
        ) : (
          strategy[column.editType as EditType]?.render()
        )}
      </Form.Item>
    </Col>
  )
}
