import TwinkleFormView from './twinkle-form-view'
import TwinkleChildrenForm from './components/twinkle-children-form'
import { renderStrategy } from './components/twinkle-form/config/render-strategy'

export type * from './interface/props'
export { TwinkleFormView, TwinkleChildrenForm }
export { renderStrategy as FormViewRenderStrategy }
