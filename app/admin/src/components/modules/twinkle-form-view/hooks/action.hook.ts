import { useEffect, useMemo, useState } from 'react'
import { eventBus } from 'twinkle-utils'
import { EventEnum } from '@/types'
import { datasource } from '@/utils/datasource'
import { FormType } from '../interface/type'
import { TwinkleFormViewActionProps } from '../interface/props'

export const useAction = (props: TwinkleFormViewActionProps) => {
  const { voName, formRef } = props
  const [formShow, setFormShow] = useState<boolean>(false)

  /**
   * 表单类型对应值
   */
  const formTypeValue = useMemo(() => {
    const strategy: Record<FormType, string> = {
      add: '新增',
      edit: '编辑',
      view: '查看'
    }
    return formRef.current && strategy[formRef.current.formType]
  }, [formRef])

  /**
   * 表单取消事件
   */
  const onCancel = () => {
    setFormShow(false)
    setTimeout(() => {
      formRef.current?.reset()
    })
  }

  /**
   * 表单确定事件
   */
  const onConfirm = async () => {
    const formValues = formRef.current?.getFormValues()
    await datasource.save(voName, formValues)
    eventBus.publish(EventEnum.TABLE_GRID_RESET)
    onCancel()
  }

  useEffect(() => {
    const { unsubscribe } = eventBus.subscribe(EventEnum.FORM_VIEW_SHOW, ([state, type, row]) => {
      setFormShow(state)
      setTimeout(() => {
        type && formRef.current?.setFormType(type)
        row && formRef.current?.setTargetId(row.id)
      })
    })

    return () => {
      unsubscribe()
    }
  }, [])

  return { formShow, formTypeValue, onCancel, onConfirm }
}
