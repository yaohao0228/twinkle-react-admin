import { useState } from 'react'

export const useChildrenFormAction = () => {
  const [activeKey, setActiveKey] = useState<string>('')

  const onTabsChange = (activeKey: string) => {
    setActiveKey(activeKey)
  }
  return { activeKey, onTabsChange }
}
