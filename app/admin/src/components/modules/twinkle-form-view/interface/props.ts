import { RouteMeta } from '@/types'
import { TwinkleFormContainer } from '../components/twinkle-form'
export interface TwinkleFormViewProps {
  /**
   * 视图对象名称
   */
  voName: string
  /**
   * 路由元数据
   */
  routeMeta: RouteMeta
}

type ActionHookBase = Pick<TwinkleFormViewProps, 'voName'>

export interface TwinkleFormViewActionProps extends ActionHookBase {
  formRef: React.RefObject<TwinkleFormContainer>
}
