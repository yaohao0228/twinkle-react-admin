import TwinkleDictBox from './twinkle-dict-box'

export type * from './interface/props'
export { TwinkleDictBox }
