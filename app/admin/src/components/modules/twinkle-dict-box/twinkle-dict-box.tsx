import React, { memo } from 'react'
import { DictBoxProps } from './interface/props'

const TwinkleDictBox: React.FC<DictBoxProps> = memo(() => {
  return <div>TwinkleDictBox</div>
})

export default TwinkleDictBox
