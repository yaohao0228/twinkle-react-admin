import { useEnvStore } from '@/store'

export const useSetup = () => {
  /**
   * 初始化环境变量
   */
  const setupEnv = () => {
    const envConfig: Partial<ImportMetaEnv> = {
      VITE_PROJECT_NAME: import.meta.env.VITE_PROJECT_NAME,
      VITE_HTTP_URL: import.meta.env.VITE_HTTP_URL
    }
    const setEnv = useEnvStore.getState().setEnv
    setEnv(envConfig)
  }

  return { setupEnv }
}
