import { useRoute } from '@/router/hooks/route.hook'

export const useSetup = () => {
  const { searchRoute } = useRoute()
  const routeMeta = searchRoute().meta || {}
  const editOption = searchRoute().meta?.editOption
  const voName = editOption?.voName || routeMeta.code || ''

  return { voName, editOption, routeMeta }
}
