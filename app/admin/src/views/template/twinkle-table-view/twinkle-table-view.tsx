import React from 'react'
import { useSetup } from '../hooks/setup.hook'
import {
  TwinkleActionBar,
  TwinkleTableGrid,
  tableGridRouteMetaPropsPipe,
  actionBarRouteMetaPropsPipe
} from '@/components'

const TwinkleTableView: React.FC = () => {
  const { routeMeta } = useSetup()

  return (
    <div>
      <TwinkleActionBar {...actionBarRouteMetaPropsPipe(routeMeta)} />
      <TwinkleTableGrid {...tableGridRouteMetaPropsPipe(routeMeta)} />
      {/* <TwinkleFormView voName={voName} routeMeta={routeMeta} /> */}
    </div>
  )
}

export default TwinkleTableView
