import { ResultEnum } from 'twinkle-request'
import { authApi } from '@/apis'
import { LoginParams, LoginResponse, StorageCacheKey } from '@/types'
import { storage } from 'twinkle-storage'

export const useLogin = () => {
  /**
   * 登录
   */
  const login = async (loginParams: LoginParams) => {
    const result = await authApi.login<LoginResponse>(loginParams)
    if (result.code === ResultEnum.SUCCESS && result.data) {
      storage.setItem(StorageCacheKey.TOKEN, result.data.access_token)
      storage.setItem(StorageCacheKey.USER_INFO, result.data.user)
      await findMenu()
      return result.data
    } else {
      return undefined
    }
  }

  /**
   * 获取菜单
   */
  const findMenu = async () => {
    const result = await authApi.findPermission()
    if (result.code === ResultEnum.SUCCESS) {
      storage.setItem(StorageCacheKey.MENU, result.data)
    }
  }

  return { login, findMenu }
}
