import { useEnvStore } from '@/store'
import LoginForm from './components/LoginForm'

const DefaultLogin: React.FC = () => {
  const projectName = useEnvStore((state) => state.VITE_PROJECT_NAME)

  return (
    <div className="relative flex items-center justify-center min-w-[550px] h-full min-h-[500px] bg-[url('/images/login_bg.svg')] bg-center bg-cover">
      <div className=" box-border flex items-center justify-around w-[96%] h-[94%] pr-[4%] pl-[20px] overflow-hidden rounded-xl">
        <div className="w-[750px] hidden lg:block">
          <img className="w-full h-full" src="/images/login_left.png" alt="login" />
        </div>
        <div className="pt-[40px] pr-[0px] lg:pr-[45px] pb-[25px] rounded-xl">
          <div className="flex items-center justify-center mb-[40px]">
            <img className="w-[70px]" src="/logo_light.svg" alt="logo" />
            <span className="pl-[25px] text-5xl font-bold whitespace-nowrap">{projectName}</span>
          </div>
          <LoginForm />
        </div>
      </div>
    </div>
  )
}

export default DefaultLogin
