import { REDIRECT_PATH } from '@/router/constant'
import { CloseCircleOutlined, LockOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Flex, Form, Input, App } from 'antd'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useLogin } from '../../../hooks/use-login'
import { isUndefined } from 'twinkle-utils'

const LoginForm: React.FC = () => {
  const [form] = Form.useForm()
  const { message } = App.useApp()
  const [loading, setLoading] = useState<boolean>(false)
  const navigate = useNavigate()
  const { login } = useLogin()

  const onFinish = async () => {
    try {
      setLoading(true)
      const result = await login({ ...form.getFieldsValue(), login_type: 'password' })
      if (!isUndefined(result)) {
        setLoading(false)
        message.success('登录成功')
        navigate(REDIRECT_PATH)
      }
    } catch (error: any) {
      setLoading(false)
      message.error(error.message)
    }
  }

  const onFinishFailed = (error: any) => {
    console.log(error)
  }

  return (
    <Form
      form={form}
      name="basic"
      labelCol={{ span: 5 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      size="large"
      autoComplete="off"
    >
      <Form.Item name="username" rules={[{ required: true, message: '请输入用户名' }]}>
        <Input placeholder="请输入用户名" prefix={<UserOutlined />} />
      </Form.Item>
      <Form.Item name="password" rules={[{ required: true, message: '请输入密码' }]}>
        <Input.Password autoComplete="new-password" placeholder="请输入密码" prefix={<LockOutlined />} />
      </Form.Item>
      <Form.Item className="w-full mt-3">
        <Flex justify="space-between">
          <Button
            className="flex-1 mr-1"
            onClick={() => {
              form.resetFields()
            }}
            icon={<CloseCircleOutlined />}
          >
            重置
          </Button>
          <Button className="flex-1 ml-1" type="primary" htmlType="submit" loading={loading} icon={<UserOutlined />}>
            确认
          </Button>
        </Flex>
      </Form.Item>
    </Form>
  )
}

export default LoginForm
