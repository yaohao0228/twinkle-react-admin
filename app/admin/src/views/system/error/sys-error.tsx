import { Button } from 'antd'

const SysError: React.FC = () => {
  return (
    <div className="h-full w-full flex flex-col items-center content-center">
      <img className="w-[650px] h-[400px] mt-10" src="/images/404.png" />
      <div className="py-10">实在抱歉！页面一不小心跑丢了...</div>
      <Button type="primary">回到首页</Button>
    </div>
  )
}

export default SysError
