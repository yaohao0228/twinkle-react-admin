import { merge, concat } from 'lodash-es'
import { isArray, isEmpty, isFunction, isNotEmpty, getObjectHashCode } from 'twinkle-utils'
import {
  QueryParameter,
  QueryCondition,
  PageParameter,
  ResultEnum,
  BaseApi,
  http,
  PageResult,
  Result
} from 'twinkle-request'
import { DataCacheType, DataSourceSetting, SourceType, transformStrategy } from './interface'
import { DataSourceType } from './interface/DataSourceType'
import { ViewObject } from './interface/ViewObjectType'

/**
 * 全局数据源缓存
 */
const $dataSource: Record<number, SourceType> = {}

/**
 * 数据源
 */
export class DataSource {
  /**
   * 数据源设置
   */
  private readonly setting: DataSourceSetting
  /**
   * 数据源存储对象
   */
  private readonly dataSources: Record<string, DataSourceType> = {}
  /**
   * 数据源缓存
   */
  private readonly dataCache: Record<string, DataCacheType> = {}

  static defaultSetting: Partial<DataSourceSetting> = {
    expire: 0,
    onInit: null,
    onRegister: null
  }

  constructor(setting: DataSourceSetting) {
    this.setting = setting
    this.dataSources = {}
    this.dataCache = {}

    isFunction(this.setting.onInit) && this.setting.onInit()
  }

  /**
   * 注册数据源
   * @param {DataSourceType | DataSourceType[]} dataSources 数据源信息
   */
  public async register(dataSources: DataSourceType | DataSourceType[]): Promise<any> {
    if (!isArray(dataSources)) {
      dataSources = [dataSources]
    }

    for (const index in dataSources) {
      const { key, transform } = dataSources[index]
      let { service, options, config, expire, queryParameter } = dataSources[index]

      const viewObject = await this.getViewObject(key)
      service = service ?? (viewObject.data?.list[0].service || '')

      config = merge({}, this.setting.config || {}, config || {})
      options = merge({}, this.setting.options || {}, options || {})
      queryParameter = queryParameter || new QueryParameter()

      const source: SourceType = { key, service, config, options, queryParameter }
      const hash = getObjectHashCode(source)

      const handler = new BaseApi(service, key)

      if (isEmpty($dataSource[hash])) {
        $dataSource[hash] = merge({}, source, { callbacks: [] })
      }

      expire = expire || this.setting.expire

      this.dataSources[key] = merge({}, source, {
        hash,
        handler,
        expire,
        queryParameter,
        transform,
        viewObject: viewObject.data?.list[0]
      })

      isFunction(this.setting.onRegister) && this.setting.onRegister(key)
    }
  }

  /**
   * 获取数据源
   * @param {string} id 数据源key
   * @param {boolean} force 强制刷新
   */
  public async get<T extends Result<T> = any>(id: string, force: boolean = false): Promise<any> {
    const dataSource = await this.getDataSource(id)

    const request = async () => {
      isFunction(this.setting.onRequest) && this.setting.onRequest(id)
      const result = await dataSource.handler?.find<T>(dataSource.queryParameter)
      if (result?.code === ResultEnum.SUCCESS) {
        const expire = 1000 * dataSource.expire! + Date.now()
        this.dataCache[id] = { expire, data: result }
        return result
      }
    }

    if (force) {
      const result = await request()
      return result?.data
    }

    const cache = this.dataCache[id]
    if (isNotEmpty(cache) && cache.expire > Date.now()) {
      return cache.data.data
    }

    const result = await request()
    return result?.data
  }

  /**
   * 获取数据详情
   * @param {string} id 数据源key
   * @param {string} targetId 目标id
   */
  public async getInfo<T extends Result<T> = any>(id: string, targetId: string): Promise<any> {
    const dataSource = await this.getDataSource(id)

    const request = async () => {
      isFunction(this.setting.onRequest) && this.setting.onRequest(id)
      const result = await dataSource.handler?.findById<T>(targetId)
      return result
    }

    const result = await request()
    return result?.data
  }

  /**
   * 获取本地数据源
   * @param {string} id 数据源id
   */
  public async getDataSource(id: string): Promise<DataSourceType> {
    const dataSource = this.dataSources[id]
    if (isEmpty(dataSource)) throw new Error(`数据源${id}不存在`)
    return dataSource
  }

  /**
   * 保存数据源
   * @param {string} id 数据源id
   * @param {*} data 需要保存的数据
   */
  public async save(id: string, data: any, force: boolean = false): Promise<any> {
    const dataSource = this.dataSources[id]
    if (isEmpty(dataSource)) throw new Error(`数据源${id}不存在`)

    const request = async () => {
      isFunction(this.setting.onRequest) && this.setting.onRequest(id)
      const result = await dataSource.handler?.save(data)
      if (result?.code === ResultEnum.SUCCESS) {
        // 同步数据源
        return await this.get(id, force)
      }
    }

    const result = await request()
    return result
  }

  /**
   * 删除数据源数据
   * @param {string} id 数据源id
   * @param {string[]} ids 需要删除数据源数据id集合
   */
  public async remove(id: string, ids: string[]): Promise<any> {
    const dataSource = this.dataSources[id]
    if (isEmpty(dataSource)) throw new Error(`数据源${id}不存在`)

    const request = async () => {
      isFunction(this.setting.onRequest) && this.setting.onRequest(id)
      const result = await dataSource.handler?.delete(ids)
      if (result?.code === ResultEnum.SUCCESS) {
        // 同步数据源
        return await this.get(id, true)
      }
    }

    const result = await request()
    return result
  }

  /**
   * 修改数据源数据
   * @param {string} id 数据源id
   * @param {*} data 需要修改的数据
   */
  public async update(id: string, data: any): Promise<any> {
    const dataSource = this.dataSources[id]
    if (isEmpty(dataSource)) throw new Error(`数据源${id}不存在`)

    const request = async () => {
      isFunction(this.setting.onRequest) && this.setting.onRequest(id)
      const result = await dataSource.handler?.save(data)
      if (result?.code === ResultEnum.SUCCESS) {
        // 同步数据源
        return await this.get(id, true)
      }
    }

    const result = await request()
    return result
  }

  /**
   * 响应转换
   */
  private async transform<T = any>(transformStrategies: transformStrategy[], data: T): Promise<T> {
    let result = data
    transformStrategies.forEach((strategy) => {
      if (isFunction(strategy)) result = strategy(result)
    })
    return data
  }

  /**
   * 设置数据源查询条件
   * @param {string} id 数据源key
   * @param {QueryCondition[]} queryConditions 查询条件参数
   */
  public async setQueryCondition(id: string, queryConditions: QueryCondition[]) {
    const dataSource = this.dataSources[id]
    if (isEmpty(dataSource)) throw new Error(`数据源${id}不存在`)

    const source = $dataSource[dataSource.hash!]
    const sourceConditions = source.queryParameter.queryConditions

    // 传入空数组 重置为默认查询条件
    if (queryConditions.length === 0 && dataSource.queryParameter) {
      dataSource.queryParameter.queryConditions = sourceConditions
      return
    }

    // 如果source中有数据则表示存在默认查询条件
    if (sourceConditions.length > 0 && dataSource.queryParameter) {
      queryConditions = concat(sourceConditions, new QueryCondition('', '', '', 'and'), queryConditions)
      dataSource.queryParameter.queryConditions = queryConditions
      return
    }

    dataSource.queryParameter && (dataSource.queryParameter.queryConditions = queryConditions)
  }

  /**
   * 设置数据源分页参数
   * @param {string} id 数据源key
   * @param {PageParameter} pageParameter 分页参数
   */
  public async setPageParameter(id: string, pageParameter: PageParameter) {
    const dataSource = this.dataSources[id]
    if (isEmpty(dataSource)) throw new Error(`数据源${id}不存在`)

    dataSource.queryParameter && (dataSource.queryParameter.pageParameter = pageParameter)
  }

  /**
   * 设置数据源排序条件
   * @param {string} id 数据源key
   * @param {Record<string,any>} sort 排序条件
   */
  public async setSort(id: string, sort: Record<string, any>) {
    const dataSource = this.dataSources[id]
    if (isEmpty(dataSource)) throw new Error(`数据源${id}不存在`)

    dataSource.queryParameter && (dataSource.queryParameter.sort = sort)
  }

  /**
   * 获取视图对象
   * @param {string} voName 视图对象名称
   */
  public async getViewObject(voName: string) {
    const url = `${this.setting.modelUrl}/v1/system/viewObject/list`
    const queryParameter = new QueryParameter()
    queryParameter.queryConditions.push(new QueryCondition('code', 'eq', voName))
    return http.post<PageResult<ViewObject>>(url, queryParameter)
  }
}
