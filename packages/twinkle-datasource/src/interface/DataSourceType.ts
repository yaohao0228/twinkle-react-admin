import { BaseApi, QueryParameter, RequestConfig, RequestOptions } from 'twinkle-request'
import { transformStrategy } from '.'
import { ViewObject } from './ViewObjectType'

export class DataSourceType {
  /**
   * 数据源key标识
   */
  key: string
  /**
   * 数据源服务
   */
  service?: string
  /**
   * 数据源请求配置
   */
  config?: RequestConfig
  /**
   * 数据源请求选项
   */
  options?: RequestOptions
  /**
   * 数据源查询参数
   */
  queryParameter?: QueryParameter
  /**
   * 数据源过期时长（单位：秒）
   */
  expire?: number
  /**
   * source对象hash值
   */
  hash?: number
  /**
   * 处理函数
   */
  handler?: BaseApi
  /**
   * 数据源响应处理函数
   */
  transform?: transformStrategy[]
  /**
   * 数据源模型
   */
  viewObject?: ViewObject

  constructor(options: DataSourceType) {
    this.key = options.key
    this.service = options.service
    this.config = options.config
    this.options = options.options
    this.queryParameter = options.queryParameter
    this.expire = options.expire
    this.hash = options.hash
    this.handler = options.handler
    this.transform = options.transform
    this.viewObject = options.viewObject
  }
}
