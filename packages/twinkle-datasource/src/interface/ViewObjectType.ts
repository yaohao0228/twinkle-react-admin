/**
 * 视图对象
 */
export interface ViewObject {
  /**
   * 主键
   */
  id: number
  /**
   * 数据版本
   */
  version: number
  /**
   * 创建时间
   */
  createTime: string
  /**
   * 更新时间
   */
  updateTime: string
  /**
   * 代码
   */
  code: string
  /**
   * 名称
   */
  name: string
  /**
   * 接口服务
   */
  service: string
  /**
   * 列字段
   */
  viewObjectColumn: ViewObjectColumn[]
}

export interface EditOption {
  style?: any
  options?: Record<string, any> & Partial<ChildrenOption>
}

export interface ChildrenOption {
  voName: string
}

/**
 * 视图对象列字段
 */
export interface ViewObjectColumn {
  /**
   * 主键
   */
  id: number
  /**
   * 数据版本
   */
  version: number
  /**
   * 创建时间
   */
  createTime: string
  /**
   * 更新时间
   */
  updateTime: string
  /**
   * 字段名称
   */
  name: string
  /**
   * 字段编码
   */
  code: string
  /**
   * 顺序
   */
  rank: number
  /**
   * 宽度
   */
  width: number
  /**
   * 编辑类型
   */
  editType: string
  /**
   * 是否可搜索
   */
  isSearch: boolean
  /**
   * 是否展示
   */
  isShow: boolean
  /**
   * 是否必填
   */
  isRequired: boolean
  /**
   * 编辑配置
   */
  editOption: EditOption
  /**
   * 备注
   */
  comment: string
  /**
   * 前端渲染策略
   */
  render?: (value: any) => any
}
