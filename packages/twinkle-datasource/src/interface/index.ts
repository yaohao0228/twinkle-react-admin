import { QueryParameter, RequestOptions, RequestConfig } from 'twinkle-request'

export interface DataSourceSetting {
  /**
   * 数据源过期时长（单位：秒）
   */
  expire?: number
  /**
   * 模型请求地址
   */
  modelUrl?: string
  /**
   * 数据源请求配置
   */
  config?: RequestConfig
  /**
   * 数据源请求选项
   */
  options?: RequestOptions
  /**
   * 初始化后的回调函数
   */
  onInit?: (() => void) | null
  /**
   * 注册后的回调函数
   */
  onRegister?: ((id: string) => void) | null
  /**
   * 请求前的回调函数
   */
  onRequest?: ((id: string) => void) | null
}

/**
 * 数据缓存类型
 */
export interface DataCacheType<T = any> {
  /**
   * 数据源过期时间
   */
  expire: number
  /**
   * 数据
   */
  data: T
}

export interface SourceType {
  /**
   * 数据源key标识
   */
  key: string
  /**
   * 数据源服务
   */
  service: string
  /**
   * 数据源请求配置
   */
  config: RequestConfig
  /**
   * 数据源请求选项
   */
  options: RequestOptions
  /**
   * 数据源查询参数
   */
  queryParameter: QueryParameter
}

/**
 * 转换策略
 */
export type transformStrategy = <T = any>(data: T) => T

export * from './DataSourceType'

export * from './ViewObjectType'
