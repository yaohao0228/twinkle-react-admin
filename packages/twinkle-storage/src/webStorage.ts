import { isEmpty, isNotEmpty } from 'twinkle-utils'

export interface StorageCacheItem<T = any> {
  value: T
  time: number
  expire: number
}

/**
 * 本地存储 local
 */
export class WebStorage<StorageCacheKey extends string = any> {
  private readonly prefix: string
  private readonly storage: Storage
  private readonly timeout: number

  constructor(storage: Storage, prefix: string, timeout: number) {
    this.storage = storage
    this.prefix = prefix
    this.timeout = timeout
  }

  /**
   * 获取缓存数据
   */
  public getItem<T = any>(key: StorageCacheKey): T | null {
    const cacheItem = this.storage.getItem(this.getKey(key))
    if (!cacheItem) return null
    try {
      const decryptValue = cacheItem
      const data: StorageCacheItem = JSON.parse(decryptValue)
      const { value, expire } = data
      if (isEmpty(expire) || expire >= new Date().getTime()) {
        return value
      }
      return null
    } catch (error) {
      return null
    }
  }

  /**
   * 设置缓存数据
   */
  public setItem(key: StorageCacheKey, value: any, expire: number | null = this.timeout) {
    const stringData = JSON.stringify({
      value,
      time: Date.now(),
      expire: isNotEmpty(expire) ? new Date().getTime() + expire * 1000 : null
    })
    this.storage.setItem(this.getKey(key), stringData as string)
  }

  /**
   * 删除缓存
   */
  public remove(key: StorageCacheKey): void {
    this.storage.removeItem(this.getKey(key))
  }

  /**
   * 清空缓存
   */
  public clear(): void {
    this.storage.clear()
  }

  /**
   * 获取缓存Key
   */
  private getKey(key: string): string {
    return `${this.prefix}${key}`.toUpperCase()
  }
}
