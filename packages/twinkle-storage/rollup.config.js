import { defineConfig } from 'rollup'
import { babel } from '@rollup/plugin-babel'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import typescript from '@rollup/plugin-typescript'
import commonjs from '@rollup/plugin-commonjs'
import json from '@rollup/plugin-json'
import terser from '@rollup/plugin-terser'

export default defineConfig([
  {
    input: 'src/index.ts',
    output: [
      {
        file: 'dist/index.js',
        format: 'es',
        name: 'twinkleStorage'
      }
    ],
    plugins: [
      typescript({ tsconfig: './tsconfig.json', sourceMap: false, declarationDir: 'dist' }),
      nodeResolve({ preferBuiltins: true, mainFields: ['browser', 'main'] }),
      commonjs(),
      babel({ babelHelpers: 'bundled', exclude: '**/node_modules/**' }),
      json(),
      terser()
    ]
  },
  {
    input: 'src/index.ts',
    output: [
      {
        file: 'dist/index.cjs',
        format: 'cjs',
        exports: 'named'
      }
    ],
    plugins: [
      typescript({ tsconfig: './tsconfig.json', sourceMap: false, declarationDir: 'dist' }),
      nodeResolve({ preferBuiltins: true, mainFields: ['browser', 'main'] }),
      commonjs(),
      babel({ babelHelpers: 'bundled', exclude: 'node_modules/**' }),
      json(),
      terser()
    ]
  }
])
