/**
 * 请求返回值
 */
export enum ResultEnum {
  /**
   * 成功状态码
   */
  SUCCESS = 200,
  /**
   * 失败
   */
  ERROR = -1,
  /**
   * 超时
   */
  TIME_OUT = 10042,
  /**
   * 成功状态
   */
  TYPE = 'success'
}

/**
 * 请求方式
 */
export enum RequestEnum {
  /**
   * GET
   */
  GET = 'GET',
  /**
   * POST
   */
  POST = 'POST',
  /**
   * PATCH
   */
  PATCH = 'PATCH',
  /**
   * PUT
   */
  PUT = 'PUT',
  /**
   * DELETE
   */
  DELETE = 'DELETE'
}

/**
 * content-type
 */
export enum ContentTypeEnum {
  /**
   * JSON
   */
  JSON = 'application/json;charset=UTF-8',
  /**
   * text
   */
  TEXT = 'text/plain;charset=UTF-8',
  /**
   * form-data
   */
  FORM_URLENCODED = 'application/x-www-form-urlencoded;charset=UTF-8',
  /**
   * form-data 上传
   */
  FORM_DATA = 'multipart/form-data;charset=UTF-8'
}
