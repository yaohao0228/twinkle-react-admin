import { AxiosResponse, InternalAxiosRequestConfig } from 'axios'
import { CreateAxiosOptions, RequestOptions, Result } from './type'

export class AxiosTransform {
  /**
   * 请求前的回调
   */
  beforeRequestHook?: (
    config: Partial<InternalAxiosRequestConfig>,
    options: RequestOptions
  ) => Partial<InternalAxiosRequestConfig>

  /**
   * 请求后的回调
   */
  afterRequestHook?: (response: AxiosResponse<Result>, options: RequestOptions) => any

  /**
   * 请求失败的回调
   */
  afterRequestCatchHook?: <T = any>(error: Error) => Promise<T>

  /**
   * 全局请求拦截器
   */
  requestInterceptors?: <T = any>(
    config: InternalAxiosRequestConfig<T>,
    options: CreateAxiosOptions
  ) => InternalAxiosRequestConfig

  /**
   * 全局请求拦截器错误处理
   */
  requestInterceptorsCatch?: (error: Error) => void

  /**
   * 全局响应拦截器
   */
  responseInterceptors?: (response: AxiosResponse<Result>) => AxiosResponse<Result>

  /**
   * 全局响应拦截器错误处理
   */
  responseInterceptorsCatch?: (error: Error) => void
}
