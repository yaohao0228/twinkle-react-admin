/**
 * 操作符
 */
export type Operator = 'eq' | 'like' | 'not' | 'in' | 'lt' | 'lte' | 'mt' | 'mte' | 'isNull'

/**
 * 查询条件类
 */
export class QueryCondition {
  /**
   * 字段
   */
  field: string
  /**
   * 操作符
   */
  operator: Operator | ''
  /**
   * 值
   */
  value: unknown
  /**
   * 连接符
   */
  andOr?: 'and' | 'or'

  constructor(field: string, operator: Operator | '', value: unknown, andOr?: 'and' | 'or') {
    this.field = field
    this.operator = operator
    this.value = value
    this.andOr = andOr
  }
}

/**
 * 分页查询条件类
 */
export class PageParameter {
  /**
   * 每页条数
   */
  pageSize: number
  /**
   * 当前页
   */
  pageIndex: number
  constructor(pageIndex?: number, pageSize?: number) {
    this.pageIndex = pageIndex || 1
    this.pageSize = pageSize || 20
  }
}

/**
 * 查询参数类
 */
export class QueryParameter {
  /**
   * 查询条件
   */
  queryConditions: QueryCondition[]
  /**
   * 分页条件
   */
  pageParameter: PageParameter = new PageParameter()
  /**
   * 排序条件
   */
  sort: Record<string, any> = {}
  constructor(queryConditions?: QueryCondition[], pageParameter?: PageParameter, sort?: Record<string, any>) {
    this.queryConditions = queryConditions || []
    this.pageParameter = pageParameter || new PageParameter()
    this.sort = sort || {}
  }
}
