import { CreateAxiosDefaults, InternalAxiosRequestConfig } from 'axios'
import { AxiosTransform } from './AxiosTransform'

/**
 * 请求选项
 */
export interface RequestOptions {
  /**
   * 请求参数拼接到url
   */
  joinParamsToUrl?: boolean
  /**
   * 是否显示提示信息
   */
  isShowMessage?: boolean
  /**
   * 成功的文本信息
   */
  successMessageText?: string
  /**
   * 是否显示成功信息
   */
  isShowSuccessMessage?: boolean
  /**
   * 是否显示失败信息
   */
  isShowErrorMessage?: boolean
  /**
   * 错误的文本信息
   */
  errorMessageText?: string
  /**
   * 是否加入url
   */
  joinPrefix?: boolean
  /**
   * 接口地址， 不填则使用默认apiUrl
   */
  apiUrl?: string
  /**
   * 请求拼接路径
   */
  urlPrefix?: string
  /**
   * 错误消息提示类型
   */
  errorMessageMode?: 'none' | 'modal'
  /**
   * 是否添加时间戳
   */
  joinTime?: boolean
  /**
   * 是否返回原生响应头
   */
  isReturnNativeResponse?: boolean
  /**
   * 忽略重复请求
   */
  ignoreCancelToken?: boolean
  /**
   * 是否携带token
   */
  withToken?: boolean
  /**
   * 是否需要loading动画
   */
  loading?: boolean
}

/**
 * 返回值类型
 */
export interface Result<T = any> {
  /**
   * 状态码
   */
  code: number
  /**
   * 返回状态
   */
  type?: 'success' | 'error' | 'warning'
  /**
   * 返回信息
   */
  message: string
  /**
   * 数据
   */
  data?: T
}

/**
 * 分页返回值类型
 */
export interface PageResult<T = any> {
  /**
   * 分页数据
   */
  list: Array<T>
  /**
   * 分页记录
   */
  pagination: {
    /**
     * 当前页
     */
    page: number
    /**
     * 每页展示条数
     */
    size: number
    /**
     * 总数据条数
     */
    total: number
  }
}

export interface CreateAxiosOptions extends Partial<CreateAxiosDefaults> {
  transform?: AxiosTransform
  requestOptions?: RequestOptions
  authenticationScheme?: string
}

export interface RequestConfig extends Partial<InternalAxiosRequestConfig> {
  requestOptions?: RequestOptions
}
