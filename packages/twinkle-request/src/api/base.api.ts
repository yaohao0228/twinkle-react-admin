import { http } from '../axios'
import { RequestOptions, Result } from '../types/type'

/**
 * 基础CRUD接口
 */
export class BaseApi {
  public readonly service: string
  public readonly model: string

  /**
   * @param {string} service 服务名称
   * @param {string} model 模型名称
   */
  constructor(service: string, model?: string) {
    this.service = service
    this.model = model || ''
    this.find = this.find.bind(this)
    this.save = this.save.bind(this)
    this.update = this.update.bind(this)
    this.delete = this.delete.bind(this)
    this.findAll = this.findAll.bind(this)
    this.findById = this.findById.bind(this)
  }

  /**
   * 条件查询
   * @param {*} queryParameter 查询参数
   * @param {RequestOptions} options 请求选项
   */
  public async find<T = any>(queryParameter: any, options?: RequestOptions): Promise<Result<T>> {
    return http.post<T>(`/${this.service}/${this.model}/list`, queryParameter, options)
  }

  /**
   * 查询全部
   * @param {RequestOptions} options 请求选项
   */
  public async findAll<T = any>(options?: RequestOptions): Promise<Result<T>> {
    return http.get<T>(`/${this.service}/${this.model}/listAll`, null, options)
  }

  /**
   * 查询详情
   * @param {string | number} id 主键id
   * @param {RequestOptions} options 请求选项
   */
  public async findById<T = any>(id: string | number, options?: RequestOptions): Promise<Result<T>> {
    return http.get<T>(`/${this.service}/${this.model}/${id}`, null, options)
  }

  /**
   * 保存
   * @param {*} data 数据
   * @param {RequestOptions} options 请求选项
   */
  public async save<T = any>(data: any, options?: RequestOptions): Promise<Result<T>> {
    return http.post<T>(`/${this.service}/${this.model}/save`, data, options)
  }

  /**
   * 修改
   * @param {*} data 数据
   * @param {RequestOptions} options 请求选项
   */
  public async update<T = any>(data: any, options?: RequestOptions): Promise<Result<T>> {
    return http.put<T>(`/${this.service}/${this.model}`, data, options)
  }

  /**
   * 删除
   * @param {Array<string | number>} ids 主键id数组
   * @param {RequestOptions} options 请求选项
   */
  async delete<T = any>(ids: Array<string | number>, options?: RequestOptions): Promise<Result<T>> {
    return http.delete<T>(`/${this.service}/${this.model}`, { ids }, options)
  }
}
