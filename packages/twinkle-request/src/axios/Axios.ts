import axios from 'axios'
import type { AxiosInstance, AxiosResponse, InternalAxiosRequestConfig } from 'axios'
import { isFunction } from 'twinkle-utils'
import { RequestEnum } from '../types/enum'
import { AxiosTransform } from '../types/AxiosTransform'
import { CreateAxiosOptions, RequestConfig, RequestOptions, Result } from '../types/type'
import { AxiosQueue } from './queue'

export class TwinkleAxios {
  private axiosInstance: AxiosInstance

  private options: CreateAxiosOptions

  constructor(options: CreateAxiosOptions) {
    this.axiosInstance = axios.create(options)
    this.options = options
    this.setupInterceptors()
  }

  // 获取Axios
  getAxios(): AxiosInstance {
    return this.axiosInstance
  }

  // 获取拦截器
  private getTransform(): AxiosTransform | undefined {
    const { transform } = this.options
    return transform
  }

  // 初始化拦截器
  private setupInterceptors(): void {
    const transform = this.getTransform()
    if (!transform) return
    const { requestInterceptors, responseInterceptors, requestInterceptorsCatch, responseInterceptorsCatch } = transform

    const axiosCanceler = new AxiosQueue()

    // 请求拦截器配置处理
    this.axiosInstance.interceptors.request.use((config: InternalAxiosRequestConfig) => {
      const {
        headers: { ignoreCancelToken }
      } = config
      const ignoreCancel =
        ignoreCancelToken !== undefined ? ignoreCancelToken : this.options.requestOptions?.ignoreCancelToken

      // 判断是否忽略重复请求
      !ignoreCancel && axiosCanceler.addPending(config)
      // 判断是否有自定义 全局请求拦截器
      if (requestInterceptors && isFunction(requestInterceptors)) {
        config = requestInterceptors(config, this.options)
      }
      return config
    }, undefined)

    // 全局请求拦截器错误捕获
    requestInterceptorsCatch &&
      isFunction(requestInterceptorsCatch) &&
      this.axiosInstance.interceptors.request.use(undefined, requestInterceptorsCatch)

    // 响应拦截器配置处理
    this.axiosInstance.interceptors.response.use((response: AxiosResponse<Result>) => {
      response && axiosCanceler.removePending(response.config)
      // 判断是否有自定义 全局响应拦截器
      if (responseInterceptors && isFunction(responseInterceptors)) {
        response = responseInterceptors(response)
      }
      return response
    }, undefined)

    // 全局响应结果拦截器错误捕获
    responseInterceptorsCatch &&
      isFunction(responseInterceptorsCatch) &&
      this.axiosInstance.interceptors.response.use(undefined, responseInterceptorsCatch)
  }

  /**
   * 通用请求方法
   * @param {RequestConfig} config 请求配置
   * @param {RequestOptions} options 请求选项
   */
  request<T = any>(config: RequestConfig, options?: RequestOptions): Promise<Result<T>> {
    // 深拷贝对象
    let conf: RequestConfig = config
    const transform = this.getTransform()
    const { requestOptions } = this.options
    const opt: RequestOptions = { ...requestOptions, ...options }
    const { beforeRequestHook, afterRequestHook, afterRequestCatchHook } = transform || {}

    // 判断是否有 单个请求前回调
    if (beforeRequestHook && isFunction(beforeRequestHook)) {
      conf = beforeRequestHook(conf, opt)
    }

    // 重新赋值最新配置
    conf.requestOptions = opt

    return new Promise((resolve, reject) => {
      this.axiosInstance
        .request<any, AxiosResponse<Result>>(conf)
        .then((response: AxiosResponse<Result>) => {
          // 请求是否被取消
          const isCancel = axios.isCancel(response)
          // 判断是否有 单个请求完成后回调
          if (afterRequestHook && !isCancel) {
            try {
              const result = afterRequestHook(response, opt)
              resolve(result)
            } catch (error) {
              reject(error || new Error('request error'))
            }
            return
          }
          resolve(response as unknown as Promise<Result<T>>)
        })
        .catch((error: Error) => {
          // 判断是否有 单个请求失败后回调
          if (afterRequestCatchHook && isFunction(afterRequestCatchHook)) {
            reject(afterRequestCatchHook(error))
            return
          }
          reject(error)
        })
    })
  }

  /**
   * GET请求
   * @param {string} url 请求地址
   * @param {*} data 请求参数
   * @param {RequestOptions} options 请求选项
   */
  get<T = any>(url: string, data?: any, options?: RequestOptions): Promise<Result<T>> {
    return this.request({ url, data, method: RequestEnum.GET }, options)
  }

  /**
   * POST请求
   * @param {string} url 请求地址
   * @param {*} data 请求参数
   * @param {RequestOptions} options 请求选项
   */
  post<T = any>(url: string, data?: any, options?: RequestOptions): Promise<Result<T>> {
    return this.request({ url, data, method: RequestEnum.POST }, options)
  }

  /**
   * DELETE请求
   * @param {string} url 请求地址
   * @param {*} data 请求参数
   * @param {RequestOptions} options 请求选项
   */
  delete<T = any>(url: string, data?: any, options?: RequestOptions): Promise<Result<T>> {
    return this.request({ url, data, method: RequestEnum.DELETE }, options)
  }

  /**
   * PATCH请求
   * @param {string} url 请求地址
   * @param {*} data 请求参数
   * @param {RequestOptions} options 请求选项
   */
  put<T = any>(url: string, data?: any, options?: RequestOptions): Promise<Result<T>> {
    return this.request({ url, data, method: RequestEnum.PUT }, options)
  }
}
