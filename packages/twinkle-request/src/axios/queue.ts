import axios from 'axios'
import type { AxiosRequestConfig, Canceler } from 'axios'
import { isFunction } from 'twinkle-utils'

export const getPendingUrl = (config: AxiosRequestConfig) =>
  [config.method, config.url, config.data, config.params].join('&')

/**
 * Axios请求队列实例
 */
export class AxiosQueue {
  // 队列Map
  private pendingMap: Map<string, Canceler>

  constructor() {
    this.pendingMap = new Map()
  }

  // 加入请求
  addPending(config: AxiosRequestConfig) {
    this.removePending(config)
    const url = getPendingUrl(config)
    config.cancelToken =
      config.cancelToken ||
      new axios.CancelToken((cancel) => {
        if (!this.pendingMap.has(url)) {
          this.pendingMap.set(url, cancel)
        }
      })
  }

  // 移除请求
  removePending(config: AxiosRequestConfig) {
    const url = getPendingUrl(config)
    if (this.pendingMap.has(url)) {
      // 如果队列中存在该请求 需要取消当前请求 并且移除
      const cancel = this.pendingMap.get(url)
      cancel && cancel(url)
      this.pendingMap.delete(url)
    }
  }

  // 清空所有请求
  removeAllPending() {
    this.pendingMap.forEach((cancel) => {
      cancel && isFunction(cancel) && cancel()
    })
    this.pendingMap.clear()
  }

  // 重置
  reset(): void {
    this.pendingMap = new Map()
  }
}
