import toast from 'react-hot-toast'
import { isHttpUrl, isString } from 'twinkle-utils'
import { storage } from 'twinkle-storage'

import { AxiosTransform } from '../../types/AxiosTransform'
import { CreateAxiosOptions } from '../../types/type'
import { ResultEnum } from '../../types/enum'

import { REQUEST_ERROR_MESSAGE, REQUEST_SUCCESS_MESSAGE } from './constant'
import { checkStatus } from '../checkStatus'
import axios from 'axios'

const defaultTransform = new AxiosTransform()

defaultTransform.afterRequestHook = (response, options) => {
  const { isShowMessage = true, isReturnNativeResponse, successMessageText, isShowSuccessMessage } = options

  // 是否返回原生响应头
  if (isReturnNativeResponse) return response

  const { data: res } = response
  if (!res) throw new Error(REQUEST_ERROR_MESSAGE)

  const { code, data, message } = res
  const hasSuccess = data && Reflect.has(data, 'code') && code === ResultEnum.SUCCESS

  isShowMessage && isShowSuccessMessage && hasSuccess && toast.success(successMessageText || REQUEST_SUCCESS_MESSAGE)

  // 响应后端返回的错误信息
  if (code !== ResultEnum.SUCCESS) toast.error(message)
  return response.data
}

defaultTransform.beforeRequestHook = (config, options) => {
  const { apiUrl, joinPrefix, urlPrefix } = options
  const isUrlStr = isHttpUrl(config.url as string)

  if (!isUrlStr && joinPrefix) {
    config.url = `${urlPrefix}${config.url}`
  }

  if (!isUrlStr && apiUrl && isString(apiUrl)) {
    config.url = `${apiUrl}${config.url}`
  }

  return config
}

defaultTransform.requestInterceptors = (config) => {
  // 处理token
  const token = storage.getItem('token')
  config.headers.Authorization = `Bearer ${token}`
  // 判断是否需要loading动画

  // 将请求参数处理成为params形式
  if ((config as CreateAxiosOptions).requestOptions?.joinParamsToUrl) {
    for (const key in config.data) {
      config.url += `/${config.data[key]}`
    }
    delete config.data
  }
  return config
}

defaultTransform.responseInterceptorsCatch = (error: any) => {
  const { response, code, message } = error || {}
  // 获取后端接口错误信息
  const msg: string = response && response.data && response.data.message ? response.data.message : ''
  const err: string = error.toString()
  try {
    if (code === 'ECONNABORTED' && message.indexOf('timeout') !== -1) {
      console.log('接口请求超时，请稍后重试！')
      return
    }
    if (err && err.includes('Network Error')) {
      console.log('网络异常，请稍后重试！')
      return Promise.reject(error)
    }
  } catch (error) {
    throw new Error(error as any)
  }
  // 请求是否被取消
  const isCancel = axios.isCancel(error)
  if (!isCancel) {
    checkStatus(error.response && error.response.status, msg)
  } else {
    console.warn(error, '请求被取消')
  }
  return Promise.reject(response?.data)
}

export { defaultTransform }
