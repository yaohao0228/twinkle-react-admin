import {
  NOT_AUTH_ERROR_MESSAGE,
  FORBID_VISIT_ERROR_MESSAGE,
  NOT_FOUND_ERROR_MESSAGE,
  METHOD_ERROR_MESSAGE,
  TIME_OUT_ERROR_MESSAGE,
  SERVER_ERROR_MESSAGE,
  SERVER_NOT_APPLICABLE_ERROR_MESSAGE
} from './config/constant'
import { toast } from 'react-hot-toast'

export const checkStatus = (status: number, msg: string): void => {
  const codeOptions: Record<number, () => void> = {
    400: () => toast.error(msg),
    401: () => toast.error(NOT_AUTH_ERROR_MESSAGE),
    403: () => toast.error(FORBID_VISIT_ERROR_MESSAGE),
    404: () => toast.error(NOT_FOUND_ERROR_MESSAGE),
    405: () => toast.error(METHOD_ERROR_MESSAGE),
    408: () => toast.error(TIME_OUT_ERROR_MESSAGE),
    500: () => toast.error(SERVER_ERROR_MESSAGE),
    503: () => toast.error(SERVER_NOT_APPLICABLE_ERROR_MESSAGE)
  }
  typeof codeOptions[status] === 'function' && codeOptions[status]()
}
