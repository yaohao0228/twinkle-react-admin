import { TwinkleAxios } from './Axios'
import { CreateAxiosOptions } from '../types/type'
import { defaultTransform } from './config/defaultTransform'

const defaultOptions: Partial<CreateAxiosOptions> = {
  timeout: 10 * 1000,
  transform: defaultTransform,
  requestOptions: {
    joinPrefix: true,
    isReturnNativeResponse: false,
    joinParamsToUrl: false,
    errorMessageMode: 'none',
    joinTime: true,
    ignoreCancelToken: true,
    withToken: true,
    loading: false,
    apiUrl: 'http://localhost:12000',
    urlPrefix: '/v1'
  }
}

export const createAxios = (options: Partial<CreateAxiosOptions> = {}) => {
  return new TwinkleAxios(Object.assign(defaultOptions, options))
}

export const http = createAxios()
