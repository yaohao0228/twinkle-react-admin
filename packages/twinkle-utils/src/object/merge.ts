type CustomObject = Record<string, any>

/**
 * 合并对象
 * @param {Object} target 对象
 */
export const merge = <T extends CustomObject>(target: T, ...sources: T[]): T => {
  if (!sources.length) {
    return target
  }

  const source = sources.shift()

  if (source === undefined) {
    return target
  }

  for (const key in source) {
    if (Object.prototype.hasOwnProperty.call(source, key)) {
      if (typeof source[key] === 'object' && source[key] !== null) {
        if (typeof target[key] === 'object' && target[key] !== null) {
          target[key] = merge(target[key], source[key])
        } else {
          target[key] = source[key]
        }
      } else {
        target[key] = source[key]
      }
    }
  }

  return merge(target, ...sources)
}
