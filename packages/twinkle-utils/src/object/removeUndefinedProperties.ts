/**
 * 删除对象中空属性值的属性
 * @param {Object} target 目标对象
 */
export const removeUndefinedProperties = (target: Record<string, any>): Record<string, any> => {
  const result: Record<string, any> = {}

  for (const key in target) {
    if (target[key] !== undefined) {
      result[key] = target[key]
    }
  }

  return result
}
