/**
 * 获取对象的hash码
 * @param {Object} obj 目标对象
 */
export const getObjectHashCode = (obj: object): number => {
  const str = JSON.stringify(obj)
  let hash = 0
  if (str.length === 0) {
    return hash
  }
  for (let i = 0; i < str.length; i++) {
    const char = str.charCodeAt(i)
    hash = (hash << 5) - hash + char
    hash = hash & hash
  }
  return hash
}
