export { camelCase } from './camelCase'
export { upperFirst } from './upperFirst'
export { replaceTemplateStrings } from './replaceTemplateStrings'
