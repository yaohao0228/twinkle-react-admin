/**
 * 替换模版字符串
 * @param {string} template 模版
 * @param {Object} values 数据对象
 * @returns {string}
 */
export const replaceTemplateStrings = (template: string, values: Record<string, any>): string => {
  return template.replace(/\${(.*?)}/g, (match, key) => {
    const trimmedKey = key.trim()
    if (trimmedKey in values) {
      return encodeURIComponent(values[trimmedKey])
    } else {
      console.warn(`Warning: Key "${trimmedKey}" not found in values object.`)
      return match
    }
  })
}
