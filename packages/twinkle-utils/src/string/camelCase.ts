/**
 * 字符串转为驼峰命名法
 */
export const camelCase = (input: string, isUpperCamelCase: boolean = true): string => {
  // 处理短横线和下划线，将其替换为空格
  const stringWithoutHyphensOrUnderscores = input.replace(/[-_]/g, ' ')
  // 将每个单词的首字母大写
  const capitalizedWords = stringWithoutHyphensOrUnderscores.replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
    return index === 0 && !isUpperCamelCase ? word.toLowerCase() : word.toUpperCase()
  })
  // 移除空格并返回结果
  const result = capitalizedWords.replace(/\s+/g, '')
  return result
}
