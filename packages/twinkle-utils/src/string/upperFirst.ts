/**
 * 首字母大写
 */
export const upperFirst = (input: string): string => {
  const characters = [...input]
  characters[0] = characters[0].toUpperCase()
  return characters.join('')
}
