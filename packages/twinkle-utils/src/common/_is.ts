/**
 * 判断值是否未某个类型
 */
export const _is = (val: unknown, type: string) => Object.prototype.toString.call(val) === `[object ${type}]`
