/**
 * 判断用户设备类型
 */
export const _agentHas = (type: string): boolean => navigator.userAgent.toLowerCase().indexOf(type) > -1
