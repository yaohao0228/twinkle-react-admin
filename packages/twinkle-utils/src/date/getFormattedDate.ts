/**
 * 获取日期
 * @param {number} offset 日期偏移量
 * @param {string} format 日期格式
 */
export const getFormattedDate = (offset: number = 0, format: string = 'YYYY-MM-DD'): string => {
  const currentDate = new Date()
  currentDate.setDate(currentDate.getDate() + offset)

  const year = currentDate.getFullYear()
  const month = (currentDate.getMonth() + 1).toString().padStart(2, '0')
  const day = currentDate.getDate().toString().padStart(2, '0')

  let formattedDate = format.replace('YYYY', year.toString())
  formattedDate = formattedDate.replace('MM', month)
  formattedDate = formattedDate.replace('DD', day)

  return formattedDate
}
