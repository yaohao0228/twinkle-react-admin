/**
 * 是否为promise
 */
export const isPromise = <T = unknown>(value: any): value is Promise<T> =>
  typeof value.then === 'function' && value.toString() === '[object Promise]'
