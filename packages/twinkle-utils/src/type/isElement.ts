import { isObject } from './isObject'

/**
 * 是否为事件
 */
export const isElement = (val: unknown): val is Element => isObject(val) && !!val.tagName
