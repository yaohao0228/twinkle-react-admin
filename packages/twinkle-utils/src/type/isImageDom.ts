/**
 * 是否为图片节点
 */
export const isImageDom = (o: Element): boolean => o && ['IMAGE', 'IMG'].includes(o.tagName)
