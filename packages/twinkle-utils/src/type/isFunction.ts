/**
 * 是否为函数
 */
export const isFunction = (val: unknown): val is (...args: any[]) => any => typeof val === 'function'
