import { _is } from '../common/_is'

/**
 * 是否为字符串类型
 */
export const isString = (val: unknown): val is string => _is(val, 'String')
