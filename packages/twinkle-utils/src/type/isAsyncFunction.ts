import { _is } from '../common/_is'

/**
 * 是否为AsyncFunction
 */
export const isAsyncFunction = <T = unknown>(val: unknown): val is () => Promise<T> => _is(val, 'AsyncFunction')
