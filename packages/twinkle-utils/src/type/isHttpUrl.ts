/**
 * 是否为请求路径
 */
export const isHttpUrl = (val: string): boolean => /^(http|https):\/\//g.test(val)
