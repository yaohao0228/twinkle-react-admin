/**
 * 是否为空对象
 */
export const isNotObject = (val: unknown): boolean => JSON.stringify(val) === '{}'
