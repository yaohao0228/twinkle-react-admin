import { _is } from '../common/_is'

/**
 * 是否为时间
 */
export const isDate = (val: unknown): val is Date => _is(val, 'Date')
