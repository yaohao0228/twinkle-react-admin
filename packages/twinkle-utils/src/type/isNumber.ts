import { _is } from '../common/_is'

/**
 * 是否为数值类型
 */
export const isNumber = (val: unknown): val is number => _is(val, 'Number')
