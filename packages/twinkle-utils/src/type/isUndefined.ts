/**
 * 是否为undefined
 */
export const isUndefined = (val: unknown): val is undefined => val === undefined
