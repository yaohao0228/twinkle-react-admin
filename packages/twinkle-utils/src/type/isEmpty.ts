import { isNull } from './isNull'
import { isUndefined } from './isUndefined'

/**
 * 判断是否为空
 */
export const isEmpty = (val: unknown): val is null | undefined | '' => isNull(val) || isUndefined(val) || val === ''
