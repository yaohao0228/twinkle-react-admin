/**
 * 是否为数组
 */
export const isArray = (val: unknown): val is Array<unknown> => Array.isArray(val)
