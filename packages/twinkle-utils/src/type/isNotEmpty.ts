import { isNull } from './isNull'
import { isUndefined } from './isUndefined'

/**
 * 判断是否为非空
 */
export const isNotEmpty = (val: unknown): val is NonNullable<typeof val> =>
  !isNull(val) && !isUndefined(val) && val !== ''
