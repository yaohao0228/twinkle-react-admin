import { _is } from '../common/_is'

/**
 * 是否为对象
 */
export const isObject = (val: unknown): val is Record<string | number | symbol, unknown> =>
  val !== null && _is(val, 'Object')
