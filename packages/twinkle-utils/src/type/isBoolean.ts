import { _is } from '../common/_is'

/**
 * 是否为布尔类型
 */
export const isBoolean = (val: unknown): val is boolean => _is(val, 'Boolean')
