import { _agentHas } from '../common/_agentHas'

/**
 * 是否为firefox
 */
export const isFirefox = (): boolean => _agentHas('firefox')
