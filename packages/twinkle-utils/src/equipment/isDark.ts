/**
 * 是否为深色系统
 */
export const isDark = (): boolean => window.matchMedia('(prefers-color-scheme:dark)').matches
