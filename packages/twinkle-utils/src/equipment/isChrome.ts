import { _agentHas } from '../common/_agentHas'

/**
 * 是否为chrome
 */
export const isChrome = (): boolean => _agentHas('chrome') && !_agentHas('edge')
