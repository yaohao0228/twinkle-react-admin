import { _agentHas } from '../common/_agentHas'

/**
 * 是否为mac
 */
export const isMac = (): boolean => _agentHas('macintosh')
