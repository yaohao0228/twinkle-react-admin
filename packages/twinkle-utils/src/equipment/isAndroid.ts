import { _agentHas } from '../common/_agentHas'

/**
 * 是否为android
 */
export const isAndroid = (): boolean => _agentHas('android')
