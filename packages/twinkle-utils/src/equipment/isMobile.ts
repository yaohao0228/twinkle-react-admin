import { _agentHas } from '../common/_agentHas'

/**
 * 是否为移动端
 */
export const isMobile = (): boolean => _agentHas('android') || _agentHas('iphone')
