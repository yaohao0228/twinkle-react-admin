import { _agentHas } from '../common/_agentHas'

/**
 * 是否为ie
 */
export const isIE = (): boolean => _agentHas('msie')
