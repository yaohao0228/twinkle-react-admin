import { _agentHas } from '../common/_agentHas'

/**
 * 是否为edge
 */
export const isEdge = (): boolean => _agentHas('edge')
