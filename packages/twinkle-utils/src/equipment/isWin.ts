import { _agentHas } from '../common/_agentHas'

/**
 * 是否为windows
 */
export const isWin = (): boolean => _agentHas('windows')
