import { _agentHas } from '../common/_agentHas'

/**
 * 是否为iphone
 */
export const isIPhone = (): boolean => _agentHas('iphone')
