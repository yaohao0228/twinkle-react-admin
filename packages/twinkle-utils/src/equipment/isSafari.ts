import { _agentHas } from '../common/_agentHas'

/**
 * 是否为safari
 */
export const isSafari = (): boolean => _agentHas('safari') && !_agentHas('chrome')
