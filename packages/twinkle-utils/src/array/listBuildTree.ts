/**
 * @description 将数组对象按照指定字段的树形结构进行排序
 * @param {Array} data 待排序的数组对象
 * @param {string} currentNodeKey 当前节点字段
 * @param {string} parentKey 用于判断父节点的字段。当值为 null 时，表示顶节点
 * @returns 排序后的树形结构数组
 */
export const listBuildTree = <T extends Record<string, any> & { children?: T[] }>(
  data: T[],
  currentNodeKey: keyof T,
  parentKey: keyof T
) => {
  const treeList: any[] = []
  // 生成一个以 currentNodeKey 为键值的 对象
  const treeNode: Map<number, T> = new Map()
  data.forEach((item) => {
    const currentNodeId = Number(item[currentNodeKey])
    treeNode.set(currentNodeId, item)
  })
  data.forEach((item) => {
    const parentNodeId = Number(item[parentKey])
    const node = item
    const parentNode = treeNode.get(parentNodeId)
    if (parentNode) {
      if ('children' in parentNode) {
        parentNode.children && parentNode.children.push(node)
      } else {
        parentNode.children = []
        parentNode.children.push(node)
      }
    } else {
      treeList.push(node)
    }
  })
  return treeList
}
