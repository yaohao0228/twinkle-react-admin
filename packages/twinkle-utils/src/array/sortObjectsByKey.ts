/**
 * @description 快速递归排序
 * @param {Array} objects 待排序的数组对象
 * @param {string} key 根据排序的字段
 * @returns 排序后的数据
 */
export const sortObjectsByKey = <T extends Record<string, any>>(objects: T[], key: keyof T): T[] => {
  // 递归排序函数
  function recursiveSort(objArray: T[]): void {
    if (objArray.length <= 1) {
      return
    }

    const pivotIndex = Math.floor(objArray.length / 2)
    const pivot = objArray[pivotIndex]
    const smaller: T[] = []
    const equal: T[] = []
    const larger: T[] = []

    for (const obj of objArray) {
      if (obj[key] < pivot[key]) {
        smaller.push(obj)
      } else if (obj[key] === pivot[key]) {
        equal.push(obj)
      } else {
        larger.push(obj)
      }
    }

    recursiveSort(smaller)
    recursiveSort(larger)

    objArray.splice(0, objArray.length, ...smaller, ...equal, ...larger)
  }

  // 深度优先递归排序函数
  function recursiveSortChildren(obj: T): void {
    if (obj.children && obj.children.length > 0) {
      recursiveSort(obj.children)
      obj.children.forEach(recursiveSortChildren)
    }
  }

  // 主排序逻辑
  recursiveSort(objects)
  objects.forEach(recursiveSortChildren)

  return objects
}
