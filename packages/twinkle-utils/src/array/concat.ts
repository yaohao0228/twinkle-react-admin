/**
 * 合并数组
 */
export const concat = <T extends never>(...arrays: T[][]): T[] => {
  return [].concat(...arrays)
}
