/**
 * 发布订阅者
 */
class EventBus {
  subscriptions: Record<string, Record<symbol, (...args: any) => void>> = {}

  /**
   * 订阅
   * @param {string} eventType 订阅类型
   * @param {function} callback 回调方法
   */
  public subscribe<T extends string = any>(eventType: T, callback: (...args: any) => void) {
    const id = Symbol('id')
    if (!this.subscriptions[eventType]) this.subscriptions[eventType] = {}
    this.subscriptions[eventType][id] = callback
    return {
      /**
       * 取消订阅
       */
      unsubscribe: () => {
        delete this.subscriptions[eventType][id]
        if (Object.getOwnPropertySymbols(this.subscriptions[eventType]).length === 0) {
          delete this.subscriptions[eventType]
        }
      }
    }
  }

  /**
   * 发布
   * @param {string} eventType 订阅类型
   * @param {*} arg 参数
   */
  public publish<T extends string = any>(eventType: T, ...arg: any) {
    if (!this.subscriptions[eventType]) return

    Object.getOwnPropertySymbols(this.subscriptions[eventType]).forEach((key) => {
      this.subscriptions[eventType][key](arg)
    })
  }
}

const eventBus = new EventBus()

export { eventBus }
